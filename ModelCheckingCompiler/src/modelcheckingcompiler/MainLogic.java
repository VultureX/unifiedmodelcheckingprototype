/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelcheckingcompiler;

import Benchmarks.Algorithm;
import Benchmarks.Benchmark;
import Benchmarks.CMDAlgorithm;
import Benchmarks.NuSMV;
import Benchmarks.Uppaal;
import Benchmarks.Yices;
import CodeGeneration.AST.Formula;
import CodeGeneration.Compiler;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.CompilerNuSMV;
import CodeGeneration.CompilerSMT;
import CodeGeneration.CompilerUppaal;
import CodeGeneration.Parser;
import CodeGeneration.Parser.ParseError;
import CodeGeneration.Tokenizer;
import GUI.MainWindow;
import Tools.DirectoryInfo;
import Tools.FileInfo;
import Tools.FileWriterUtility;
import Tools.Utils;
import java.util.ArrayList;

/**
 *
 * @author VultureX
 */
public class MainLogic {
    
    private Benchmark benchmark;
    private boolean writeToFile;
    private DirectoryInfo outputDir;
    private FileInfo scriptFile;
    //private String outputDir;
    //private String relativeDir;
    //private String filename;
    private Algorithm[] algorithms;
    private MainWindow gui;    
    
    private SelectableCompiler[] compilers = new SelectableCompiler[] {
        new SelectableCompiler(CompilerSMT.class, Yices.class),
        new SelectableCompiler(CompilerNuSMV.class, NuSMV.class),
        new SelectableCompiler(CompilerUppaal.class, Uppaal.class)
    };
    private ArrayList<Compiler> selectedCompilers;
    private ArrayList<Algorithm> selectedAlgorithms;
    
    public MainLogic(boolean writeToFile, DirectoryInfo outputDir, FileInfo scriptFile) {
        this.writeToFile = writeToFile;
        this.outputDir = outputDir;
        this.scriptFile = scriptFile;
        Runtime.getRuntime().addShutdownHook(new CleanupThread());
    }
    
    public void setGUI(MainWindow gui) {
        this.gui = gui;
    }
    
    public void killBenchmark(int reason) {
        if(benchmark != null /*&& benchmarkThread.isAlive()*/) {
            benchmark.stop(reason);
        }
    }
    
    public void runBenchmark(int timeOut) {
        if(algorithms == null) {
            System.err.println("Algorithms are not compiled");
            return;
        }
        
        if(benchmark == null || !benchmark.isRunning()) {
            if(writeToFile) {
                this.benchmark = new Benchmark(algorithms, gui, outputDir, timeOut);
                FileWriterUtility.moveFile(scriptFile, outputDir);                
            } else {
                this.benchmark = new Benchmark(algorithms, gui, timeOut);
            }
            this.benchmark.start();
        } else if(benchmark.isRunning()) {
            System.err.println("Benchmark is already running");
        }
    }
    
    public void compileAndRun(String code, DirectoryInfo relativeDir, String name, int timeOut) {
        if(tryCompile(code, relativeDir, name)) {
            createAlgorithmList();
            this.algorithms = selectedAlgorithms.toArray(new Algorithm[]{});
            runBenchmark(timeOut);
        }        
    }
    
    public void compile(String code, DirectoryInfo compileDir, String outputName) throws ParseError, CompileError {       
        createCompilerList(code);
        
        for(Compiler c : selectedCompilers) {            
            c.compile(compileDir, outputName);
        }
        
        if(selectedCompilers.isEmpty()) {
            System.err.println("Nothing to compile!");
        }
    }
    
    public boolean tryCompile(String code, DirectoryInfo compileDir, String outputName) {       
        try {
            createCompilerList(code);
        } catch (ParseError ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        
        for(Compiler c : selectedCompilers) {
            try {
                c.compile(compileDir, outputName);
            } catch (CompileError ex) {
                System.err.println(ex.getMessage());
                return false;
            }
        }
        
        if(selectedCompilers.isEmpty()) {
            System.err.println("Nothing to compile!");
            return false;
        }
        
        return true;
    }
    
    public void enable(Class<? extends Compiler>... compilerClasses) {
        for(int i = 0; i< compilerClasses.length; i++) {
            for(SelectableCompiler compiler : compilers) {
                if(compiler.compilerClass.equals(compilerClasses[i])) {
                    compiler.setSelected(true);
                }
            }
        }        
    }
    
    public void setEnabled(Class<? extends Compiler> compilerClass, boolean isSelected) {
        for(SelectableCompiler compiler : compilers) {
            if(compiler.compilerClass.equals(compilerClass)) {
                compiler.setSelected(isSelected);
            }
        }
    }
    
    private void createAlgorithmList() {
        selectedAlgorithms = new ArrayList<>();
        for(SelectableCompiler compiler : compilers) {
            if(compiler.isSelected()) {                
                FileInfo[] files = compiler.getCompilerInstance().getCompiledFilesInfo();
                
                //a = new Algorithm(files)
                Algorithm a = Utils.getNewInstanceFromArgs(compiler.algorithmClass, 
                        new Class<?>[]{FileInfo[].class, Formula.Mode.class}, 
                        new Object[] {files, compiler.getCompilerInstance().getMode()});                
                
                selectedAlgorithms.add(a);
                compiler.setInstance(a);
            }
        }
    }
    
    private void createCompilerList(String code) throws ParseError {
        Tokenizer t = new Tokenizer(code);
        t.tokenize();
        Parser p = new Parser(t);
        
        selectedCompilers = new ArrayList<>();
        for(SelectableCompiler compiler : compilers) {
            if(compiler.isSelected()) {
                Formula e = p.parseFormula();
                
                Compiler c = Utils.getNewInstanceFromArgs(compiler.compilerClass,
                        new Class<?>[]{Formula.class}, new Object[]{e});
                selectedCompilers.add(c);
                compiler.setInstance(c); //Use the instance to determine filenames for algorithms later
            }
        }
    }
    
    private class SelectableCompiler {
        private Class<Compiler> compilerClass;
        private Class<Algorithm> algorithmClass;
        private Compiler compilerInstance;
        private Algorithm algorithmInstance;
        private boolean isSelected;
        
        //@Todo: how can I type the Class compiler correctly?
        public SelectableCompiler(Class compiler, Class algorithm) {
            this.compilerClass = compiler;
            this.algorithmClass = algorithm;
            this.isSelected = false;
        }
        
        public Compiler getCompilerInstance() {
            return this.compilerInstance;
        }
        
        public Algorithm getAlgorithmInstance() {
            return this.algorithmInstance;
        }
        
        public void setInstance(Compiler c) {
            this.compilerInstance = c;
        }
        
        public void setInstance(Algorithm a) {
            this.algorithmInstance = a;
        }
        
        public void setSelected(boolean selected) {
            this.isSelected = selected;
        }
        
        public boolean isSelected() {
            return this.isSelected;
        }
        
        public Class getCompilerClass() {
            return compilerClass;
        }
        
        public Class getAlgorithmClass() {
            return algorithmClass;
        }
    }
    
    /*private abstract class Selectable {
        private Class selectableItem;
        private boolean isSelected;
        public Selectable(Class selectableItem, boolean isSelected) {
            this.selectableItem = selectableItem;
            this.isSelected = isSelected;
        }
        
        public void setSelected(boolean selected) {
            this.isSelected = selected;
        }
        
        public boolean isSelected() {
            return this.isSelected;
        }
        
        public Class getItem() {
            return selectableItem;
        }
    }*/
    
    private class CleanupThread extends Thread {
        @Override
        public void run() {
            if(benchmark != null) {
                benchmark.stop(CMDAlgorithm.USER_ABORT);
            }
        }
    }
}
