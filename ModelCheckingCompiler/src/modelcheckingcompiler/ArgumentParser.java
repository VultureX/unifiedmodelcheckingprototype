/*
 * ArgumentParser.java
 * Copyright(c) 2014
 */

package modelcheckingcompiler;

import Benchmarks.Yices;
import CodeGeneration.CompilerNuSMV;
import CodeGeneration.CompilerSMT;
import CodeGeneration.CompilerUppaal;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Peter Maandag
 */
public class ArgumentParser {
    public static final int OPEN = 0x01;
    public static final int COMPILE = 0x02;
    public static final int RUN = 0x04;
    public static final int ENABLE_COMPILERS = 0x08;
    public static final int GENERATE_SCRIPT = 0x10;
    public static final int NO_GUI = 0x20;
    public static final int REDIRECT_OUTPUT = 0x40;
    public static final int WRITE_TO_FILE = 0x80;
    
    public static final String ARG_COMPILE = "compile";
    public static final String ARG_RUN = "run";
    public static final String ARG_BENCH = "benchmark";
    public static final String YICES = "y";
    public static final String NUSMV = "n";
    public static final String UPPAAL = "u";
    
    private String[] args;
    
    public ArgumentParser(String[] args) {
        this.args = args;
        format();
    }   
    
    private void format() {
        for(int i = 0; i < args.length; i++) {
            if(args[i].startsWith("-")) {
                args[i] = args[i].toLowerCase().replaceAll("[^a-z]","");
            }
        }
    }
    
    public Command parse() {
        if(args.length == 0) {
            System.err.println("No command arguments detected");
            return null;
        }
        
        Command c = new Command();
        switch(args[0]) {
            case ARG_COMPILE:
                c.mode = ENABLE_COMPILERS | COMPILE | NO_GUI;
                break;
            case ARG_RUN:
                c.mode = OPEN | ENABLE_COMPILERS | COMPILE | RUN | NO_GUI;
                break;
            case ARG_BENCH:
                c.mode = OPEN | ENABLE_COMPILERS | COMPILE | RUN | NO_GUI | WRITE_TO_FILE;
                break;
            default:
                System.err.println("Invalid command argument detected");
                return null;
        }
        
        if(args.length <= 1) {
            System.err.println("location of script file expected");
            return null;
        }
        
        c.script = args[1];
        
        if(args.length <= 2) {
            System.err.println("back-end specification expected");
            return null;
        }
        
        c.compilers = new ArrayList();
        boolean stop = false;
        for(int i = 2; i < args.length; i++) {
            switch(args[i]) {
                case YICES:
                    if(!c.compilers.contains(CompilerSMT.class)) {
                        c.compilers.add(CompilerSMT.class);
                    } break;
                case NUSMV:
                    if(!c.compilers.contains(CompilerNuSMV.class)) {
                        c.compilers.add(CompilerNuSMV.class);
                    } break;
                case UPPAAL:
                    if(!c.compilers.contains(CompilerUppaal.class)) {
                        c.compilers.add(CompilerUppaal.class);
                    } break;
                default:
                    stop = true;
            }
            
            if(stop) {
                break;
            }
        }
        
        if(c.compilers.isEmpty()) {
            System.err.println("back-end specification expected");
            return null;
        }
        
        if(args[0].equals(ARG_BENCH)) {
            String timeout = args[args.length-1];
            try {
                c.timeout = Integer.parseInt(timeout);
            } catch (NumberFormatException e) {
                c.timeout = 0;
                //System.err.println("timeout value expected");
                //return null;
            }           
        }       
        
        return c;
    }
    
    public static class Command {
        public int mode;
        public ArrayList<Class> compilers;
        public String script;
        public int timeout;
    }
}
