/*
 * Main.java
 * Copyright(c) 2014
 */
package modelcheckingcompiler;

import CodeGeneration.AST;
import CodeGeneration.Compiler;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.CompilerNuSMV;
import CodeGeneration.CompilerSMT;
import CodeGeneration.CompilerUppaal;
import CodeGeneration.Parser;
import CodeGeneration.Parser.ParseError;
import CodeGeneration.Semantics.TypeChecker;
import CodeGeneration.Tokenizer;
import GUI.MainWindow;
import Scripts.Robot;
import Scripts.Script;
import Tools.DirectoryInfo;
import Tools.FileInfo;
import Tools.FileReader;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import static modelcheckingcompiler.ArgumentParser.*;
import modelcheckingcompiler.ArgumentParser.Command;

/**
 *
 * @author Peter Maandag
 */
public class Main {
    public static final String VERSION = "v0.03";
    public static final String APP_NAME = "Transition system multi-compiler";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseError, CompileError {        
        final int compileAndRun = OPEN | ENABLE_COMPILERS | COMPILE | RUN;
        final int compile = ENABLE_COMPILERS | COMPILE | NO_GUI;
        
        //compileAndRun(compileAndRun | NO_GUI | WRITE_TO_FILE); //compile and run
        //compileAndRun(compileAndRun | REDIRECT_OUTPUT); //compile and run
        //compileAndRun(GENERATE_SCRIPT | compileAndRun | WRITE_TO_FILE); //generate script, compile it and run
        //compileAndRun(OPEN | ENABLE_COMPILERS); //Just open and set compilers
        //compileAndRun(GENERATE_SCRIPT); //Just generate a script
        //compileAndRun(compile);
        //compileAndRun(OPEN | ENABLE_COMPILERS | RUN);
        //compileAndRun(OPEN | REDIRECT_OUTPUT);
        
        //compile();
        //generateScript();
        //run();
        
        ArgumentParser p = new ArgumentParser(args);
        Command c = p.parse();
        if(c == null) {
            System.out.println("Exiting...");
            return;
        }
        compileAndRun(c);
    }
    
    public static void showInfo() {
        MainWindow.getConsole().println(APP_NAME + " " + VERSION);
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        MainWindow.getConsole().println(dateFormat.format(new Date()));
    }
    
    public static String generateScript() throws ParseError, CompileError {
        
//        FileInfo file = new FileInfo("../", "WaterBottles.script");
//        final int[] bottles = new int[]{93,97,93};
//        final int maxSteps = 200;
//        final int goalValue = 2;
//        s = new WaterBottles(bottles, maxSteps, goalValue);
//        return s.generate(file).fileName;
        
//        FileInfo file = new FileInfo("../", "TruckDelivery.script");
//        Script s = TruckDeliveryDet.getExampleInstance(322, 27);
//        return s.generate(file).fileName;
        
//        FileInfo file = new FileInfo("../", "TruckDeliveryNonDet.script");
//        Script s = TruckDeliveryNonDet.getExampleInstance(322, -1);
//        return s.generate(file).fileName;
        
//        FileInfo file = new FileInfo("../", "ABP.script");
//        Script s = new ABP(1, ABP.DataType.Int, false, -1);
//        return s.generate(file).fileName;
        
//        final int maxValueC = 1023;
//        final int startValueC = 1;
//        final int numProcesses = 2;               
//        final int goalValueC = 355;
//        final int maxSteps = 12 + 33;
//        FileInfo file = new FileInfo("../", "Parallel.script");
//        Script s = new ParallelVariable(numProcesses, startValueC, goalValueC, maxValueC, maxSteps);
//        return s.generate(file).fileName;
        
        FileInfo file = new FileInfo("../", "VacuumRobot.script");
        final int n = 2;
        final int x = 0;
        final int y = 0;
        final Robot.Dir dir = Robot.Dir.N;
        final int maxSteps = -1;
        Script s = new Robot(n, x, y, dir, maxSteps);
        return s.generate(file).fileName;
    }
    
    public static void compileAndRun(int mode) throws ParseError, CompileError {        
        //Script name, output file name and compilers
        String filename;
        //filename = "robot.script";
        //filename = "properties.script";
        //filename = "properties2.script";
        //filename = "stepsTest.script";
        //filename = "WaterBottles.script";
        //filename = "deadlockTest.script";
        filename = "processTest.script";
        //filename = "ABPGenerated.script";
        //filename = "opg2_3NonDetTest.script";
        //filename = "TruckDelivery_Det_S4_E10_T319_BS0.script";
        //filename = "Parallel.script";
        //filename = "testScript.script";
        //filename = "ABPTypeErrorToFix.script";
        //filename = "parallelTestNew";
        //filename = "parallelTest";
        final DirectoryInfo relativeDir = new DirectoryInfo ("../");
        String compileName = "compileTest";
        final Class[] compilers = new Class[] {
            //CompilerNuSMV.class,
            CompilerSMT.class,
            //CompilerUppaal.class,
        };
        final int timeout = 15 * 60;
        
        compileAndRun(mode, filename, compileName, relativeDir, compilers, timeout);       
    }
    
    public static void compileAndRun(Command c) throws ParseError, CompileError {
        final int mode = c.mode;
        final String filename = c.script;
        final String compileName = filename;
        final DirectoryInfo relativeDir = new DirectoryInfo ("../");
        final Class[] compilers = c.compilers.toArray(new Class[0]);
        final int timeout = c.timeout;
        
        compileAndRun(mode, filename, compileName, relativeDir, compilers, timeout);
    }
    
    public static void compileAndRun(int mode, String filename, String compileName,
            DirectoryInfo relativeDir, Class[] compilers, int timeout) throws ParseError, CompileError {
        //Generate script
        String outputFolder = compileName;
        if((mode & GENERATE_SCRIPT) > 0) {
            filename = generateScript();
            compileName = filename;
            outputFolder = filename;
        }
        
        //Read the file
        final FileInfo file = new FileInfo(relativeDir, filename);
        String code = FileReader.readFile(file);
        
        //Setup model
        DirectoryInfo outputDir = relativeDir;
        final boolean writeToFile = (mode & WRITE_TO_FILE) > 0;
        if(writeToFile) {
            outputDir = new DirectoryInfo ("../Benchmarks/" + outputFolder);
            //Create the benchmark output directory
            File f = outputDir.getFile();
            if(!f.exists()) {
                f.mkdir();
            }
        }
        MainLogic m = new MainLogic(writeToFile, outputDir, file);
        
        //Setup GUI
        if((mode & NO_GUI) == 0) {
            MainWindow w = new MainWindow(m);
            m.setGUI(w);
            w.setControllers();
            
            if((mode & OPEN) > 0) {
                w.setEditorText(filename, code, relativeDir, compileName, false);
                w.setVisible(true);
            }
            
            if((mode & REDIRECT_OUTPUT) > 0) {
                w.createConsole(); //Dangerous
                showInfo();
            }
        }       
        
        //Start benchmarking:      
        if((mode & ENABLE_COMPILERS) > 0) {
            m.enable(compilers);
        }        
        
        if((mode & COMPILE) > 0) {
            if((mode & RUN) > 0) {
                m.compileAndRun(code, outputDir, compileName, timeout);
            } else {
                m.compile(code, outputDir, compileName);
            }
        } else if((mode & RUN) > 0) {
            m.runBenchmark(timeout);
        }
    }
    
    /*public static void run() {
        final int TIMEOUT = 15 * 60;
        FileInfo[] sourceFiles = new FileInfo[] {
            new FileInfo("../", "parrallelNative.xml"),
            new FileInfo("../", "parrallelNative.q")
            //new FileInfo("../", "compileTest.xml"),
            //new FileInfo("../", "compileTest.q")
        };
        Uppaal up = new Uppaal(sourceFiles);
        Benchmark b = new Benchmark(new Algorithm[] { up },
                null, new DirectoryInfo ("../Benchmarks/"), TIMEOUT);
        b.start();
    }*/
    
    public static void compile() throws ParseError, CompileError {
        //FileInfo file = new FileInfo("../", "parallel.script");
        //FileInfo file = new FileInfo("../", "parallelTest");
        //FileInfo file = new FileInfo("../", "parallelTestNew");
        //FileInfo file = new FileInfo("../", "processTest.script");
        //FileInfo file = new FileInfo("../", "testScript.script");
        FileInfo file = new FileInfo("../", "stepsTest.script");
        Tokenizer t = new Tokenizer(file);
        t.tokenize();
        Parser p = new Parser(t);       
        AST.Formula e = p.parseFormula();        
        //System.out.println(e.flattenToken());
        //System.out.println(e.getSourceCodeDescription());
        
        TypeChecker s = TypeChecker.getInstance(e);
        s.performChecks();
        
        //Semantics s = new Semantics(e);
        //s.performChecks();
        
        //Transformation transform = new Transformation(e);
        //transform.addStepsVariable();
        
        //Compiler c = new CompilerNuSMV(e);
        //Compiler c = new CompilerSMT(e);
        Compiler c = new CompilerUppaal(e);
        c.compile(new DirectoryInfo("../"), "compileTest");
        
        //Benchmarking
        //Algorithm a = new NuSMV(compile("WaterBottles.script"));
        //Algorithm a = new Yices(compile("WaterBottles.script"));
        //Benchmark b = new Benchmark(new Algorithm[] {a});
        //b.start();
    }
}