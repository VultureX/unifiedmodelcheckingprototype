/*
 * Timer.java
 * Copyright(c) 2014
 */
package Tools;

import java.text.DecimalFormat;

/**
 *
 * @author Peter Maandag
 */
public class Timer {
    
    private long start;
    private long end;
    private boolean isStopped;
    private boolean isStarted;
    private static final DecimalFormat format = new DecimalFormat("#.##");
    
    public Timer() {
        isStopped = false;
        isStarted = false;
    }
    
    public void restart() {
        start = System.nanoTime();
        isStopped = false;
        isStarted = true;
    }
    
    public void stop() {
        end = System.nanoTime();
        isStopped = true;
    }
    
    public boolean isStopped() {
        return isStopped;
    }
    
    public String getFormattedElapsedSeconds() {
        return format.format(getElapsedSeconds());
    }
    
    public static String getFormattedTime(double time) {
        return format.format(time);        
    }
    
    public double getElapsedSeconds() {
        if(!isStarted) {
            return 0.0;
        }
        
        if(!isStopped) {
            end = System.nanoTime();
        }
        long elapsedTime = end - start;
        double seconds = (double)elapsedTime / 1000000000.0;
        return seconds;
    }
}
