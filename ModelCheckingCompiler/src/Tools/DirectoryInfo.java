/*
 * DirectoryInfo.java
 * Copyright(c) 2014
 */

package Tools;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public final class DirectoryInfo {
    public final String dir;
    
    public DirectoryInfo(String dir) {
        this.dir = formatDir(dir);
    }
    
    private String formatDir(String dir) {
        try {
            File f = new File(dir);
            return f.getCanonicalPath() + File.separator;
        } catch (IOException ex) {
            Logger.getLogger(FileInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dir;
    }
    
    public File getFile() {
        return new File(dir);
    }
    
    @Override
    public String toString() {
        return dir;
    }
}
