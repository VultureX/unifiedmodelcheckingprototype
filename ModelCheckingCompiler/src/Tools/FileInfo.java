/*
 * FileInfo.java
 * Copyright(c) 2014
 */

package Tools;

import java.io.File;

/**
 *
 * @author Peter Maandag
 */
public final class FileInfo {
    public final String relativeDir;
    public final DirectoryInfo relativeDirInfo;
    public final String fileName;
    
    public FileInfo(String relativeDir, String fileName) {
        this.relativeDirInfo = new DirectoryInfo(relativeDir);
        this.relativeDir = relativeDirInfo.dir;
        this.fileName = fileName;
    }
    
    public FileInfo(DirectoryInfo relativeDir, String fileName) {
        this.relativeDirInfo = relativeDir;
        this.relativeDir = relativeDir.dir;
        this.fileName = fileName;
    }
    
    public File getFile() {
        return new File(relativeDir + fileName);
    }
    
    public File getDirFile() {
        return new File(relativeDir);
    }
    
    @Override
    public String toString() {
        return relativeDir + fileName;
    }
}
