/*
 * FileReader.java
 * Copyright(c) 2014
 */
package Tools;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public class FileReader {
    
    private static byte[] readFileBytes(FileInfo file) {
        byte[] filearray = null;
        try {            
            Path path = FileSystems.getDefault().getPath(file.relativeDir, file.fileName);
            return Files.readAllBytes(path);            
        } catch (IOException ex) {
            Logger.getLogger(FileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return filearray;
    }
    
    /*public static String readFile(String filename) {
        //Get path in the current directory
        byte[] filearray = readFileBytes("", filename);
        return new String(filearray);
    }*/
    
    public static String readFile(FileInfo file) {
        byte[] filearray = readFileBytes(file);
        try {
            return new String(filearray, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(FileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }    
}
