/*
 * Utils.java
 * Copyright(c) 2014
 */
package Tools;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public class Utils {
    public static final String newLine = System.getProperty("line.separator");
    public static final String executionPath = System.getProperty("user.dir");
    
    public static void pauseConsole() {
        pauseConsole("Press enter to continue...");
    }
    
    public static void pauseConsole(String msg) {
        try {
            System.out.print(msg);
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static <T> void printArray(T[] array) {
        for(T t : array) {
            System.out.println(t);
        }
        System.out.println();
    }
    
    /**
     * This function will return the superclass of subClass that directly extends baseClass
     * For example if Object is the baseClass and there is a class MyClass that extends Object 
     * and a class MyExtendedClass that extends MyClass, then 
     * getSuperClassThatExtends(Object.class, MyExtendedClass.class) will return MyClass.class
     * if subClass already directly extends baseClass or subClass.equals(baseClass)
     * then subClass is returned
     * @param <T> Type parameter to make sure your function call makes sense
     * @param baseClass
     * @param subClass
     * @return a class of which baseClass is a super Class (or the same class)
     */
    public static <T> Class<? super T> getSuperClassThatExtends(Class<T> baseClass, Class<? extends T> subClass) {        
        Class superClass = subClass;
        Class returnClass = superClass;
        while(superClass != null && !superClass.equals(baseClass)) {
            returnClass = superClass;
            superClass = superClass.getSuperclass();
        }
        
        return returnClass;
    }
    
    public static <T> T getNewInstanceFromArgs(Class<T> clazz, Class<?>[] parameterTypes, Object[] initArgs) {
        try {
            Constructor<T> ctor = clazz.getConstructor(parameterTypes);
            T instance = ctor.newInstance(initArgs);
            return instance;
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException 
                | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    //@Todo: make this better?
    /*public static String formatDir(String dir) throws IOException {
        File f = new File(dir);
        return f.getCanonicalPath() + File.separator;
    }*/
    
    /*public static <T> Class<T> getSuperClass(Class subClass, Class<T> goalClass) {
        Class superClass = subClass.getSuperclass();
        while(superClass != null && !superClass.equals(goalClass)) {
            superClass = superClass.getSuperclass();
        }
        return superClass;
    }*/
    
    public static <T> T[] concatenate(T[] left, T[] right) {
        //Create new instance:
        //T[] sum = Arrays.copyOfRange(left, 0, 0);
        //sum = Arrays.copyOf(sum, left.length + right.length);
       
        Class arrayType = left.getClass().getComponentType();        
        T[] sum = (T[]) Array.newInstance(arrayType, left.length + right.length);        
        System.arraycopy(left, 0, sum, 0, left.length);
        System.arraycopy(right, 0, sum, left.length, right.length);
        return sum;
    }
    
    public static <T> T[] concatenate(T[]... stuff) {
        T[] result = stuff[0];
        for(int i = 1; i< stuff.length; i++) {
            result = concatenate(result, stuff[i]);
        }
        return result;
    }
    
    public static <T> T[] concatenate(T element, T[] array) {
        Class arrayType = array.getClass().getComponentType();        
        T[] sum = (T[]) Array.newInstance(arrayType, array.length + 1);
        System.arraycopy(array, 0, sum, 1, array.length);
        sum[0] = element;
        return sum;
    }
    
    public static <T> T[] concatenate(T[] array, T element) {
        Class arrayType = array.getClass().getComponentType();        
        T[] sum = (T[]) Array.newInstance(arrayType, array.length + 1);        
        System.arraycopy(array, 0, sum, 0, array.length);
        sum[array.length] = element;
        return sum;
    }
    
    public static <T> boolean contains(T[] array, T element) {
        for (T e : array) {
            if (e.equals(element)) {
                return true;
            }
        }
        return false;
    }
}
