/*
 * Lambda.java
 * Copyright(c) 2014
 */
package Tools;

/**
 * This class is a collection of functional interfaces to be used in a fashion
 * much like the Action and Function delegates in C#.
 * @author Peter Maandag
 */
public final class Lambda {
    /**
     * Encapsulates a method that has no parameters and does not return a value.
     */
    @FunctionalInterface
    public interface Action0 {
        void invoke();
    }
    
    /**
     * Encapsulates a method that has no parameters and does not return a value.
     * It allows to throw any checked exception
     * @param <E> the type of the exception the action may throw
     */
    @FunctionalInterface
    public interface Action0E<E extends Throwable> {
        void invoke() throws E;
    }
    
    /**
     * Encapsulates a method that has one parameter and does not return a value.
     * @param <A> the type of the input argument to the action
     */
    @FunctionalInterface
    public interface Action1<A> {
        void invoke(A arg);
    }
    
    /**
     * Encapsulates a method that has two parameters and does not return a value.
     * @param <A> the type of the first input argument to the action
     * @param <B> the type of the second input argument to the action
     */
    @FunctionalInterface
    public interface Action2<A, B> {
        void invoke(A arg1, B arg2);
    }
    
    /**
     * Encapsulates a method that has one parameter and does not return a value.
     * It allows to throw any checked exception
     * @param <A> the type of the input argument to the action
     * @param <E> the type of the exception the action may throw
     */
    @FunctionalInterface
    public interface Action1E<A, E extends Throwable> {
        void invoke(A arg) throws E;
    }
    
    /**
     * Encapsulates a method that has two parameters and does not return a value.
     * It allows to throw any checked exception
     * @param <A> the type of the first input argument to the action
     * @param <B> the type of the second input argument to the action
     * @param <E> the type of the exception the action may throw
     */
    @FunctionalInterface
    public interface Action2E<A, B, E extends Throwable> {
        void invoke(A arg1, B arg2) throws E;
    }
    
    /**
     * Encapsulates a method that has three parameters and does not return a value.
     * It allows to throw any checked exception
     * @param <A> the type of the first input argument to the action
     * @param <B> the type of the second input argument to the action
     * @param <C> the type of the third input argument to the action
     * @param <E> the type of the exception the action may throw
     */
    @FunctionalInterface
    public interface Action3E<A, B, C, E extends Throwable> {
        void invoke(A arg1, B arg2, C arg3) throws E;
    }
    
    /**
     * Encapsulates a method that has no parameters and returns a value of type R
     * @param <R> The type of the returned value
     */
    @FunctionalInterface
    public interface Function0<R> {
        R invoke();
    }
    
    /**
     * Encapsulates a method that has no parameters and returns a value
     * It allows to throw any checked exception
     * @param <R> The type of the returned value
     * @param <E> The type of the exception the function may throw
     */
    @FunctionalInterface
    public interface Function0E<R, E extends Throwable> {
        R invoke() throws E;
    }
    
    /**
     * Encapsulates a method that has one parameter and returns a value
     * @param <A> The type of the input argument to the function
     * @param <R> The type of the returned value
     */
    @FunctionalInterface
    public interface Function1<A, R> {
        R invoke(A arg);
    }
    
    /**
     * Encapsulates a method that has one parameter and returns a value
     * It allows to throw any checked exception
     * @param <A> The type of the input argument to the function
     * @param <R> The type of the returned value
     * @param <E> The type of the exception the function may throw
     */
    @FunctionalInterface
    public interface Function1E<A, R, E extends Throwable> {
        R invoke(A arg) throws E;
    }
    
    public static <A,T extends Throwable> void forEach(A[] array, 
            Action1E<A,T> action) throws T {
        for(A a: array) {
            action.invoke(a);
        }
    }
    
    public static <A,B,C,T extends Throwable> void forEach(A[] array, B arg2, 
            C arg3, Action3E<A,B,C,T> action) throws T {
        for(A a: array) {
            action.invoke(a, arg2, arg3);
        }
    }
    
    public static <T extends Throwable> void repeat(int n, Action0 action) throws T {
        for(int i = 0; i < n; i++) {
            action.invoke();
        }
    }
    
    /*public static void repeat(int n, Action1<Integer> action) {
        for(int i = 0; i < n; i++) {
            action.invoke(i);
        }
    }*/
    
    public static <T extends Throwable> void repeat(int n, Action1E<Integer, T> action) throws T {
        for(int i = 0; i < n; i++) {
            action.invoke(i);
        }
    }
    
    public static <T extends Throwable> void repeat(int n1, int n2, Action2E<Integer, Integer, T> action) throws T {
        for(int i = 0; i < n1; i++) {
            for(int j = 0; i < n2; i++) {
                action.invoke(i,j);
            }
        }
    }
    
    public static <T extends Throwable> Action0 wrap(Action0E<T> callable) {
        return () -> {
            try {
                callable.invoke();
            } catch (Throwable ex) {
                if(ex instanceof RuntimeException) {
                    throw (RuntimeException) ex;
                }
                throw new RuntimeException(ex);
            }
        };
    }
    
    public static <A, T extends Throwable> Action1<A> wrap(Action1E<A,T> callable) {
        return (a) -> {
            try {
                callable.invoke(a);
            } catch (Throwable ex) {
                if(ex instanceof RuntimeException) {
                    throw (RuntimeException) ex;
                }
                throw new RuntimeException(ex);
            }
        };
    }
}
