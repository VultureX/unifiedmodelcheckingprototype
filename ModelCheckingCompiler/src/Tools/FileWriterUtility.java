/*
 * FileWriterUtility.java
 * Copyright(c) 2014
 */
package Tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public class FileWriterUtility {
    
    private BufferedWriter bw;
    private boolean isClosed;
    
    public FileWriterUtility(FileInfo file) throws IOException {
        this(file, false);
    }

    public FileWriterUtility(FileInfo file, boolean checkExists) throws IOException {
        File f = file.getFile();
        if(f.exists()) {
            if(checkExists) {
                throw new IOException(f.getCanonicalPath() + " already exists!");
            }
            f.delete();
        }
        //@todo: We get an exception here if the a double folder sturcture doesn't exist!
        f.createNewFile();
        FileWriter fw = new FileWriter(f.getAbsoluteFile());
        this.bw = new BufferedWriter(fw);
        this.isClosed = false;
    }

    public void write(String content) throws IOException {
        bw.write(content);
    }
    
    public boolean isClosed() {
        return isClosed;
    }

    public void close() {
        try {
            this.isClosed = true;
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileWriterUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Utility Functions
    public static void writeFile(FileInfo file, String content) {
        createNew(file);
        appendFile(file, content);
    }
    
    public static void createNew(FileInfo file) {
        Path path = FileSystems.getDefault().getPath(file.relativeDir, file.fileName);
        try {
            Files.deleteIfExists(path);
            Files.createFile(path);
        } catch (IOException ex) {
            Logger.getLogger(FileWriterUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void appendFile(FileInfo file, String content) {
        try {
            Files.write( FileSystems.getDefault().getPath(file.relativeDir, file.fileName), 
                         content.getBytes(), 
                         StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException ex) {
            Logger.getLogger(FileWriterUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void copyFile(FileInfo file, DirectoryInfo copyDir) {
        try {
            Files.copy(FileSystems.getDefault().getPath(file.relativeDir, file.fileName),
                    FileSystems.getDefault().getPath(copyDir.dir, file.fileName));
        } catch (IOException ex) {
            Logger.getLogger(FileWriterUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static boolean moveFile(FileInfo file, DirectoryInfo copyDir) {
        try {
            Files.move(FileSystems.getDefault().getPath(file.relativeDir, file.fileName),
                    FileSystems.getDefault().getPath(copyDir.dir, file.fileName));
            return true;
        } catch (IOException ex) {
            Logger.getLogger(FileWriterUtility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
