/*
 * ParallelVariable.java
 * Copyright(c) 2014
 */

package Scripts;

import java.io.IOException;

/**
 *
 * @author Peter Maandag
 */
public class ParallelVariable extends Script {
    
    private final int n, maxSteps, startValue, goalValue, maxValue;
    
    public ParallelVariable(int n, int startValue, int goalValue, int maxValue, int maxSteps) {
        this.n = n; //Number of parallel processes
        this.maxSteps = maxSteps; //Maximum search depth
        this.startValue = startValue; //The initial value of the global variable
        this.goalValue = goalValue; //Goal value for the global variable
        this.maxValue = maxValue; //Maximum value that global variable can reach
    }
    
    @Override
    protected String getFileDescription() {
        return "n" + n + "_s" + maxSteps + "_i" + startValue + "_g" + goalValue + "_m" + maxValue;
    }

    @Override
    protected void generate() throws IOException {
        if(maxSteps != -1) {
            writeln("MAXSTEPS:");
            writetln("" + this.maxSteps);
        }
        
        //generateTS();
        generatePS();
    }
    
    private void generateTS() throws IOException {
        writeln("VARS:");
        for(int i = 0; i<n; i++) {
            writetln("int s" + i + " : 0..2;");
        }
        for(int i = 0; i<n; i++) {
            writetln("int v" + i + " : 0.." + maxValue + ";");
        }
        writetln("int c : 0.." + maxValue + ";");
        
        writeln("INIT:");
        for(int i = 0; i<n; i++) {
            writetln("s" + i + " = 0;");
        }
        for(int i = 0; i<n; i++) {
            writetln("v" + i + " = 0;");
        }
        writetln("c = " + startValue + ";");
        
        writeln("TRANS:");
        for(int i = 0; i<n; i++) {
            String s = "s" + i;
            String v = "v" + i;
            writetln("? " + s + " == 0 -> " + v + "," + s + " = c,1;");
            writetln("? " + s + " == 1 -> " + v + "," + s + " = " + v + "+" + "c,2;");
            writetln("? " + s + " == 2 -> c," + s + " = " + v + ",0;");
        }       
        
        writeln("REACH:");
        writetln("c == " + goalValue);
    }
    
    private void generatePS() throws IOException {
        writeln("VARS:");
        writetln("int c : 0.." + maxValue + ";");
        for(int i = 0; i<n; i++) {
            writetln("proc p" + i + " = p;");
        }
        
        writeln("INIT:");
        writetln("c = " + startValue + ";");
        
        writeln("PROGRAM:");
        writet("");
        for(int i = 0; i<n-1; i++) {
            write("p" + i + " || ");
        }
        writeln("p" + (n-1));
        
        writeln("REACH:");
        writetln("c == " + goalValue);
        
        writeln("PROCESS p:");
        writeln("VARS:");
        writetln("int s : 0..2;");
        writetln("int v : 0.." + maxValue + ";");
        writeln("INIT:");
        writetln("s = 0;");
        writetln("v = 0;");
        writeln("TRANS:");
        writetln("? s == 0 -> v,s = c,1;");
        writetln("? s == 1 -> v,s = v+c,2;");
        writetln("? s == 2 -> c,s = v,0;");
    }
}
