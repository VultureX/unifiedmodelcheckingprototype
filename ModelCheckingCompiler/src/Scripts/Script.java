/*
 * Script.java
 * Copyright(c) 2014
 */
package Scripts;

import GUI.MainWindow;
import Tools.FileInfo;
import Tools.FileWriterUtility;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public abstract class Script {
    
    private FileWriterUtility fw;
    
    public FileInfo generate(FileInfo file) {
        int dot = file.fileName.indexOf('.');
        if(dot != -1) {
            String filename = file.fileName;
            filename = filename.substring(0, dot) + "_" + getFileDescription() + filename.substring(dot, filename.length());
            file = new FileInfo(file.relativeDir, filename);
        }
            
        try {            
            fw = new FileWriterUtility(file);
            MainWindow.getConsole().println("Generating " + file.fileName + "...");
            generate();
            MainWindow.getConsole().println("Done!", Color.GREEN);
        } catch (IOException ex) {
            MainWindow.getConsole().println("Script generation failed: " + ex.getMessage(), Color.RED);
            Logger.getLogger(Scripts.Script.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fw.close();
        }
        
        return file;
    }
    
    /*protected <T extends Throwable> void writeUTS (Action0E<T> writeMAXSTEPS,
            Action0E<T> writeVARS, Action0E<T> writeINIT, Action0E<T> writeTRANS,
            Action0E<T> writeGOAL) {
        
    }*/
    
    protected void MAXSTEPS(int n) throws IOException {
        writeln("MAXSTEPS:");
        writetln("" + n);
    }
    
    protected void ass(String ID, String value) throws IOException {
        writetln(ID + " = " + value + ";");
    }
    
    protected void ci(String ID, int c) throws IOException {         
        writetln("const int " + ID + " = " + c + ";");
    }
    
    protected void i(String ID, String a, String b) throws IOException {
        writetln("int " + ID + " : " + a + ".." + b + ";");
    }
    
    protected void writet(String s) throws IOException {
        fw.write("\t" + s);
    }
    
    protected void writetln(String s) throws IOException {
        fw.write("\t" + s + newLine);
    }
    
    protected void write(String s) throws IOException {
        fw.write(s);
    }
    
    protected void writeln(String s) throws IOException {
        fw.write(s + newLine);
    }
    
    protected abstract void generate() throws IOException;
    /**
     * This description will be appended to the file name that is passed to
     * the generate function
     * @return a short description that describes with what arguments this script
     * was instantiated 
     */
    protected abstract String getFileDescription();
}
