/*
 * ABP.java
 * Copyright(c) 2014
 */

package Scripts;

import java.io.IOException;

/**
 *
 * @author Peter Maandag
 */
public class ABP extends Script {
    
    public enum DataType {
        Int, Bool;
        
        @Override
        public String toString() {
            switch(this) {
                case Int:
                    return "int";
                case Bool:
                    return "bool";
                default:
                    return null;
            }
        }
    }
    
    private int size;
    private DataType type;
    private boolean fixed;
    public static final int MAXINT = 1023;
    private int maxSteps;
    
    public ABP(int size, DataType type, boolean fixed, int maxSteps) {
        this.size = size;
        this.type = type;
        this.fixed = fixed;
        this.maxSteps = maxSteps;
    }
    
    @Override
    protected String getFileDescription() {
        return type.toString() + "_" + size + (!fixed ? "_any" : "") + "_M" + maxSteps;
    }
    
    private void finishType() throws IOException {
        if(type == DataType.Int) {
            write(" : 0.." + MAXINT);
        }
        writeln(";");
    }

    @Override
    protected void generate() throws IOException {
        if(this.maxSteps != -1) {
            writeln("MAXSTEPS:");
            writetln("" + this.maxSteps);
        }
        
        writeln("VARS:");
        writetln("bool Sb; bool S2Rb; bool Rb; bool R2Sb; bool S2Rf; bool R2Sf;");
        writet(type.toString() + " S2Rd");
        finishType();
        for(int i = 0; i<size; i++) {
            writet(type.toString() + " Sd" + i);
            finishType();
        }
        for(int i = 0; i<size; i++) {
            writet(type.toString() + " Rd" + i);
            finishType();
        }
        writetln("int Snum : 0.." + size + ";");
        writetln("int Rnum : 0.." + size + ";");
        
        writeln("INIT:");
        writetln("Snum = 0; Rnum = 0; S2Rf = false; R2Sf = false; Sb = false; Rb = false;");
        
        //Completely specified start state:
        if(fixed) {
            writetln("S2Rb = false; R2Sb = false;");
            writetln("S2Rd = " + ((type == DataType.Bool) ? "false" : "0") + ";");
            for(int i = 0; i<size; i++) {
                String init = (type == DataType.Bool) ? "false" : "0";
                writetln("Sd" + i + " = "  + init + ";");
            }
            for(int i = 0; i<size; i++) {
                String init = (type == DataType.Bool) ? "true" : "1";
                writetln("Rd" + i + " = "  + init + ";");
            }
        }
        
        writeln("TRANS:");
        writetln("S2Rf = false; R2Sf = false;");
        for(int i = 0; i<size; i++) {
            writetln("? Snum == " + i + " -> S2Rb,S2Rd,S2Rf = Sb,Sd" + i + ",true;");
        }
        writetln("R2Sb,R2Sf = !Rb,true;");
        writetln("? R2Sf & R2Sb == Sb -> Snum,Sb = Snum+1,!Sb;");
        for(int i = 0; i<size; i++) {
            writetln("? S2Rf & S2Rb == Rb & Snum == " + i + " -> Rd" + i + ",Rb,Rnum = S2Rd,!Rb,Rnum+1;");
        }
        
        writeln("REACH:");
        writet("Snum == " + size + " & Rnum == " + size);
        
        write(" & (");
        if(type == DataType.Int) {
            for(int i = 0; i<size; i++) {
                write("Sd" + i + " > Rd" + i + "| Sd" + i + " < Rd" + i);
                if(i < size-1) {
                    write(" | ");
                }
            }
        } else {
            for(int i = 0; i<size; i++) {
                write("Sd" + i + " == !Rd" + i);
                if(i < size-1) {
                    write(" | ");
                }
            }           
        }
        writeln(")");       
    }
}
