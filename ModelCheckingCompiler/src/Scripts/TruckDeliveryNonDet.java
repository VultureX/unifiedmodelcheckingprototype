/*
 * TruckDeliveryNonDet.java
 * Copyright(c) 2014
 */

package Scripts;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public class TruckDeliveryNonDet extends TruckDelivery {
    
    public TruckDeliveryNonDet(State[] states, State beginState, int maxTruck, int maxSteps) {
        super(states, beginState, maxTruck, maxSteps);
    }
    
    @Override
    protected String getFileDescription() {
        return "NonDet_S" + states.length + "_E" + edges.size() + "_T" + maxTruck + "_B" + beginState.getName() + "_M" + maxSteps;
    }
    
    public static TruckDelivery getExampleInstance(int maxTruck, int maxSteps) {
        State s = new State(); //ReloadState
        State a = new State(50, 120);
        State b = new State(40, 120);
        State c = new State(150, 200);
        State[] theStates = new State[] {
            s,a,b,c
        };
        TruckDelivery t = new TruckDeliveryNonDet(theStates, s, maxTruck, maxSteps); 
        
        t.addEdge(s, a, 29);
        t.addEdge(s, b, 21);
        t.addEdge(a, s, 29);
        t.addEdge(a, b, 17);
        t.addEdge(a, c, 32);
        t.addEdge(b, s, 21);
        t.addEdge(b, a, 17);
        t.addEdge(b, c, 37);
        t.addEdge(c, a, 32);
        t.addEdge(c, b, 37);
        
        return t;
    }

    @Override
    protected void generate() throws IOException {
        if(maxSteps != -1) {
            writeln("MAXSTEPS:");
            writetln("" + this.maxSteps);
        }
        
        writeln("VARS:");
        writetln("int _state : 0.." + (states.length-1) + ";");
        for(State state : states) {
            if(!state.isReloadState()) {
                writetln("int " + state.getName() + " : 0..MAX" + state.getName() + ";");
            }
        }
        writetln("int truck : 0..MAX_TRUCK;");
        writetln("nondet int amount : 0..MAX_TRUCK;");
        for(State state : states) {
            if(!state.isReloadState()) {
                writetln("const int MAX" + state.getName() + " = " + state.getMaxCapacity() + ";");
            }
        }
        for(TruckDelivery.Edge edge : edges) {
            writetln("const int " + edge.name + " = " + edge.weight + ";");
        }
        writetln("const int MAX_TRUCK = " + maxTruck + ";");
        
        writeln("INIT:");
        writetln("_state = " + beginState.getNumber() + ";");
        for(State state : states) {
            if(!state.isReloadState()) {
                writetln(state.getName() + " = " + state.getInitialCapacity() + ";");
            }
        }
        writetln("truck = " + maxTruck + ";");
        
        writeln("TRANS:");
        //Write all edges:
        for(TruckDelivery.Edge e : edges) {
            String edge = e.name;
            String from = e.from.getName();
            String to = e.to.getName();
            ArrayList<Integer> reloadStates = new ArrayList<>();
            
            //To reload state transition:
            if(e.to.isReloadState() && !reloadStates.contains(e.from.getNumber())) {
                writetln("? _state == " + e.from.getNumber());
                writet("-> _state,");
                for(State state : states) {
                    if(!state.isReloadState()) {
                        write(state.getName() + ",");
                    }
                }
                write("truck = " + e.to.getNumber() + ",");
                for(State state : states) {
                    if(!state.isReloadState()) {
                        write(state.getName() + "-" + edge + ",");
                    }
                }
                writeln("MAX_TRUCK;");
                reloadStates.add(e.from.getNumber());
            }
            
            if(!e.to.isReloadState()) {
                //Fill all transition
                writetln("? " + to + "-" + edge + " >= 0 & _state == " + e.from.getNumber());
                writet("-> _state," + to + ",");
                for(State state : states) {
                    if(!state.equals(e.to) && !state.isReloadState()) {
                        write(state.getName() + ",");
                    }                
                }
                write("truck = " + e.to.getNumber() + ",(" + to + " - " + edge + ") + amount,");
                for(State state : states) {
                    if(!state.equals(e.to) && !state.isReloadState()) {
                        write(state.getName() + "-" + edge + ",");
                    }                
                }
                writeln("truck - amount;");
            }
        }
        
        writeln("INF:");
    }

}
