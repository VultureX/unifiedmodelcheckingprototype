/*
 * TruckDelivery.java
 * Copyright(c) 2014
 */
package Scripts;

import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public abstract class TruckDelivery extends Script {
    
    protected State[] states;
    protected ArrayList<Edge> edges;
    protected int maxTruck;
    protected State beginState;
    protected int maxSteps;
    
    public TruckDelivery(State[] states, State beginState, int maxTruck, int maxSteps) {
        this.maxTruck = maxTruck;
        this.edges = new ArrayList<>();
        this.states = states;
        this.beginState = beginState;
        this.maxSteps = maxSteps;
    }
    
    public State[] getStates() {
        return states;
    }
    
    public void addEdge(State from, State to, int weight) {
        edges.add(new Edge(from, to, weight));
    }    
    
    public static class State {
        public static int count = 0;
        private String name;
        private int initialCapacity, maxCapacity;
        private int number;
        private boolean isReloadState;
        
        public State() {
            this(-1,-1);
            isReloadState = true;
        }
        
        public State(int initialCapacity, int maxCapacity) {
            this.initialCapacity = initialCapacity;
            this.maxCapacity = maxCapacity;
            this.name = "S" + count;
            this.number = count;
            count++;
        }
        
        public boolean equals(State other) {
            return this.name.equals(other.name);
        }
        
        public int getMaxCapacity() {
            return maxCapacity;
        }
        
        public int getInitialCapacity() {
            return initialCapacity;
        }
        
        public int getNumber() {
            return number;
        }
        
        public String getName() {
            return name;
        }
        
        public boolean isReloadState() {
            return isReloadState;
        }
    }
    
    protected class Edge {
        protected State from, to;
        protected int weight;
        protected String name;
        public Edge(State from, State to, int weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
            this.name = from.getName() + "_" + to.getName();
        }
    }
    
}
