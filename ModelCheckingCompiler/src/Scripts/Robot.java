/*
 * Robot.java
 * Copyright(c) 2014
 */

package Scripts;

import Tools.Lambda;
import Tools.Lambda.Action3E;
import java.io.IOException;

/**
 *
 * @author Peter Maandag
 */
public class Robot extends Script {
    /** Directions that the robot is facing (North, East, South, West) */
    public enum Dir {
        N, E, S, W;
    }
    
    /** Actions that the robot can take in a cell */
    public enum Action {
        UNDEF, TURN, FORWARD
    }
    
    /** Grid size */
    private final int n;
    /** Robot's current coordinates */
    private final int x,y;
    /** Direction that the robot is facing */
    private final Dir d;
    /** Maximum number of steps, -1 indicates that the MAXSTEPS block should
     not be generated*/
    private final int maxSteps;
    
    final String N = Dir.N.name();
    final String E = Dir.E.name();
    final String S = Dir.S.name();
    final String W = Dir.W.name();
    final String UNDEF = Action.UNDEF.name();
    final String TURN = Action.TURN.name();
    final String FORWARD = Action.FORWARD.name();
    
    /**
     * Robot vaccuum cleaner script. The robot starts at cell (x,y) facing a
     * particular direction d in an n x n grid.
     * @param n determines n x n grid size
     * @param x horizontal starting coordinate in [0,n>
     * @param y vertical starting coordinate in [0,n>
     * @param d initial direction the robot is facing
     * @param maxSteps maximum number of actions the robot may execute. 
     * -1 indicates an infinite amount of steps
     */
    public Robot(int n, int x, int y, Dir d, int maxSteps) {
        if(maxSteps < -1 || maxSteps == 0) {
            throw new IllegalArgumentException("Invalid number of steps");
        }
        if(n <= 0) {
            throw new IllegalArgumentException("Invalid grid size");
        }
        if(x < 0 || y < 0 || x >= n || y >= n) {
            throw new IllegalArgumentException("Invalid starting coordinates");
        }
        
        this.n = n;
        this.x = x; this.y = y; this.d = d;
        this.maxSteps = maxSteps;
    }
    
    private void writeStrategy(Action3E<Integer, Integer, Dir, IOException> action) throws IOException {
        Lambda.repeat(n, i -> 
            Lambda.repeat(n, j -> 
                Lambda.forEach(Dir.values(), dir -> 
                        action.invoke(i,j,dir))));
    }
    
    private String var(int x, int y, Dir dir) {
        return "x"+x+"y"+y+dir.name();
    }
    
    private void trans(int x, int y, Dir dir, Action a1, String _x, String _y, String _dir, Action a2) throws IOException {
        String v = var(x,y,dir);
        writetln("? x == " + x + " & y == " + y + " & d == " + dir.name() + 
                    " & !(" + v + " == " + a1.name() + ") -> x,y,d," + v +
                    " = " + _x + "," + _y + "," + _dir + "," + a2.name() + ";");
    }
    
    private void transForward(int x, int y, Dir dir) throws IOException {
        switch(dir) {
            case N:
                if(y != n-1) {
                    trans(x,y,dir, Action.TURN, "x", ((Integer)(y+1)).toString(), "d", Action.FORWARD);
                }
                break;
            case E:
                if(x != n-1) {
                    trans(x,y,dir, Action.TURN, ((Integer)(x+1)).toString(), "y", "d", Action.FORWARD);
                }
                break;
            case S:
                if(y > 0) {
                    trans(x,y,dir, Action.TURN, "x", ((Integer)(y-1)).toString(), "d", Action.FORWARD);
                }
                break;
            case W:
                if(x > 0) {
                    trans(x,y,dir, Action.TURN, ((Integer)(x-1)).toString(), "y", "d", Action.FORWARD);
                }
                break;
        }        
    }
    
    private void transTurn(int x, int y, Dir dir) throws IOException {
        switch(dir) {
            case N:
                trans(x,y,dir, Action.FORWARD, "x", "y", E, Action.TURN);
                break;                
            case E:
                trans(x,y,dir, Action.FORWARD, "x", "y", S, Action.TURN);
                break;                
            case S:
                trans(x,y,dir, Action.FORWARD, "x", "y", W, Action.TURN);
                break;                
            case W:
                trans(x,y,dir, Action.FORWARD, "x", "y", N, Action.TURN);
                break;
        }
    }
    
    @Override
    protected void generate() throws IOException {       
        if(maxSteps != -1) {
            MAXSTEPS(maxSteps);
        }        

        writeln("VARS:");
        ci("n", n);        
        Lambda.forEach(Action.values(), a -> ci(a.name(), a.ordinal()));
        Lambda.forEach(Dir.values(), dir -> ci(dir.name(), dir.ordinal()));
        ci("MAX", n-1);
        i("x", "0", "MAX");
        i("y", "0", "MAX");
        i("d", N, W);
        
        writeStrategy((i,j,dir) -> i(var(i,j,dir), UNDEF, FORWARD));     
        
        
        writeln("INIT:");
        writetln("x = " + x + ";");
        writetln("y = " + y + ";");
        writetln("d = " + d.name() + ";");
        writeStrategy((i,j,dir) -> ass(var(i,j,dir), UNDEF));
        
        writeln("TRANS:");
        writeStrategy(this::transForward);
        writeStrategy(this::transTurn);
        
        writeln("REACH:");
        for (int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                int k = 0;
                write("\t");
                for(Dir dir : Dir.values()) {
                    if(k == 0) {
                        write("!(");
                    }
                    write(var(i,j,dir) + " == " + UNDEF);
                    if(k < Dir.values().length -1) {
                        write(" & ");
                    } else {
                        writeln(") & ");
                    }                    
                    k++;
                }
            }
        }
        
        writetln("x == " + x + " & y == " + y + " & d == " + d.name());
    }

    @Override
    protected String getFileDescription() {
        return "n" + n + "_x" + x + "_y" + y + "_d" + d + "_m" + maxSteps;
    }
    
    /*for(Action a : Action.values()) {
        ci(a.name(), a.ordinal());
    }

    for(Dir dir : Dir.values()) {
        ci(dir.name(), dir.ordinal());
    }       

    for (int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {                
            Lambda.forEach(Dir.values(), i, j, 
                    (dir, a, b) -> i("x"+a+"y"+b+dir.name(), UNDEF, FORWARD));
            for(Dir dir : Dir.values()) {
                i("x"+i+"y"+j+dir.name(), UNDEF, FORWARD);
            }                
        }
    }*/

}
