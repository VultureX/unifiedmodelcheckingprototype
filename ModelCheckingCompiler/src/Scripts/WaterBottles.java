/*
 * WaterBottles.java
 * Copyright(c) 2014
 */
package Scripts;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public class WaterBottles extends Script {
    
    private int maxSteps;
    private int goalValue;
    private int[] capacities;
    private ArrayList<String> variables;
    
    public WaterBottles(int[] capacities, int maxSteps, int goalValue) {
        this.maxSteps = maxSteps; //Maximum search depth
        this.capacities = capacities; //Maximum capacity for each bottle
        this.goalValue = goalValue; //Desired value in one of the bottles
        this.variables = new ArrayList<>();
        for(int i = 0; i<capacities.length; i++) {
            variables.add("b" + i);
        }
    }
    
    @Override
    protected String getFileDescription() {
        return "n" + capacities.length + "_s" + maxSteps;
    }

    @Override
    public void generate() throws IOException {
        writeln("MAXSTEPS:");
        writeln(Integer.toString(maxSteps));
        writeln("VARS:");
        for(int i = 0; i<capacities.length; i++) {
            int cap = capacities[i];
            String var = variables.get(i);
            writeln("const int c" + var + " = " + Integer.toString(cap) + ";");
        }
        for(String s : variables) {
            writeln("int " + s + " : 0..c" + s + ";");
        }
        writeln("INIT:");
        for(String s : variables) {
            writeln(s + " = 0;");
        }
        writeln("TRANS:");
        
        //Empty bottles:
        for(String s : variables) {
            writeln(s + " = 0;");
        }
        //Fill bottles:
        for(String s : variables) {
            writeln(s + " = c" + s + ";");
        }
        //Pour bottles:
        for(int i =0; i < variables.size(); i++) {
            for(int j = i+1; j < variables.size(); j++) {
                String x = variables.get(i);
                String y = variables.get(j);
                writeln("? " + x + " + " + y + " <= c" + y + " -> " +
                        x + "," + y + " = 0," + x + "+" + y + ";");
                writeln("? " + x + " + " + y + " <= c" + x + " -> " +
                        x + "," + y + " = " + x + "+" + y + ", 0;");
                writeln("? " + x + " + " + y + " > c" + y + " -> " +
                        x + "," + y + " = (" + x + "+" + y + ")-c" + y + ",c" + y + ";");
                writeln("? " + x + " + " + y + " > c" + x + " -> " +
                        x + "," + y + " = c" + x + ",(" + x + "+" + y + ")-c" + x + ";");
            }
        }
        writeln("REACH:");
        for(int i =0; i < variables.size(); i++) {
            String s = variables.get(i);
            write("(" + s + " == " + goalValue + ")");
            if(i < variables.size() -1) {
                write(" | ");
            }
        }
    }
    
}
