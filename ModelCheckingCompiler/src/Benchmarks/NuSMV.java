/*
 * NuSMV.java
 * Copyright(c) 2014
 */
package Benchmarks;

import CodeGeneration.AST.Formula;
import Tools.FileInfo;

/**
 *
 * @author Peter Maandag
 */
public class NuSMV extends ModelCheckingAlgorithm {
    public static final String name = "NuSMV";
    public static final String dir = "../NuSMV-2.5.4-x86_64-w64-mingw32/bin/";
    public static final String executable = "NuSMV.exe";
    
    public NuSMV(FileInfo[] sourceFiles, Formula.Mode mode) {
        super(name, sourceFiles, new FileInfo(dir, executable), mode);
    }
    
    public NuSMV(FileInfo sourceFile, Formula.Mode mode) {
        super(name, sourceFile, new FileInfo(dir, executable), mode);
    }

    @Override
    protected ProcessBuilder getProcess(FileInfo[] sourceFiles) {
        return new ProcessBuilder(algorithmFile.toString(), 
                sourceFiles[0].toString());
    }
    
    @Override
    protected boolean hasTrace(String line) {
        return line.endsWith("is false");
    }    
}
