/*
 * ModelCheckingAlgorithm.java
 * Copyright(c) 2014
 */

package Benchmarks;

import CodeGeneration.AST;
import CodeGeneration.AST.Formula;
import Tools.FileInfo;

/**
 *
 * @author Peter Maandag
 */
public abstract class ModelCheckingAlgorithm extends CMDAlgorithm {
    private Formula.Mode mode;
    private boolean hasTrace = false;
    private String result = "UNKNOWN";
    
    public ModelCheckingAlgorithm(String algorithmName, FileInfo sourceFile, 
            FileInfo algorithmFile, Formula.Mode mode) {
        super(algorithmName, sourceFile, algorithmFile);
        this.mode = mode;
    }
    
    public ModelCheckingAlgorithm(String algorithmName, FileInfo[] sourceFile, 
            FileInfo algorithmFile ,Formula.Mode mode) {
        super(algorithmName, sourceFile, algorithmFile);
        this.mode = mode;
    }
    
    protected abstract boolean hasTrace(String line);
    
    @Override
    protected void processOutput(String line) {
        super.processOutput(line);
        if(hasTrace) {
            return;
        }
        
        hasTrace = hasTrace(line);
        String[] first = {"STATE IS ", "", "", "LIVENESS PROPERTY IS ", "SAFETY PROPERTY IS "};
        boolean[] negate = {!hasTrace, !hasTrace, !hasTrace, hasTrace, hasTrace};
        String[] second = {"NOT ", "NO ", "NO ", "NOT ", "NOT "};
        String[] third = {"REACHABLE", "INFINITE PATH EXISTS", "DEADLOCK EXISTS", "VALID", "VALID"};
        
        int num;
        switch(mode) {
            case Reachability:
                num = 0;
                break;
            case Loop:
                num = 1;
                break;
            case Deadlock:
                num = 2;
                break;
            case Liveness:
                num = 3;
                break;
            case Safety:
                num = 4;
                break;
            default:
                System.err.println("Unknown property");
                return;
        }
        
        result = first[num] + (negate[num] ? second[num] : "") + third[num];
    }
    
    @Override
    public String getResult() {
        switch(killReason) {
            case KILL_TIMEOUT:
                return "TIMEOUT";
            case KILL_RESET:
            case USER_ABORT:
                return "ABORTED";
            case NOT_KILLED:
            default:
                return result;
        }
    }
}
