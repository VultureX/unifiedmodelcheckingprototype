/*
 * CMDAlgorithm.java
 * Copyright(c) 2014
 */
package Benchmarks;

import Tools.FileInfo;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static Tools.Utils.newLine;

/**
 *
 * @author Peter Maandag
 */
public abstract class CMDAlgorithm extends Algorithm {
    private String name; //Name of the algorithm
    //protected String directoryPath; //Directory of the Algorithm executable
    //private String[] fileNames; //The input source files
    //private String[] fileDirs; //Relative directories of the input source files
    protected FileInfo algorithmFile;
    private FileInfo[] sourceFiles;
    private Process p;
    private volatile boolean hasStopped = true;
    
    public final static int NOT_KILLED = 0;
    public final static int KILL_TIMEOUT = 1;
    public final static int USER_ABORT = 2;
    public final static int KILL_RESET = 3;
    protected volatile int killReason = NOT_KILLED;
    
    /**
     * @param algorithmName Descriptive name of the algorithm
     * @param sourceFileName The full name of input source file
     * @param sourceDir (Relative) directory of the input source file
     * @param toolDir Directory where of the tool executable
     */
    public CMDAlgorithm(String algorithmName, FileInfo sourceFile, FileInfo algorithmFile) {
        //this(algorithmName, new String[] { sourceFileName }, new String[] { sourceDir }, toolDir);
        this(algorithmName, new FileInfo[] { sourceFile }, algorithmFile);
    }
    
    public CMDAlgorithm(String algorithmName, FileInfo[] sourceFiles, FileInfo algorithmFile) {
        this.name = algorithmName;
        this.sourceFiles = sourceFiles;
        this.algorithmFile = algorithmFile;
        //this.fileNames = fileNames;
        //this.fileDirs = fileDirs;
        //this.directoryPath = directoryPath;
        
        /*Path p = Paths.get(directoryPath, fileNames[0]);
        if(!Files.exists(p) || !p.endsWith(".bat")) {
            generateBatch(fileNames[0], fileNames);
        }*/
    }
    
    @Override
    public void onStart(String name) {
        killReason = NOT_KILLED;
        super.onStart(name);
        super.printConsoles("Running " + getAlgorithmName() + " for " + sourceFiles[0] + "...");
        //console.println("Running " + getAlgorithmName() + " for " + sourceFiles[0] + "...");
    }
    
    @Override
    public void onShutdown(String name, String result, int exitCode, double elapsedTime) {
        super.printConsoles(newLine + name + " verification result: " + result, Color.BLUE.darker());
        super.onShutdown(name, result, exitCode, elapsedTime);
    }
    
    @Override
    public void onAborted(String name, String result, int exitCode, double elapsedTime) {
        super.printConsoles(newLine + name + " verification result: " + result, Color.BLUE.darker());
        super.onAborted(name, result, exitCode, elapsedTime);
    }
    
    @Override
    protected boolean reset() {
        //Make sure the algorithm is stopped
        boolean killed = this.kill(KILL_RESET);
        if(!killed) {
            return false;
        }
        this.hasStopped = false;
        return true;
    }

    @Override
    protected int execute() {
        try {
            //InputStream is;
            //BufferedReader reader;
            synchronized(this) {
                if(hasStopped) {
                    return -1;
                }
                //this.p = Runtime.getRuntime().exec("cmd.exe /c " + fileName, null, new File(directoryPath));
                //ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "start", fileName);
                //ProcessBuilder pb = new ProcessBuilder(new File(directoryPath).getCanonicalPath() + "\\NuSMV.exe", "..\\..\\WaterBottles.NuSMV");
                //StringWriter infos = new StringWriter();
                //StringWriter errors = new StringWriter();
                
                ProcessBuilder pb = getProcess(this.sourceFiles);
                if(pb == null) {
                    System.err.println("Process creation failed");
                    return 1;
                }
                pb.directory(this.algorithmFile.getDirFile());
                this.p = pb.start();
                
                //Threaded output such that this function does not hang on is.read()
                StreamBoozer seInfo = new StreamBoozer(p.getInputStream()/*, console*/); //new PrintWriter(infos, true));
                StreamBoozer seError = new StreamBoozer(p.getErrorStream()/*, console*/);//new PrintWriter(errors, true));
                seInfo.start();
                seError.start();
            }
            
            int returnCode = p.waitFor();
            hasStopped = true;
            return returnCode;
        } catch (IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        } catch (InterruptedException e) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, e);
            return 2;
        }
    }
    
    @Override
    public boolean kill(int reason) {
        synchronized(this) {
            killReason = reason;
            if(hasStopped) {
                return true; 
            }
            if(p != null) {
                p.destroy();
                try {
                    p.waitFor();
                } catch (InterruptedException ex) {
                    Logger.getLogger(CMDAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
                hasStopped = true;
                return true;

                //Only on Windows if destroy does not work:
                //Runtime rt = Runtime.getRuntime();                
                //Process proc = rt.exec("taskkill /F /IM " + this.getAlgorithmExecutable());
                //proc.waitFor();
            }
            return false;
        }
    }
    
    /*private void generateBatch(String filename, String[] filenames) {
        int index = filename.indexOf('.');
        String batchName = (index != -1) ? 
                filename.substring(0, filename.indexOf('.')) : filename;
        batchName += ".bat";
        FileWriterUtility.writeFile(directoryPath, batchName, getInstructions(filenames));
        this.fileName = batchName;
    }*/
    
    //protected abstract String getInstructions(String[] filenames);
    //protected abstract ProcessBuilder getProcess(String[] filenames, String[] relativeDirs);
    protected abstract ProcessBuilder getProcess(FileInfo[] sourceFiles);
    
    @Override
    public String getAlgorithmName(){
        return name;
    }
    
    @Override
    public String getExecutableFileName() {
        return algorithmFile.fileName;
    }
}
