/*
 * ExecutionNotifier.java
 * Copyright(c) 2014
 */

package Benchmarks;

/**
 *
 * @author Peter Maandag
 */
public interface ExecutionNotifier {
    public void onStart(String name);
    public void onProgress(String name, double elapsedTime);
    public void onShutdown(String name, String result, int exitCode, double elapsedTime);
    public void onAborted(String name, String result, int exitCode, double elapsedTime);
}
