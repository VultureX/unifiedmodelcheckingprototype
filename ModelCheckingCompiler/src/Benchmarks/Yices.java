/*
 * Yices.java
 * Copyright(c) 2014
 */
package Benchmarks;

import CodeGeneration.AST.Formula;
import Tools.FileInfo;

/**
 *
 * @author Peter Maandag
 */
public class Yices extends ModelCheckingAlgorithm {
    
    public static final String name = "Yices";
    public static final String dir = "../yices-1.0.40/bin/";
    public static final String executable = "yices.exe";
    
    public Yices(FileInfo[] sourceFiles, Formula.Mode mode) {
        super(name, sourceFiles, new FileInfo(dir, executable), mode);
    }
    
    public Yices(FileInfo sourceFile, Formula.Mode mode) {
        super(name, sourceFile, new FileInfo(dir, executable), mode);
    }

    @Override
    protected ProcessBuilder getProcess(FileInfo[] sourceFiles) {
        return new ProcessBuilder(algorithmFile.toString(), 
                "-e", "-smt", sourceFiles[0].toString());        
    }
    
    @Override
    protected boolean hasTrace(String line) {
        return line.equals("sat");
    }
}
