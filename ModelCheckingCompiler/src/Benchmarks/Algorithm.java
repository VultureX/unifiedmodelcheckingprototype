/*
 * Algorithm.java
 * Copyright(c) 2014
 */
package Benchmarks;

import GUI.Console.Console;
import Tools.Timer;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An algorithm that can be executed in a thread. Its progress and return values 
 * are pushed through ExecutionNotifiers that can be registered to this algorithm.
 * You can register an instance of the inner class DefaultExecutionNotifier to get
 * some default updates pushed to a Console.
 * Any output from an InputStream can be re-routed through the public inner class 
 * StreamBoozer and will be pushed through to Consoles that are registered to this
 * algorithm.
 * @author Peter Maandag
 */
public abstract class Algorithm extends Thread implements ExecutionNotifier {
    /**Interval in ms for onProgress events to be fired*/
    public static final int TIMEOUT = 5000;
    /**Timer that keeps track of the algorithm execution time*/
    private final Timer t = new Timer();
    /**List of notifiers that are registered to receive events from this algorithm*/
    private final ArrayList<ExecutionNotifier> notifiers = new ArrayList<>();
    /**List of consoles that are registered to this algorithm to receive algorithm InputStream output*/
    private final ArrayList<Console> consoles = new ArrayList<>();
    /**Whether the algorithm being executed right now*/
    private volatile boolean running = false;
    
    /**Un-threaded execution of the algorithm. In this function StreamBoozers may
     * be started to get output from an InputStream of the algorithm.
     * @return return code of the algorithm when it's completed. Will return 0
     * if completed successfully */
    protected abstract int execute();
    /**Resets the state of the algorithm, so that it can be started again
     * @return Whether or not the algorithm was successfully reset*/
    protected abstract boolean reset();
    /** Stops execution of the algorithm 
     * @return Whether or not the algorithm was successfully stopped. Returns true
     if the algorithm was already stopped*/
    public abstract boolean kill(int reason);
    /** @return A descriptive name of the algorithm*/
    public abstract String getAlgorithmName();
    /** @return The full name of the algorithm executable (including extension)*/
    public abstract String getExecutableFileName();
    
    /** @return Whether or not the algorithm is executing */
    public boolean isRunning() {
        return running;
    }
    
    /** Registers an ExecutionNotifier that receives events from this algorithm
     * when the thread starts running
     * @param e the ExecutionNotifier*/
    public void registerExecutionNotifier(ExecutionNotifier e) {
        if(!notifiers.contains(e)) {
            notifiers.add(e);
        }
    }
    
    /** Registers a Console that any other output of the Algorithm
     * @param c the Console*/
    public void registerConsole(Console c) {
        if(!consoles.contains(c)) {
            consoles.add(c);
        }
    }
    
    protected void processOutput(String line) {
        printConsoles(line);
    }
    
    protected void printConsoles(String line) {
        for(Console c : consoles) {
            c.println(line);
        }
    }
    
    protected void printConsoles(String line, Color color) {
        for(Console c : consoles) {
            c.println(line, color);
        }
    }
    
    public abstract String getResult();
    
    @Override
    public void onStart(String name) {
        for(ExecutionNotifier e : notifiers) {
            e.onStart(name);
        }
    }
    
    @Override
    public void onShutdown(String name, String result, int exitCode, double elapsedTime) {
        for(ExecutionNotifier e : notifiers) {
            e.onShutdown(name, result, exitCode, elapsedTime);
        }
    }
    
    @Override
    public void onAborted(String name, String result, int exitCode, double elapsedTime) {
        for(ExecutionNotifier e : notifiers) {
            e.onAborted(name, result, exitCode, elapsedTime);
        }
    }
    
    @Override
    public void onProgress(String name, double elapsedTime) {
        for(ExecutionNotifier e : notifiers) {
            e.onProgress(name, elapsedTime);
        }
    }
    
    @Override
    public void run() {
        if(!reset()) {
            this.onAborted(getAlgorithmName(), getResult(), -1, 0.0);
            return;
        }
        
        //Fire start event and start progress thread
        running = true;
        this.onStart(getAlgorithmName());
        Thread updateThread = new Thread(new UpdateThread());
        updateThread.setDaemon(true);
        updateThread.start();        
        
        //Execute the algorithm and monitor time
        t.restart();
        int exitCode = execute();
        t.stop();
        
        //Execution has completed, fire completed events
        //@Todo: join StreamBoozer threads in execute(), instead of waiting here;
        Tools.Utils.sleep(500); //Wait a bit for the last output to be flushed       
        if(exitCode != 0) {
            this.onAborted(getAlgorithmName(), getResult(), exitCode, t.getElapsedSeconds());
        } else {
            this.onShutdown(getAlgorithmName(), getResult(), exitCode,t.getElapsedSeconds());
        }        
        running = false;
    }
    
    public class DefaultExecutionNotifier implements ExecutionNotifier {
        private final Console console;
        public DefaultExecutionNotifier(Console console) {
            this.console = console;
        }
        
        @Override
        public void onStart(String name) {
            console.println("Starting " + getAlgorithmName() + "...");
        }
    
        @Override
        public void onShutdown(String name, String result, int exitCode, double elapsedTime) {
            Color c = (exitCode == 0) ? Color.GREEN.darker() : Color.ORANGE;
            String info = getAlgorithmName() + " completed with exit code " + exitCode + newLine
                    + "Execution time: " + t.getFormattedElapsedSeconds() + " seconds" + newLine;
            console.println(info, c);
        }
    
        @Override
        public void onAborted(String name, String result, int exitCode, double elapsedTime) {        
            String info = getAlgorithmName() + " was aborted with exit code " + exitCode + newLine
                    + "Execution time: " + t.getFormattedElapsedSeconds() + " seconds" + newLine;
            console.println(info, Color.RED);
        }
    
        @Override
        public void onProgress(String name, double elapsedTime) {
            console.println(getAlgorithmName() + " running time: " + 
                            t.getFormattedElapsedSeconds());
        }        
    }
    
    private class UpdateThread implements Runnable {
        @Override
        public void run() {
            while(running) {
                Tools.Utils.sleep(TIMEOUT);
                if(!t.isStopped()) {
                    Algorithm.this.onProgress(getAlgorithmName(), t.getElapsedSeconds());
                }            
            }
        }
    }
    
    /**
     * StreamBoozer re-routes output from an InputStream to all registered Consoles
     */
    public class StreamBoozer extends Thread {
        /**InputStream that is re-routed to all registered Consoles*/
        private final InputStream in;
 
        public StreamBoozer(InputStream in) {
            this.in = in;
        }
 
        @Override
        public void run() {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(in));
                String line;
                while ( (line = br.readLine()) != null) {
                    Algorithm.this.processOutput(line);
                }                
            } catch (IOException e) {
                Logger.getLogger(CMDAlgorithm.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                if(br != null) {
                    try {
                        br.close();
                    } catch (IOException ex) {
                        Logger.getLogger(CMDAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }             
            }
        }
    }
}
