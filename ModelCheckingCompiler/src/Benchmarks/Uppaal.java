/*
 * Uppaal.java
 * Copyright(c) 2014
 */
package Benchmarks;

import CodeGeneration.AST.Formula;
import Tools.FileInfo;

/**
 *
 * @author Peter Maandag
 */
public class Uppaal extends ModelCheckingAlgorithm {
    public static final String name = "UPPAAL";
    public static final String dir = "../uppaal-4.0.13/bin-Win32/";
    public static final String executable = "verifyta.exe";
    
    public Uppaal(FileInfo[] sourceFiles, Formula.Mode mode) {
        super(name, sourceFiles, new FileInfo(dir, executable), mode);
    }

    @Override
    protected ProcessBuilder getProcess(FileInfo[] sourceFiles) {
        return new ProcessBuilder(algorithmFile.toString(), 
                sourceFiles[0].toString(), 
                sourceFiles[1].toString(), "-t0");
    }
    
    @Override
    protected boolean hasTrace(String line) {
        return line.endsWith("Property is satisfied.");
    }
}
