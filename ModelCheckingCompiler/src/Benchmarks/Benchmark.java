/*
 * Benchmark.java
 * Copyright(c) 2014
 */
package Benchmarks;

import GUI.Console.Console;
import GUI.Console.ConsoleDump;
import GUI.MainWindow;
import Tools.DirectoryInfo;
import Tools.FileInfo;
import Tools.Timer;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public class Benchmark implements ExecutionNotifier {
    
    private Algorithm[] algorithms;
    private ExecutorService executorService;
    private MainWindow gui;
    private String outputDir;
    private volatile int numStarted;
    private volatile int numFinished;
    private volatile int numAborted;
    private Timer timer;
    private volatile double totalTime;
    public static final int UPDATE_INTERVAL = 60;
    private final int TIMEOUT;
    
    public Benchmark(Algorithm[] algorithms, MainWindow gui, 
            DirectoryInfo outputDir, int timeOut) {
        this(algorithms, gui, timeOut);
        this.outputDir = outputDir.dir;
        
        /*//Create the benchmark output directory
        this.outputDir = outputDir.dir;
        File f = outputDir.getFile();
        if(!f.exists()) {
            f.mkdir();
        }*/
    }
    
    public Benchmark(Algorithm[] algorithms, MainWindow gui, int timeOut) {
        this.algorithms = algorithms;
        this.gui = gui;
        this.TIMEOUT = timeOut;
        init();
    }
    
    private void init() {
        this.timer = new Timer();
        timer.restart();
        totalTime = 0.0;
        
        numStarted = 0;
        numFinished = 0;
        numAborted = 0;
    }
    
    public boolean isRunning() {
        return executorService != null && !executorService.isTerminated();
    }
    
    @Override
    public void onStart(String name) {
        MainWindow.getConsole().println("Starting " + name + "...");
        numStarted++;
    }
    
    @Override
    public void onProgress(String name, double elapsedTime) {
        synchronized(this) {
            if(timer.getElapsedSeconds() > UPDATE_INTERVAL) {            
                totalTime += timer.getElapsedSeconds();
                MainWindow.getConsole().println("Total running time: " + Timer.getFormattedTime(totalTime) + " seconds");
                timer.restart(); //restart timer                        
            }
            if(TIMEOUT != 0 && totalTime >= TIMEOUT) {
                MainWindow.getConsole().println("Aborting due to timeout...");
            }
        }
        
        if(TIMEOUT != 0 && totalTime >= TIMEOUT) {            
            this.stop(CMDAlgorithm.KILL_TIMEOUT);
        }
    }
    
    @Override
    public void onShutdown(String name, String result, int exitCode, double elapsedTime) {
        MainWindow.getConsole().println(name + " verification result: " + result, Color.BLUE.darker());
        String info1 = name + " completed with exit code " + exitCode + newLine
                + "Execution time: " + Timer.getFormattedTime(elapsedTime) + " seconds" + newLine;
        MainWindow.getConsole().println(info1, Color.GREEN.darker());
        numFinished++;
        
        if(numFinished == algorithms.length) {
            String info = "Benchmarks completed" + newLine + "Total running time: " + Timer.getFormattedTime(elapsedTime) + " seconds";
            MainWindow.getConsole().println(info, Color.GREEN.darker());
        }
    }
    
    @Override
    public void onAborted(String name, String result, int exitCode, double elapsedTime) {
        MainWindow.getConsole().println(name + " verification result: " + result, Color.BLUE.darker());
        String info1 = name + " was aborted with exit code " + exitCode + newLine
                + "Execution time: " + Timer.getFormattedTime(elapsedTime) + " seconds" + newLine;
        MainWindow.getConsole().println(info1, Color.RED);
        numAborted++;
        if(numAborted + numFinished == algorithms.length) {
            String info = "Benchmarks aborted" + newLine + "Total running time: " + Timer.getFormattedTime(elapsedTime) + " seconds";
            MainWindow.getConsole().println(info, Color.RED);
        }
    }
    
    private void setupConsole(Algorithm a) {
        //Get a console to write the algorithm output to:
        Console outputConsole = (gui == null) ? MainWindow.getConsole()
                : gui.addNewTab(a.getAlgorithmName());
        a.registerConsole(outputConsole);
        a.registerExecutionNotifier(a.new DefaultExecutionNotifier(outputConsole));
        
        //Dump the algorithm results to a file if specified:
        if(outputDir != null) {
            final String outputFilename = a.getAlgorithmName() + ".txt";
            ConsoleDump consoleToFile = new ConsoleDump(new FileInfo(outputDir, outputFilename));                   
            a.registerExecutionNotifier(consoleToFile);
            a.registerConsole(consoleToFile);
        }
    }
    
    /**
     * Non-blocking method that starts all algorithms
     */
    public void start() {
        init();
        executorService = Executors.newFixedThreadPool(algorithms.length);
        MainWindow.getConsole().println("Running " + algorithms.length + " benchmarks...");
        for(Algorithm a : algorithms) {
            //Set up (file-writing) console
            setupConsole(a);
            
            //Start the algorithm
            a.registerExecutionNotifier(this);
            executorService.execute(a);
        }

        executorService.shutdown();
    }
    
    /**
     * Blocking method that waits for all algorithms to stop.
     */
    public void stop(int reason) {
        for(Algorithm a : algorithms) {
            if(a.isRunning()) {
                if(a.kill(reason)) {
                    MainWindow.getConsole().println("Killed " + a.getAlgorithmName());
                } else {
                    MainWindow.getConsole().println("Could not kill " + a.getAlgorithmName(), Color.RED);
                }
            }
        }
        
        if(executorService != null) {
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException ex) {
                Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
