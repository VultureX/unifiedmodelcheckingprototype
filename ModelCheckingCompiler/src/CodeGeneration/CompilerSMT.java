/*
 * CompilerSMT.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import CodeGeneration.AST.*;
import CodeGeneration.AST.ASTNode;
import CodeGeneration.AST.AndOp;
import CodeGeneration.AST.AssignmentOp;
import CodeGeneration.AST.BinOp;
import CodeGeneration.AST.BinOpExp;
import CodeGeneration.AST.Bool;
import CodeGeneration.AST.DivOp;
import CodeGeneration.AST.EqualsOp;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.GTEOp;
import CodeGeneration.AST.GTOp;
import CodeGeneration.AST.LTEOp;
import CodeGeneration.AST.LTOp;
import CodeGeneration.AST.MinOp;
import CodeGeneration.AST.Num;
import CodeGeneration.AST.OrOp;
import CodeGeneration.AST.ParenExp;
import CodeGeneration.AST.PlusOp;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.TimesOp;
import CodeGeneration.AST.UnOpExp;
import CodeGeneration.AST.Var;
import CodeGeneration.AST.VarAss;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarDeclRanged;
import CodeGeneration.AST.VarInit;
import CodeGeneration.Semantics.Initializer;
import CodeGeneration.Semantics.TransformPostSemantics;
import CodeGeneration.Semantics.TransformPostSemanticsPS;
import CodeGeneration.Semantics.TransformPreSemantics;
import CodeGeneration.Semantics.TypeChecker;
import Tools.Lambda.Function0;
import static Tools.Utils.newLine;
import java.io.IOException;

/**
 *
 * @author Peter Maandag
 */
public class CompilerSMT extends Compiler {
    
    private final int DEFAULT_STEPS = 30;
    private final int STEPS;
    private final boolean compileLoopCode;
    private int step = 0;    
     
    public CompilerSMT(Formula syntaxTree) throws CompileError {
        super(syntaxTree);
        STEPS = addSteps ? syntaxTree.getMaxSteps().getValue() : DEFAULT_STEPS;
        
        Formula.Mode mode = syntaxTree.getMode();
        compileLoopCode = mode == Formula.Mode.Loop || 
                mode == Formula.Mode.PropertyLoop || mode == Formula.Mode.Liveness;
    }
    
    @Override
    protected String[] getExtensions() {
        return new String[]{".smt"};
    }
    
    private void printWarningMessages() {
       if(tree.getMaxSteps() == null) {
           print("Number of steps is not defined! "
                    + "Reverted to default value: " + DEFAULT_STEPS, WARNING);
       }
    }

    @Override
    protected void compile(String extension) throws IOException {        
        printWarningMessages();                    
        
        compile(Initializer.getInstance(tree, true));      
    }
    
    private void compile(Initializer formula) throws IOException {
        writeln("(benchmark " + currentCompiledFileName);
        
        VarDecl[] variables = formula.getVarDecl();
        compileVarDecl(variables);
        
        writeln(":formula");
        writeln("(and" + newLine);
        
        VarInit[] inits = formula.getVarInit();
        compileVarInit(variables, inits);
        
        Stmt[] stmts = formula.getStmt();
        compileGoal(variables, stmts);
        
        //compileRange(variables);     
        
        
        compileStmts(stmts, variables);
        
        writeln("))");
    }
    
    @Override
    protected void preCheckTransformTree() throws CompileError {
        TransformPreSemantics transform = new TransformPreSemantics(tree);
        
        if(addSteps) {
            transform.addSystemStepsVariable();
        }
        if(tree.getMaxSteps() != null) {
            transform.addMaxStepsVariable();
        }
        
        
        if(compileLoopCode) {
            transform.addLoopVariable();
            //transform.addLoopVariableTransitions();
            transform.addSystemLoopVariableTransitions();
        }
    }
    
    @Override
    protected void postCheckTransformTree() throws CompileError {
        //super.postCheckTransformTree();
        
        TransformPostSemantics transform = getInstance(tree);
        transform.transformTree();
        //if(tree.getMode() != Formula.Mode.Loop) {
            transform.addIdentityRuleYices();
        //}
        //transform.addUnseenVarsToTransitions();        
        //transform.mergeProcesses();
        transform.addSystemUnseenVars();
    }
    
    private TransformPostSemantics getInstance(Formula tree) {
        if(tree instanceof AST.TransitionSystem) {
            return new TransformPostSemantics((AST.TransitionSystem) tree);
        } else if(tree instanceof AST.ParallelSystem) {
            return new TransformPostSemanticsPS((AST.ParallelSystem) tree);
        } else {
            throw new UnsupportedOperationException("Unsupported formula in getInstance");
        }
    }
    
    private void compileLoopConstraintsShort(VarDecl[] variables, boolean compileLoopProperty, boolean compileLoopVariable) throws IOException {        
//        for(int i = 0; i <= STEPS; i++) {
//            writeln("(= (loop " + i +") true)");
//        }
        
        writeln("(and");
        writeln("(> " + TypeChecker.loopVariable1 + " 0)");
        writeln("(> " + TypeChecker.loopVariable2 + " 0)");
        writeln("(<= " + TypeChecker.loopVariable1 + " " + STEPS + ")");
        writeln("(<= " + TypeChecker.loopVariable2 + " " + STEPS + ")");
        writeln("(< " + TypeChecker.loopVariable1 + " " + TypeChecker.loopVariable2 + ")");
        
        if(compileLoopVariable) {
            writeln("(= (" + TypeChecker.loopVariable + " " + TypeChecker.loopVariable2 + ") true)");
        }
        
        for(VarDecl vardecl : variables) {
            String var = vardecl.getVar().getValue();
            if(!var.equals(TypeChecker.loopVariable) && !var.equals(TypeChecker.stepsVariable)) {
                writeln("(= (" + var + " " + TypeChecker.loopVariable1 + ") (" + var + " " + TypeChecker.loopVariable2 + "))" );
            }
        }
        writeln(")" + newLine);
        
        ASTNode goal = tree.getGoal();
        if(compileLoopProperty && goal != null) {
            writeln(";;Loop property");
            writeln("(and");
            writeln("(<= " + TypeChecker.loopPropertyVariable + " " + TypeChecker.loopVariable2 + ")");
            writeln("(>= " + TypeChecker.loopPropertyVariable + " " + TypeChecker.loopVariable1 + ")");
                
            compileExpression(goal, getLoopVariable);         
            writeln(newLine + ")");
            //(and
            //(<= loopp loopj)
            //(>= loopp loopi)
            //(goal loopp)
            //)
        }
        
        //(and
        //(< testi testj) 
        //(= (x testi) (x testj))
        //(= (y testi) (y testj))
        //)
    }
    
    private void compileLoopConstraints(VarDecl[] variables) throws IOException {                
        for(int i = 0; i <= STEPS; i++) {
            writeln("(= (loop " + i +") true)");
        }
        
        int oldStep = step;
        writeln("(or");
        for(int i = 0; i <= STEPS; i++) {
            for(int j = i+1; j <= STEPS; j++) {
                writeln("(and ");
                for(VarDecl vardecl : variables) {
                    String var = vardecl.getVar().getValue();
                    if(!var.equals(TypeChecker.loopVariable) && !var.equals(TypeChecker.stepsVariable)) {
                        writeln("(= (" + var + " " + i + ") (" + var + " " + j + "))" );
                    }
                }
                
                //Loop + condition:
                ASTNode goal = tree.getGoal();
                if(goal != null) {
                    writeln("(or");
                    for(int k = i; k <= j; k++) {
                        step = k;
                        compileExpression(goal);
                        write(newLine);
                    }
                    writeln(")");
                }

                writeln(")");
            }
        }
        writeln(")" + newLine);
        step = oldStep;
    }
    
//    private void compileRange(VarDecl[] variables) throws IOException {
//        writeln(";;Range constraints");
//        for(int i = variables.length-1; i >= 0; i--) {            
//            if(variables[i] instanceof VarDeclRanged) {
//                VarDeclRanged var = (VarDeclRanged) variables[i];
//                for(int j = 0; j <= STEPS; j++) {
//                    write("(>= (" + var.getVar().getValue() + " " + j + ") ");
//                    compileExpression(var.getRange().getMin());
//                    writeln(")");
//                    write("(<= (" + var.getVar().getValue() + " " + j + ") ");
//                    compileExpression(var.getRange().getMax());
//                    writeln(")");
//                }
//            }
//        }
//        write(newLine);
//    }
    
    private void compileRange(VarDecl[] variables) throws IOException {
        for(int i = variables.length-1; i >= 0; i--) {            
            if(variables[i] instanceof VarDeclRanged) {
                VarDeclRanged var = (VarDeclRanged) variables[i];
                write("(>= (" + var.getVar().getValue() + " " + 0 + ") ");
                compileExpression(var.getRange().getMin());
                writeln(")");
                write("(<= (" + var.getVar().getValue() + " " + 0 + ") ");
                compileExpression(var.getRange().getMax());
                writeln(")");
            }
        }
    }
    
    private void compileGoal(VarDecl[] variables, Stmt[] stmts) throws IOException {
        switch(tree.getMode()) {
            case Reachability:
                compileReachability();
                break;
            case Loop:
                //compileLoopConstraints(variables);
                writeln(";;Loop conditions:");
                compileLoopConstraintsShort(variables, false, true);
                break;
            case Safety:
                compileSafety();
                break;
            case Deadlock:
                compileDeadlock(stmts);
                break;
            case Liveness:
                compileLiveness(variables);
                break;
            default:
                System.err.println("Unknown goal property in compileGoal");
        }
    }
    
    private void compileLiveness(VarDecl[] variables) throws IOException {
        writeln(";;Liveness property");        
        writeln("(or" + newLine);
        
        //There is a loop in which the property never holds
        writeln(";;Detect a loop that is not live");
        writeln("(and");
        compileLoopConstraintsShort(variables, false, true);        
        writeln("(and");
        int oldStep = step;
        for(int i = 0; i <= STEPS; i++) {
            step = i;
            writeln("(or");
            writeln("(< " + i + " " + TypeChecker.loopVariable1 + ")");
            writeln("(> " + i + " " + TypeChecker.loopVariable2 + ")");
            write("(not ");
            compileExpression(tree.getGoal());
            writeln(")" + newLine + ")");
        }
        step = oldStep;        
        writeln("))" + newLine);
        
        //Final state is not part of a loop and does not satisfy property:
        writeln(";;Detect a final invalid state that is not part of any loop");
        writeln("(and");
        write("(not ");
        compileExpression(tree.getGoal(), () -> String.valueOf(STEPS));
        writeln(")");
        
        oldStep = step;
        ASTNode goal = tree.getGoal();
        writeln("(not (or");
        for(int i = 0; i <= STEPS; i++) {
            for(int j = i+1; j <= STEPS; j++) {
                writeln("(and ");
                for(VarDecl vardecl : variables) {
                    String var = vardecl.getVar().getValue();
                    if(!var.equals(TypeChecker.loopVariable) && !var.equals(TypeChecker.stepsVariable)) {
                        writeln("(= (" + var + " " + i + ") (" + var + " " + j + "))" );
                    }
                }
                
                writeln("(or ");
                for(int k = i; k <= j-1; k++) {
                    step = k;
                    //write("(not ");
                    compileExpression(goal);
                    write(newLine);
                    //write(")" + newLine);
                }
                writeln(")");

                writeln(")");
            }
        }
        writeln(")))" + newLine);
        step = oldStep;
        
        /*writeln(";;Detect a final invalid state that is not part of any loop");
        writeln("(and");
        write("(not ");
        compileExpression(tree.getGoal(), () -> String.valueOf(STEPS));
        writeln(")");
        write("(not ");
        compileLoopConstraintsShort(variables, true, false);
        writeln(")");
        writeln(")");*/

        //Final state is not part of a loop and does not satisfy property:
        /*writeln(";;Detect a final invalid state that is not part of any loop");
        writeln("(and");
        compileLoopConstraintsShort(variables, false, false);
        writeln("(<= " + TypeChecker.loopPropertyVariable + " " + TypeChecker.loopVariable2 + ")");
        writeln("(>= " + TypeChecker.loopPropertyVariable + " " + TypeChecker.loopVariable1 + ")");
        compileExpression(tree.getGoal(), getLoopVariable);         
        writeln(newLine + "))" + newLine);*/
        
        //There is a finite path that is no loop, where the property does not 
        //hold in the last state
        /*writeln(";;Path that is no loop and ending in bad state");
        writeln("(and (not (and");
        compileLoopConstraintsShort(variables, true);       
        writeln("))");
        write("(not ");
        compileExpression(tree.getGoal(), () -> String.valueOf(STEPS));
        writeln(")" + newLine + ")");
        writeln(")" + newLine);*/
        
        //The property holds in no state:
        /*writeln("(and");
        for(int i = 0; i <= STEPS; i++) {
            step = i;
            write("(not ");
            compileExpression(tree.getGoal());
            writeln(")");
        }       
        writeln(")" + newLine);*/
        
        writeln(")" + newLine);
    }
    
    private void compileDeadlock(Stmt[] stmts) throws IOException {
        writeln(";;Deadlock property");
        
        //Get the identityGuard:
        Exp identityGuard = stmts[0].getGuard();
        if(identityGuard == null) {
            //A deadlock cannot occur if a transition is always possible:
            writeln("false" + newLine);
            return;
        }
        
        writeln("(or ");
        for(int i = 0; i < STEPS; i++) {
            step = i;
            compileExpression(identityGuard);
            write(newLine);
        }   
        writeln(")" + newLine);  
        
        
//        //Retrieve all guards
//        ArrayList<ASTNode> guards = new ArrayList<>();
//        boolean nullGuard = false;
//        for(Stmt s : stmts) {
//            if(s.getGuard() != null) {
//                guards.add(s.getGuard());
//            } else {
//                nullGuard = true;
//            }
//        }
//        
//        //Deadlock occurs when for any step none of the guards is satisfiable
//        if(!nullGuard) {
//            writeln("(or ");
//            for(int i = 0; i <= STEPS; i++) {
//                step = i;
//                write("(not (or");
//                for(ASTNode guard : guards) {
//                    compileExpression(guard);                    
//                }
//                writeln("))");
//            }   
//            writeln(")" + newLine);
//        } else {
//            //A deadlock cannot occur when there are no guards:
//            writeln("false" + newLine);
//        }
    }
    
    private void compileSafety() throws IOException {
        writeln(";;Safety property");
        writeln("(or");
        for(int i = 0; i <= STEPS; i++) {
            step = i;
            write("(not ");
            compileExpression(tree.getGoal());
            writeln(")");
        }       
        writeln(")" + newLine);
    }
    
    private void compileReachability() throws IOException {
        writeln(";;Reachable state");
        writeln("(or");
        for(int i = 0; i <= STEPS; i++) {
            step = i;      
            compileExpression(tree.getGoal());
            write(newLine);
        }       
        writeln(")" + newLine);
    }
    
    private void compileVarDecl(VarDecl[] variables) throws IOException {
        write(":extrafuns (");
        
        //(testi Int) (testj Int)//(testi Int) (testj Int)
        if(compileLoopCode) {
            write("(" + TypeChecker.loopVariable1 + " Int) ");
            write("(" + TypeChecker.loopVariable2 + " Int) ");
            if(tree.getMode() == Formula.Mode.PropertyLoop || tree.getMode() == Formula.Mode.Liveness) {
                write("(" + TypeChecker.loopPropertyVariable + " Int) ");
            }
        }
        
        for(int i = variables.length-1; i >= 0; i--) {
            String id = variables[i].getVar().getValue();
            compileVarDecl(variables[i], id);
            if(i > 0) {
                write(" ");
            }
        }
        
        writeln(")");       
    }
    
    private void compileVarDecl(VarDecl variable, String id) throws IOException {
        switch(variable.getTypeNode().getType()) {
            case Int:
                write("(" + id + " Int Int)"); break;
            case Bool:
                write("(" + id + " Int bool)"); break;
            default:
                throw new UnsupportedOperationException("Unrecognized type in compileVarDecl: " +
                        variable.getTypeNode().getType());
        }
    }
    
    private void compileVarInit(VarDecl[] vars, VarInit[] inits) throws IOException {
        if(inits.length != 0) {
            writeln(";;Initial state");
            for(int i = 0; i < inits.length; i++) {
                compileExpression(inits[i].getExpression());
                write(newLine);
            }
            compileRange(vars);
        }
        write(newLine);
    }
    
    private void compileStmts(Stmt[] stmts, VarDecl[] vars) throws IOException {
        for(int i = 0; i < STEPS; i++) {
            step = i;      
            compileTransitions(stmts, vars);
        }
    }
    
    private void compileTransitions(Stmt[] stmts, VarDecl[] vars) throws IOException {
        writeln(";;***Transition step " + step + "***");
        writeln("(or");
        
        for(int i = stmts.length-1; i >= 0; i--) {
            writeln("(and");                      
            compileTransition(stmts[i]);           
            writeln(")");
        }
        
//        if(checkLoops) {
//            writeln("(= (loop " + (step+1) + ") false)");
//        } else {
//            //Write a rule that does nothing when all variables are out of bound:
//            
//            /*writeln("(and");
//            compileIdentityRule(stmts, vars);
//            writeln(")");*/
//        }
        
        writeln(")" + newLine);
    }
    
    //@todo: implement this as a tree transformation
    private void compileIdentityRule(Stmt[] stmts, VarDecl[] vars) throws IOException {       
        //The negation of all guards:
        boolean foundOne = false;        
        for(Stmt stmt : stmts) {
            ASTNode guard = stmt.getGuard();
            if(guard != null) {
                if(!foundOne) {
                    writeln("(not (or ");
                    foundOne = true;
                }
                compileExpression(stmt.getGuard());      
            }
        }
        if(foundOne) {
            writeln("))");
        }

        //Identity rule:
        for(VarDecl var : vars) {            
            write("(= ");
            write("(" + var.getVar().getValue() + " " + (step+1) + ") ");
            compileExpression(var.getVar());
            writeln(")");
        }
    }
    
    private void compileTransition(Stmt s) throws IOException {
        ASTNode guard = s.getGuard();
        VarAss effect = s.getEffect();
        
        if(guard != null) {
            compileExpression(guard);
            write(newLine);           
        }
        compileVarAss(effect);
        //addUnseenVars(s);
    }
    
    private void compileBinOpExp(BinOpExp e, Function0<String> stepGetter) throws IOException {        
        BinOp op = e.getOp();
        ASTNode lhs = e.lhs();
        ASTNode rhs = e.rhs();
        
        write("(");
        compileBinOp(op);
        write(" ");
        compileExpression(lhs, stepGetter);
        write(" ");
        compileExpression(rhs, stepGetter);
        write(")");
    }
    
    private void compileBinOp(BinOp t) throws IOException {
        if(t instanceof PlusOp) {
            write("+");
        } else if(t instanceof MinOp) {
            write("-");
        } else if(t instanceof DivOp) {
            write("/");
        } else if(t instanceof TimesOp) {
            write("*");
        } else if(t instanceof LTEOp) {
            write("<=");
        } else if(t instanceof GTEOp) {
            write(">=");
        } else if(t instanceof LTOp) {
            write("<");
        } else if(t instanceof GTOp) {
            write(">");
        } else if(t instanceof EqualsOp) {
            write("=");
        } else if(t instanceof AndOp) {
            write("and");
        } else if(t instanceof OrOp) {
            write("or");
        } else if(t instanceof AssignmentOp) {
            write("=");
        } else {
            print("Unrecognized operator in compileBinOp: " + t.toString(), ERROR);
        }
    }
    
    private void compileExpression(ASTNode t) throws IOException {
        compileExpression(t, getStepCounter);
    }
    
    private void compileExpression(ASTNode t, Function0<String> stepGetter) throws IOException {
        if(t instanceof Var) {
            Var id = (Var) t;
            write("(" + id.getValue() + " " + stepGetter.invoke() + ")");
        } else if(t instanceof Num) {
            Num num = (Num) t;
            int i = num.getValue();
            if(i < 0) {
                write("(- 0 " + Integer.toString(Math.abs(i)) + ")");
            } else { 
                write(Integer.toString(i));
            }
        } else if(t instanceof Bool) {
            boolean b = ((Bool) t).getValue();
            String bool = b ? "true" : "false";
            write(bool);
        } else if(t instanceof ParenExp) {
            ParenExp p = (ParenExp) t;
            compileExpression(p.getExp(), stepGetter);
        } else if(t instanceof BinOpExp) {
            compileBinOpExp((BinOpExp)t, stepGetter);
        } else if(t instanceof UnOpExp) {
            write("(not ");
            compileExpression(((UnOpExp)t).getExp(), stepGetter);
            write(")");
        } else {
            print("Unrecognized expression in compileExpression: " + t.toString(), ERROR);
        }
    }
    
    private void compileVarAss(VarAss ass) throws IOException {
        VarAss[] parents = ass.getDescendants();
        
        for(int i = parents.length-1; i >= 0; i--) {         
            compileAssignment(parents[i]);
        }
    }
    
    private void compileAssignment(VarAss t) throws IOException {        
        write("(= ");
        write("(" + t.getID().getValue() + " " + (step+1) + ") ");
        compileExpression(t.getAss());
        writeln(")");
    }
    
    //Function pointers
    Function0<String> getStepCounter = () -> String.valueOf(step);
    Function0<String> getLoopVariable = () -> TypeChecker.loopPropertyVariable;
}
