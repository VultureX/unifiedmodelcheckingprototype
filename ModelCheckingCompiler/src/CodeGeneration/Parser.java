/*
 * Parser.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import CodeGeneration.AST.*;
import GUI.MainWindow;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import static Tools.Utils.newLine;

/**
 *
 * @author Peter Maandag
 */
public class Parser {
    
    private Tokenizer tokenizer;
    private Iterator<Token> tokens;
    private Token _currentToken;
    private Token.Type currentToken;
    
    public Parser(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }
    
    private void init() throws ParseError {
        this.tokens = tokenizer.getIterator();
        nextToken();
    }
    
    private void nextToken() throws ParseError {        
        while(this.tokens.hasNext()) {
            this._currentToken = this.tokens.next();
            this.currentToken = _currentToken.getType();
            if(currentToken != Token.Type.Comment) {                
                return;
            }
        }
        throw new ParseError("Unexpected end of file");                
    }
    
    private class Core {
        public VarDecl decl;
        public VarInit init;
        public Stmt stmt; 
        public ASTNode program;
        
        public Core(VarDecl decl, VarInit init, Stmt stmt, ASTNode program) {
            this.decl = decl;
            this.init = init;
            this.stmt = stmt;
            this.program = program;
        }
    }
    
    /**
     * Formula = 'VARS:' VarDecl+ ['INIT:' VarInit*] 'TRANS:' Stmt+ 'REACH:' Exp 
     */
    public Formula parseFormula() throws ParseError {        
        init();
        Token token = _currentToken;
        
        //[MAXSTEPS]
        Num maxSteps = null;
        if(consumeBlock(Token.Type.MAXSTEPS)) {
            maxSteps = parseNum("Number expected");
        }
        
        //VarDecl+ [VarInit] (Stmt+ | Program)
        Core c = parseCore("main script");
        
        //REACH p | INF | DEADLOCK | SAFE p | LIVENESS p
        Token.Type t = currentToken;
        Formula.Mode property;
        boolean isParameterized = true;
        switch(t) {            
            case LOOP:
                isParameterized = false;
                property = Formula.Mode.Loop;
                break;
            case DEADLOCK:
                isParameterized = false;
                property = Formula.Mode.Deadlock;
                break;
            case GOAL:
                property = Formula.Mode.Reachability;
                break;
            case LIVENESS:
                property = Formula.Mode.Liveness;
                break;
            case SAFE:
                property = Formula.Mode.Safety;
                break;
            default:
                throw new ParseError("Unknown Property: " + t);                
        }
        parseBlock(t, "Property block expected");        
        ASTNode goal = parseGoalExp(t, isParameterized);
        
        //[Process]
        AST.Process p = null;
        if(c.program != null) {
            p = parseProcess();
        }
        
        if(currentToken != Token.Type.EOF) {
            throw new ParseError("End of file expected");
        }
        
        if(c.program == null) {
            return new TransitionSystem(token, maxSteps, c.decl, c.init, c.stmt, goal, property);
        } else {
            return new ParallelSystem(token, maxSteps, c.decl, c.init, c.program, p, goal, property);
        }
    }
    
    private ASTNode parseGoalExp(Token.Type p, boolean isParameterized) throws ParseError {
        ASTNode e = parseExp();
        if(e != null && !isParameterized) {
            throw new ParseError(p + " property takes no parameters");
        } else if(e == null && isParameterized) {
            throw new ParseError(p + " property must have a boolean parameter");
        }
        e = AST.filterPars(e);
        if(e != null && !(e instanceof BinOpExpBool || e instanceof UnOpExpBool)) {
            throw new ParseError(p + " expression must be boolean");
        }
        return e;
    }
    
    /*private ASTNode parseGoal(boolean allowsNull) throws ParseError {
        ASTNode e = parseExp();
        if(e == null && !allowsNull) {
            throw new ParseError("Final expression expected");
        }
        e = AST.filterPars(e);
        if(e != null && !(e instanceof BinOpExpBool || e instanceof UnOpExpBool)) {
            throw new ParseError("Loop expression must be boolean");
        }
        return e;
    }*/
    
    private Core parseCore(String blockName) throws ParseError {
        //VarDecl+
        parseBlock(Token.Type.VARS, "VARS declaration block expected");
        VarDecl d = parseVarDecl();
        
        //[VarInit]
        VarInit init = null;
        if(consumeBlock(Token.Type.INIT, "INIT block was not specified in " + blockName)) {
            init = parseVarInit(blockName);
        }
        
        //Stmt+ | Program
        Stmt stmt = null;
        ASTNode program = null;
        switch(currentToken) {
            case TRANS:
                consumeBlock(Token.Type.TRANS); 
                stmt = parseStmt();
                break;
            case PROGRAM:
                consumeBlock(Token.Type.PROGRAM); 
                program = parseProgramExp();
                break;
            default:
                throw new ParseError("PROGRAM or TRANS block expected");
        }
        
        return new Core(d, init, stmt, program);
    }
    
    private void parseBlock(Token.Type block, String errorMsg) throws ParseError {
        if(!consumeBlock(block)) {
            throw new ParseError(errorMsg);
        }
    }
    
    private boolean consumeBlock(Token.Type blockToken) throws ParseError {
        boolean consumedBlock = consumeToken(blockToken);
        if(consumedBlock) {
            parseToken(Token.Type.Colon, ": expected");
        }
        return consumedBlock;
    }
    
    private boolean consumeBlock(Token.Type block, String warningMsg) throws ParseError {
        boolean consumed = consumeBlock(block);
        if(!consumed) {
            MainWindow.getConsole().println(warningMsg, Color.ORANGE);
        }
        return consumed;
    }
    
    private Var parseProcessBlock() throws ParseError {
        if(!consumeToken(Token.Type.PROCESS)) {
            return null;
        }        
        Var id = parseVar("process identifier expected");
        parseToken(Token.Type.Colon, ": expected");
        return id;
    }
    
    private ASTNode parseProgramExp() throws ParseError {
        ASTNode e = parseExp();
        if(e == null) {
            throw new ParseError("Program expression expected");
        }
        e = AST.filterPars(e);
        if(e != null && !(e instanceof BinOpExpProc || e instanceof ID)) {
            throw new ParseError("Program expression must be process algebra");
        }
        return e;
    }
    
    private AST.Process parseProcess() throws ParseError {
        AST.Process p = parseProcess(null);
        if(p == null) {
            throw new ParseError("PROCESS block expected");
        }
        
        AST.Process p2;
        do {
            p2 = p;
            p = parseProcess(p);
        } while(p != null);
        return p2;
    }
    
    private AST.Process parseProcess(AST.Process parent) throws ParseError {
        Token t = _currentToken;
        
        Var processID = parseProcessBlock();
        if(processID == null) {
            return null;
        }
        
        Core c = parseCore("Process " + processID.getValue());
        if(c.stmt == null) {
            throw new ParseError("Nested programs in processes are currently not supported " 
                    + c.program.getDescription());
        }
        ASTNode stmt = (c.stmt == null) ? c.program : c.stmt;
        return new AST.Process(t, parent, processID, c.decl, c.init, stmt);
    }
    
    private Stmt parseStmt() throws ParseError {
        Stmt stmt = parseStmt(null);
        if(stmt == null) {
            throw new ParseError("Transition expected");
        }
        
        Stmt s2;
        do {
            s2 = stmt;
            stmt = parseStmt(stmt);
        } while(stmt != null);
        return s2;
    }
    
    private VarInit parseVarInit(String blockName) throws ParseError {
        VarInit init = null;
        Token t = _currentToken;
        
        ASTNode exp = parseExp();
        while(exp != null) {
            if(!(exp instanceof BinOpExp)) {
                throw new ParseError("Assignment expression expected instead of " + exp.getDescription());
            }
            init = new VarInit(t, init, (BinOpExp)exp);
            parseToken(Token.Type.Statement, "End of statement expected");
            //nextToken();
            exp = parseExp();
        }
        
        if(init == null) {
            MainWindow.getConsole().println("INIT block is empty in " + blockName, Color.ORANGE);
        }
        return init;
    }
    
    private VarDecl parseVarDecl() throws ParseError {
        VarDecl d = parseVarDecl(null);
        if(d == null) {
            throw new ParseError("VarDecl expected");
        }
        VarDecl d2;
        do {
            d2 = d;
            d = parseVarDecl(d);
        } while(d != null);
        return d2;
    }
    
    private int countNonNull(Token[] array) {
        int count = 0;
        for(Token token : array) {
            if(token != null) {
                count++;
            }
        }
        return count;
    }
    
    private VarDecl parseVarDecl(VarDecl parent) throws ParseError {        
        Token t = _currentToken;
        
        //Parse keywords
        Token constToken = tryConsumeToken(Token.Type.CONST);
        Token nonDetToken = tryConsumeToken(Token.Type.NONDET);
        int count = countNonNull(new Token[] {
            constToken, nonDetToken});
        if(count > 1) {
            throw new ParseError("Variable can be either constant or non-deterministic");
        }
        Keyword constantKeyword = (constToken == null) ? null : 
                    new Keyword(constToken, constToken.value());
        Keyword nondetKeyword = (nonDetToken == null) ? null : 
                    new Keyword(nonDetToken, nonDetToken.value());
        
        //Parse type
        TypeNode type = tryParseTypeNode();
        if(type == null) {
            return null;
        }
        if(type.getType() == TypeNode.Type.Proc && count > 0) {
            throw new ParseError("Process variable cannot be declared constant or non-deterministic");
        }
        
        //Parse identifier
        Var id = parseVar("Identifier expected in variable declaration");
        
        //Parse constant or process variable
        if(constToken != null || type.getType() == TypeNode.Type.Proc) {
            parseToken(Token.Type.Assignment, "Assignment operator expected "
                + "after variable " + id.getDescription());
            
            Value value = parseValue();
            if(value == null) {
                throw new ParseError("Value expected in variable declaration of " + id.getDescription());
            }
            parent = new VarDecl(t, parent, constantKeyword, nondetKeyword,
                    type, id, value);
        } else {
            //Parse range declaration:
            if(consumeToken(Token.Type.Colon)) {
                if(type.getType() != TypeNode.Type.Int) {
                    throw new ParseError("Only variables of type int can have a range");
                }
                Range r = parseRange();
                parent = new VarDeclRanged(t, parent, constantKeyword, nondetKeyword,
                        type, id, null, r);
            } else {
                parent = new VarDecl(t, parent, constantKeyword, nondetKeyword,
                        type, id, null);
            }
        }        
        
        //Parse end of statement
        parseToken(Token.Type.Statement, "; expected");
        return parent;
    }
    
    private Range parseRange() throws ParseError {
        Token t = _currentToken;
        final String errorMsg = "ID or Number expected in range declaration";
        Value min = parseNumOrID(errorMsg);        
        parseToken(Token.Type.RangeSeperator, "Range seperator (..) expected");
        Value max = parseNumOrID(errorMsg);
        return new Range(t,min, max);
    }
    
    private Value parseNumOrID(String errorMsg) throws ParseError {
        Num num = tryParseNum();
        if(num != null) {
            return num;
        }
        Var id = tryParseVar();
        if(id != null) {
            return id;
        }
        throw new ParseError(errorMsg);
    }
    
    //@Todo: add ID-parser
    private VarAss parseVarAss() throws ParseError {        
        ArrayList<ID> ids = new ArrayList<>();
        ArrayList<Token> theTokens = new ArrayList<>();
        
        theTokens.add(_currentToken);
        //Var id = parseVar("Variable assignment ID expected");
        ID id = parseID("Variable assignment ID expected");
        ids.add(id);
        
        while(consumeToken(Token.Type.AssignmentSeperator)) {
            theTokens.add(_currentToken);
            id = parseVar("Assignment ID expected");
            ids.add(id);
        } 
        
        parseToken(Token.Type.Assignment, "Assignment operator expected "
                + "after variable " + id.getDescription());
        
        ArrayList<ASTNode> exprs = new ArrayList<>();

        ASTNode exp = parseExp();
        exprs.add(exp);
        while(consumeToken(Token.Type.AssignmentSeperator)) {         
            exp = parseExp();
            exprs.add(exp);
        }
        
        if(exprs.size() != ids.size()) {
            throw new ParseError("Assignment ids don't match." + newLine + "#Expressions: " 
                    + exprs.size() + newLine + "#IDs: " + ids.size());
        }
        
        VarAss v = null;
        for(int i = 0; i < ids.size(); i++) {
            v = new VarAss(theTokens.get(i), v, ids.get(i), exprs.get(i));
        }        
        
        return v;
    }
    
    private Stmt parseStmt(Stmt parent) throws ParseError {
        VarAss v;
        Token t = _currentToken;
        switch(currentToken) {
            case Guard:
                nextToken();
                ASTNode e = AST.filterPars(parseExp());
                //@todo, support any boolean expression
                if(!(e instanceof BinOpExpBool) && !(e instanceof UnOpExpBool)) {
                    throw new ParseError("Guard must be boolean expression");
                }
                parseToken(Token.Type.Transition, "Transition expected");
                v = parseVarAss();
                parseToken(Token.Type.Statement, "End of Statement expected");               
                return new Stmt(t, parent, (Exp) e, v);
            case ID:
                v = parseVarAss();
                parseToken(Token.Type.Statement, "End of Statement expected");
                return new Stmt(t, parent, null, v);
            default:
                return null;
        }
   }
    
    private void checkExp(ASTNode lhs, Op op, ASTNode rhs) throws ParseError {
        if(op == null) {
            return;
        }
        
        ASTNode.Type rightType = rhs.getType();
        if(op instanceof UnOp) {
            switch(op.getType()) {
                case Bool:
                    if(!isUndefined(rightType)) {
                        if(rightType != ASTNode.Type.Bool) {
                            throw new ParseError("Boolean expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());
                        }
                    }
                    return;
                default:
                 throw new UnsupportedOperationException("Unhandled UnOp in checkExp(): " 
                       + op.getClass());
            }
        }

        //System.out.println("***SIDE:" + lhs.flattenToken());
        //System.out.println("***TYPE:" + lhs.getType());
        //System.out.println("***OP:" + op.flattenToken());
        //System.out.println("***OPTYPE:" + op.getType());
        
        ASTNode.Type leftType = lhs.getType();
        switch(op.getType())
        {
             case Int:
                if(!isUndefinedOr(leftType, ASTNode.Type.Int)) {
                    throw new ParseError("Arithmetic expression expected instead of " + lhs.getDescription());
                }
             break;
             case Bool:
                //Int > Int
                if(op instanceof BinOpBoolCompareArith) {
                    if(!(isUndefinedOr(leftType, ASTNode.Type.Int))) {
                        throw new ParseError("Arithmetic expression expected near " + op.getDescription() + " instead of " + lhs.getDescription());
                    } else if(!(isUndefinedOr(rightType, ASTNode.Type.Int))) {
                        throw new ParseError("Arithmetic expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());
                    }
                //Int == Int, Bool == Bool
                } else if(op instanceof BinOpBoolCompare) {
                    if(leftType != rightType && !(isUndefined(leftType) || isUndefined(rightType))) {
                        if(leftType == ASTNode.Type.Int) {
                            throw new ParseError("Arithmetic expression expected instead of " + rhs.getDescription());
                        } else if(leftType == ASTNode.Type.Bool) {
                            throw new ParseError("Boolean expression expected instead of " + rhs.getDescription());                             
                        } else {
                            throw new UnsupportedOperationException("Unhandled type in checkExp(): " 
                                   + leftType + " " + rightType);
                        }
                    }
                //Bool & Bool
                } else if(op instanceof BinOpBoolConnect) {
                    if(!(isUndefinedOr(leftType, ASTNode.Type.Bool))) {
                        throw new ParseError("Boolean expression expected near " + op.getDescription() + " instead of " + lhs.getDescription());
                    } else if(!(isUndefinedOr(rightType, ASTNode.Type.Bool))) {
                        throw new ParseError("Boolean expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());
                    }
                }
             break;
             case Proc:
                 if(!(isUndefinedOr(leftType, ASTNode.Type.Proc))) {
                    throw new ParseError("process algebraic expression expected near " + op.getDescription() + " instead of " + lhs.getDescription());
                 } else if(!(isUndefinedOr(rightType, ASTNode.Type.Proc))) {
                    throw new ParseError("process algebraic expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());
                 }
                 break;
             case Void:
             case Undefined:
                 break;
             default:
                 throw new UnsupportedOperationException("Unhandled op in checkExp(): " 
                       + op.getClass());
        }
    }
   
    private ASTNode parseExp() throws ParseError {
        return parseExp(lowest);
    }
    
    private ASTNode parseExp(OperatorParser fp) throws ParseError {
         Token token = _currentToken;
         
         Op op = fp.tryParseOperator();
         ASTNode t = fp.parseTerm();
         if (op == null) {             
             if(t == null) {
                return null;
             }
             op = fp.tryParseOperator();
         } else {
             if(!(op instanceof UnOp)) {
                throw new ParseError("Term expected instead of " + op.getDescription());
             }
         }
         
         while(op != null) {
             if(op instanceof UnOp) {
                //parse UnOpExp
                checkExp(null, op, t);
                t = new UnOpExpBool(token, (UnOpBool)op, t);                 
             } else {
                //parse BinOpExp
                ASTNode rightTerm = fp.parseTerm();
                if(rightTerm == null) {
                    throw new ParseError("Term expected");
                }

                checkExp(t, op, rightTerm);

                if(op instanceof BinOpArith) {
                    t = new BinOpExpArith(token, t, (BinOpArith)op, rightTerm);
                } else if(op instanceof BinOpBool) {
                    t = new BinOpExpBool(token, t, (BinOpBool)op, rightTerm);
                } else if(op instanceof BinOpProc) {
                    t = new BinOpExpProc(token, t, (BinOpProc)op, rightTerm);
                } else if(op instanceof AssignmentOp /*|| op instanceof AccessorOp*/) {
                    t = new BinOpExp(token, t, (BinOp)op, rightTerm);
                } else {
                    throw new UnsupportedOperationException("Unhandled op case in parseExp()");
                }
             }

             op = fp.tryParseOperator();
         } 

         return t;
    }
   
    private ASTNode parseFact() throws ParseError {
         Token token = _currentToken;
         Value v = parseValue();
         if(v != null) {
             return v;
         }

         //return null;
         switch(currentToken) {
             case ParenOpen:
                 //Syntax left = new Syntax(_currentToken);
                 nextToken();
                 ASTNode exp = parseExp();
                 //Syntax right = new Syntax(_currentToken);
                 parseToken(Token.Type.ParenClose, "Closing parenthesis expected after " + exp.getDescription());
                 ASTNode t = new ParenExp(token, exp);
                 return t;
             default:
                 return null;
         }
     }
   
    private Value parseValue() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
             case ID:
                 ID id = parseID("This error should not occur");
                 //Var id = new Var(token, _currentToken.value());
                 //nextToken();
                 return id;
             case Minus:
                 nextToken();
                 Token t = parseToken(Token.Type.Number, "Number expected after negation");
                 Num negNum = new Num(token, -Integer.parseInt(t.value()));
                 return negNum;
             case Number:
                 Num num = new Num(token, Integer.parseInt(_currentToken.value()));
                 nextToken();
                 return num;
             case TRUE:
                 Bool bt = new Bool(token, true);
                 nextToken();
                 return bt;
             case FALSE:
                 Bool bf = new Bool(token, false);
                 nextToken();
                 return bf;
             default:
                 return null;
        }
    }
    
    private final OperatorParser lowest = new ParseLowest();
    private final OperatorParser low = new ParseLow();
    private final OperatorParser medium = new ParseMedium();
    private final OperatorParser high = new ParseHigh();
    private final OperatorParser higher = new ParseHigher();
    //private final OperatorParser highestBinOp = new ParseHighestBinOp();
    private final OperatorParser highestUnOp = new ParseHighestUnOp();
    
    private class ParseLowest implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseLowestPrioOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(low);
         }       
    }
   
    private class ParseLow implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseLowPrioBoolOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(medium);
         }       
    }
   
    private class ParseMedium implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseHighPrioBoolOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(high);
         }       
    }
   
    private class ParseHigh implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseLowPrioArithOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(higher);
         }       
    }
   
    private class ParseHigher implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseHighPrioArithOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(highestUnOp);
         }       
    }
    
    /*private class ParseHighestBinOp implements OperatorParser {
         @Override
         public BinOp tryParseOperator() throws ParseError {
             return tryParseHighestPrioBinOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseExp(highestUnOp);
         }       
    }*/
    
    private class ParseHighestUnOp implements OperatorParser {
         @Override
         public UnOp tryParseOperator() throws ParseError {
             return tryParseUnaryOp();
         }

         @Override
         public ASTNode parseTerm() throws ParseError {
             return parseFact();
         }       
    }
   
    private interface OperatorParser {
        public Op tryParseOperator() throws ParseError;
        public ASTNode parseTerm() throws ParseError;
    }
    
    private BinOp tryParseLowestPrioOp() throws ParseError {                
        Token token = _currentToken;
        switch(currentToken) {            
            case Assignment:
                nextToken();
                return new AssignmentOp(token);
            case Parallel:
                nextToken();
                return new ParallelOp(token);
            default:
                return null;
        }
    }
    
    private BinOpBool tryParseHighPrioBoolOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case MoreThan:
                nextToken();
                return new GTOp(token);
            case LessThan:
                nextToken();
                return new LTOp(token);
            case MoreThanEqual:
                nextToken();
                return new GTEOp(token);
            case LessThanEqual:
                nextToken();
                return new LTEOp(token);
            case Equals:
                nextToken();
                return new EqualsOp(token);
            default:
                return null;
        }
    }
    
    private BinOpBool tryParseLowPrioBoolOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case And:
                nextToken();
                return new AndOp(token);
            case Or:
                nextToken();
                return new OrOp(token);
            default:
                return null;
        }
    }
    
    private BinOpArith tryParseLowPrioArithOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case Plus:
                nextToken();
                return new PlusOp(token);
            case Minus:
                nextToken();
                return new MinOp(token);
            default:
                return null;
        }
    }
    
    private BinOpArith tryParseHighPrioArithOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case Div:
                nextToken();
                return new DivOp(token);
            case Times:
                nextToken();
                return new TimesOp(token);
            default:
                return null;
        }
    }
    
    /*private BinOp tryParseHighestPrioBinOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case FieldAccessor:
                nextToken();
                return new AccessorOp(token);
            default:
                return null;
        }
    }*/
    
    private UnOp tryParseUnaryOp() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {            
            case Not:
                nextToken();
                return new NotOp(token);
            default:
                return null;
        }
    }
    
    private boolean isUndefined(ASTNode.Type t) {
        return t == ASTNode.Type.Undefined;
    }

    private boolean isUndefinedOr(ASTNode.Type t, ASTNode.Type t2) {
        return t == ASTNode.Type.Undefined || t == t2;
    }
    
    private boolean consumeToken(Token.Type token) throws ParseError {
        if(currentToken == token) {
            nextToken();
            return true;
        }
        return false;
    }
    
    private boolean consumeToken(Token.Type token, String warningMsg) throws ParseError {
        boolean consumed = consumeToken(token);
        if(!consumed) {
            MainWindow.getConsole().println(warningMsg, Color.ORANGE);
        }
        return consumed;
    }
    
    private Token tryConsumeToken(Token.Type token) throws ParseError {
        Token t = _currentToken;
        if(!consumeToken(token)) {
            return null;
        }
        return t;
    }
    
    private Token parseToken(Token.Type token, String errorMsg) throws ParseError {
        if(currentToken != token) {
            throw new ParseError(errorMsg);
        }
        Token t = _currentToken;
        nextToken();
        return t;
    }
    
    /*private Bool parseBool(String errorMsg) throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case TRUE:
                return new Bool(token, true);
            case FALSE:
                return new Bool(token, false);
            default:
                throw new ParseError(errorMsg);
        }
    }*/
    
    private TypeNode tryParseTypeNode() throws ParseError {
       Token token = _currentToken;
        switch(currentToken) {
            case INT:
                nextToken();
                return new TypeNode(token, TypeNode.Type.Int);
            case BOOL:
                nextToken();
                return new TypeNode(token, TypeNode.Type.Bool);
            case PROC:
                nextToken();
                return new TypeNode(token, TypeNode.Type.Proc);
            default:
                return null;
        }
    }
    
    private Num tryParseNum() throws ParseError {
        Token token = _currentToken;
        switch(currentToken) {
            case Minus:
                nextToken();
                Token t = parseToken(Token.Type.Number, "Number expected after negation");
                Num negNum = new Num(token, -Integer.parseInt(t.value()));
                return negNum;
            case Number:
                Num num = new Num(token, Integer.parseInt(_currentToken.value()));
                nextToken();
                return num;
            default:
                return null;
        }
    }
    
    private Num parseNum(String errorMsg) throws ParseError {
        Num num = tryParseNum();
        if(num == null) {
            throw new ParseError(errorMsg);
        }
        return num;
    }
    
    private ID parseID(String errorMsg) throws ParseError {
        Token token = parseToken(Token.Type.ID, errorMsg);
        ID first_id = new Var(token, token.value());
        
        //@Todo: reverse order?
        return parseAccessorID(first_id);
    }
    
    private ID parseAccessorID(ID parent) throws ParseError {
        int count = 0;
        while(consumeToken(Token.Type.FieldAccessor)) {
            Token token = parseToken(Token.Type.ID, "ID expected after .");
            parent = new AccessorID(token, parent, token.value());
            count++;
        }
        if(count > 1) {
            throw new ParseError("Nested field accesors are not supported: " + parent.getDescription());
        }
        
        return parent;
    }
    
    private Var tryParseVar() throws ParseError {
        Token token = tryConsumeToken(Token.Type.ID);
        if(token == null) {
            return null;
        }
        return new Var(token, token.value());
    }
    
    private Var parseVar(String errorMsg) throws ParseError {
        Token token = parseToken(Token.Type.ID, errorMsg);
        return new Var(token, token.value());
    }
    
    public class ParseError extends Exception {
        public ParseError(String msg) {
            super("Parser error: " + msg);
            MainWindow.getConsole().println("Parser error: " + msg + "\n" + "Current token: '" + _currentToken.value() +
                    "' parsed as " + currentToken + 
                    " @line " + _currentToken.getLineNumber(), Color.red);
        }
    }
    
}

//junk
/*private BinOp tryParseOp() throws ParseError {
        BinOp op = tryParseLowPrioBoolOp();
        if(op != null) {
            return op;
        }
        op = tryParseHighPrioBoolOp();
        if(op != null) {
            return op;
        }
        op = tryParseLowPrioArithOp();
        if(op != null) {
            return op;
        }
        op = tryParseHighPrioArithOp();
        if(op != null) {
            return op;
        }
        op = tryParseLowestPrioOp();
        if(op != null) {
            return op;
        }
        return null;
    }*/
    
    /*private BinOpBool parseBoolOperator(String errorMsg) throws ParseError {
        BinOpBool b = tryParseBoolOperator();
        if(b == null) {
            throw new ParseError(errorMsg);
        }
        return b;
    }
    
    private BinOpArith parseArithOperator(String errorMsg) throws ParseError {
        BinOpArith a = tryParseArithOperator();
        if(a == null) {
            throw new ParseError(errorMsg);
        }
        return a;
    }*/
