/*
 * TransformPostSemanticsUPPAAL.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.ParallelSystem;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.Compiler.CompileError;
import Tools.Utils;

/**
 *
 * @author Peter Maandag
 */
public class TransformPostSemanticsUPPAAL extends TransformPostSemantics {
    public TransformPostSemanticsUPPAAL(Formula tree) {
        super(tree);
    }
    
    @Override
    protected void transformGuards() throws CompileError {
        if(tree instanceof AST.TransitionSystem) {
            super.transformGuards();
        } else if(tree instanceof AST.ParallelSystem) {
            ParallelSystem p = (ParallelSystem) tree;
            AST.Process[] processes = p.getProcess().getDescendants();
            for(AST.Process proc : processes) {
                //Add the globally declared variables too:
                VarDecl[] vars = tree.getVarDecl().getDescendants();
                vars = Utils.concatenate(vars, proc.getVarDecl().getDescendants());

                transformGuards(((Stmt)proc.getSystem()).getDescendants(), 
                        vars);
            }
        } else {
            throw new UnsupportedOperationException("Unsupported formula in transformGuards");
        }      
    }
}
