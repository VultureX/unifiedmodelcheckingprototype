/*
 * InitializerParallelSystem.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.*;
import Tools.Utils;

/**
 *
 * @author Peter Maandag
 */
public class InitializerCompilePS extends Initializer {
    private AST.ParallelSystem p;
    
    public InitializerCompilePS(AST.ParallelSystem p) {
        this.p = p;
    }

    @Override
    public AST.VarDecl[] getVarDecl() {
        VarDecl[] vars = p.getVarDecl().getDescendants();
        AST.Process process = p.getProcess();
        do {
            vars = Utils.concatenate(vars, process.getVarDecl().getDescendants());
            process = process.getNext();
        } while(process != null);
        return vars;
    }

    @Override
    public AST.VarInit[] getVarInit() {
        VarInit[] inits = new AST.VarInit[0];
        AST.Process process = p.getProcess();
        do {
            VarInit init = process.getVarInit();
            if(init != null) {
                inits = Utils.concatenate(inits, init.getDescendants());
            }
            process = process.getNext();
        } while(process != null);
        return inits;
    }

    @Override
    public AST.Stmt[] getStmt() {        
        AST.Process[] processes = p.getProcess().getDescendants();
        Stmt[] stmts = new Stmt[0];
        for(AST.Process proc : processes) {
            stmts = Utils.concatenate(stmts, ((Stmt) proc.getSystem()).getDescendants());
        }
        return stmts;
    }
}