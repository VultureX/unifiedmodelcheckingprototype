/*
 * TransformPreSemantics.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST.ASTNode;
import CodeGeneration.AST.BinOpExp;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.Num;
import CodeGeneration.AST.PlusOp;
import CodeGeneration.AST.Range;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.Var;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.Token;

/**
 *
 * @author Peter Maandag
 */
public class TransformPreSemantics extends Transformation {
    public TransformPreSemantics(Formula tree) {
        super(tree);
    }
    
    public void addMaxStepsVariable() {
        tree.addConstVarDecl(TypeChecker.maxStepsConstant, 
                tree.getMaxSteps().getValue());     
    }
    
    public void addLoopVariable() {
        tree.addVarDeclAndInitFront(TypeChecker.loopVariable, true);
    }
    
    private void addLoopVariableTransitions(Stmt[] stmts) {
        for(int i = 0; i < stmts.length; i++) {
        //while(stmt != null) {
            ASTNode t = new Var(TypeChecker.loopVariable);
            stmts[i].addAssignmentFront(TypeChecker.loopVariable, t);
            //stmt = stmt.getNext();
        //}            
        }
    }
    
    public void addSystemLoopVariableTransitions() throws CompileError {        
        //transformSystem(this::addLoopVariableTransitions, "addLoopVariableTransitions");
        Initializer i = Initializer.getInstance(tree, false);
        addLoopVariableTransitions(i.getStmt());        
    }
    
    public void addSystemStepsVariable() throws CompileError {
        Token token = Token.getInstance();
        tree.addVarDeclAndInitFront(TypeChecker.stepsVariable, 0, new Range(token, 
                new Num(0), new Num(tree.getMaxSteps().getValue())));
        //transformSystem(this::addStepsVariable, "addStepsVariable");
        Initializer i = Initializer.getInstance(tree, false);
        addStepsVariable(i.getStmt());
    }
    
    private void addStepsVariable(Stmt[] stmts) {
        Token token = Token.getInstance();
        /*tree.addVarDeclAndInitFront(TypeChecker.stepsVariable, 0, new Range(token, 
                new Num(0), new Num(tree.getMaxSteps().getValue())));*/
        
        for(int i = 0; i < stmts.length; i++) {
        //while(stmt != null) {
            BinOpExp t =  new BinOpExp(token,new Var(TypeChecker.stepsVariable), 
                    new PlusOp(), new Num(1));
            stmts[i].addAssignmentFront(TypeChecker.stepsVariable, t);
            //stmt = stmt.getNext();
        //}
        }
    }   
}
