/*
 * ParallelSystemInitializer.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.*;
import Tools.Utils;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public class InitializerTransformPS extends Initializer {
    private ParallelSystem p;
    private ArrayList<String> processIDs;
    
    public InitializerTransformPS(ParallelSystem p) {
        this.p = p;
        processIDs = new ArrayList<>();
        getProgramProcesses(p.getProgram(), processIDs);
    }

    @Override
    public VarDecl[] getVarDecl() {
        VarDecl[] variables = new VarDecl[0];
        if(p.getVarDecl() != null) {
            variables = p.getVarDecl().getDescendants();
        }

        for(String pid : processIDs) {
            AST.Process process = findProcess(p, pid);
            variables = Utils.concatenate(variables, process.getVarDecl().getDescendants());
        }
        return variables;
    }

    @Override
    public VarInit[] getVarInit() {
        VarInit[] inits = new VarInit[0];
        VarInit init = p.getVarInit();
        if(init != null) {
            inits = init.getDescendants();
        }

        for(String pid : processIDs) {
            AST.Process process = findProcess(p, pid);
            init = process.getVarInit();
            if(init != null) {
                inits = Utils.concatenate(inits, init.getDescendants());
            }
        }

        return inits;
    }

    @Override
    public Stmt[] getStmt() {
        Stmt[] stmts = new Stmt[0];
        for(String pid : processIDs) {
            AST.Process process = findProcess(p, pid);
            stmts = Utils.concatenate(stmts, ((Stmt) process.getSystem()).getDescendants());
        }
        return stmts;
    }
    
    public ArrayList<AST.Process> getProcesses() {
        ArrayList<AST.Process> processes = new ArrayList();
        for(String pid : processIDs) {
            processes.add(findProcess(p, pid));
        }
        return processes;
    }
        
    private AST.Process findProcess(ParallelSystem p, String id) {
        AST.Process[] processes = p.getProcess().getDescendants();
        for(AST.Process process : processes) {
            if(process.getName().getValue().equals(id)) {
                return process;
            }
        }
        throw new UnsupportedOperationException("This should not happen");
    }
    
    private void getProgramProcesses(ASTNode program, ArrayList<String> ids) {
        if(program instanceof Var) {
            ids.add(((Var) program).getValue());
        } else if(program instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) program;
            getProgramProcesses(exp.lhs(), ids);
            getProgramProcesses(exp.rhs(), ids);
        } else {
            throw new UnsupportedOperationException("unknown class in getProgramProcesses: "
                    + program.getClass());
        }
    }
}
