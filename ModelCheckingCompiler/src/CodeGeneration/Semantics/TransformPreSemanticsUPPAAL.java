/*
 * TransformPreSemanticsUPPAAL.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.ParallelSystem;
import CodeGeneration.AST.TransitionSystem;

/**
 *
 * @author Peter Maandag
 */
public class TransformPreSemanticsUPPAAL extends TransformPreSemantics {
    public TransformPreSemanticsUPPAAL(Formula tree) {
        super(tree);
    }
    
    @Override
    public void addLoopVariable() {
        if(tree instanceof TransitionSystem) {
            tree.addVarDeclAndInitFront(TypeChecker.loopVariable, true);
        } else if(tree instanceof ParallelSystem) {
            ParallelSystem p = (ParallelSystem) tree;
            //InitializerTransformPS init = new InitializerTransformPS(p);
            //ArrayList<AST.Process> processes = init.getProcesses();
            for(AST.Process proc : p.getProcess().getDescendants()) {
                proc.addVarDeclAndInitFront(TypeChecker.loopVariable, true);
            }
        } else {
            throw new UnsupportedOperationException("Unsupported formula in addLoppVariable");
        }
    }
}
