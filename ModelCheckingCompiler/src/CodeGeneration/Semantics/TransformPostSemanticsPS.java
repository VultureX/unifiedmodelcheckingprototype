/*
 * TransformPostSemantics.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.ASTNode;
import CodeGeneration.AST.AccessorID;
import CodeGeneration.AST.ParallelSystem;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.TypeNode;
import CodeGeneration.AST.Var;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarInit;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.Token;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public class TransformPostSemanticsPS extends TransformPostSemantics {
    private ArrayList<String> declaredVars = new ArrayList<>();
    
    public TransformPostSemanticsPS(ParallelSystem tree) {
        super(tree);
    }
    
    @Override
    public void transformTree() throws CompileError {
        super.transformTree();
        mergeProcesses();
    }
    
    private void mergeProcesses() throws CompileError {
        if(tree instanceof ParallelSystem) {
            ParallelSystem p = (ParallelSystem) tree;
            setDeclaredVars(p);
            mergeProcesses(p);
        }
    }
    
    private void mergeProcesses(ParallelSystem p) throws CompileError {
        ArrayList<AST.Process> newProcesses = new ArrayList<>();
        VarDecl[] variables = p.getVarDecl().getDescendants();
        for(int i = variables.length-1; i >= 0; i--) {
            if(variables[i].getTypeNode().getType() == TypeNode.Type.Proc) {
                String processID = (String) variables[i].getDeclaredValue().getValue();
                AST.Process process = findProcess(processID);
                process = transformProcess(variables[i].getVar(), process);
                newProcesses.add(process);
                variables[i].remove();
            }
        }       
        
        p.replaceProcesses(newProcesses);
    }
    
    private AST.Process transformProcess(Var processVariable, AST.Process process) {
        //Make a copy of the process and rename it
        process = (AST.Process) process.copy(null, processVariable);
        
        VarDecl[] variables = process.getVarDecl().getDescendants();
        for(int i = variables.length-1; i >= 0; i--) {
            //Get a unique name for each variable:
            Var oldVar = variables[i].getVar();
            String name = getUniqueName(process.getName().getValue(),
                    oldVar.getValue());
            Var replacement = new Var(Token.getInstance(name), name);
            
            //Replace variables everywhere:
            transformProcessVarDecl(process, oldVar, replacement);
            transformProcessInit(process, oldVar, replacement);
            transformProcessSystem(process, oldVar, replacement);
            
            //Replace field accessors:
            if(tree.getGoal() != null) {
                Var processVarCopy = (Var) processVariable.copy();
                AccessorID pVar = new AccessorID(oldVar.getToken().copy(), processVarCopy, oldVar.getValue());
                tree.getGoal().replace(pVar, replacement);
            }
        }
        
        return process;
    }
    
    private void transformProcessSystem(AST.Process process, Var variable, Var replacement) {
        ASTNode system = process.getSystem();
        if(system instanceof Stmt) {
            Stmt stmt = (Stmt) system;
            Stmt[] variables = stmt.getDescendants();
            for(int i = variables.length-1; i >= 0; i--) {
                //System.out.println("OLD, replace " + variable.getValue() + " with " + replacement.getValue());
                //System.out.println(variables[i].flattenToken());
                variables[i].replace(variable, replacement);
                //System.out.println("NEW, replaced " + variable.getValue() + " with " + replacement.getValue());
                //System.out.println(variables[i].flattenToken());
            }
        } else {
            throw new UnsupportedOperationException("Unsupported system in transformProcessSystem: " 
                    + system.getClass());
        }
    }
    
    private void transformProcessInit(AST.Process process, Var variable, Var replacement) {
        if(process.getVarInit() != null) {
            VarInit[] variables = process.getVarInit().getDescendants();
            for(int i = variables.length-1; i >= 0; i--) {
                variables[i].getExpression().replace(variable, replacement);            
            }
        }
    }
    
    private void transformProcessVarDecl(AST.Process process, Var variable, Var replacement) {
        VarDecl[] variables = process.getVarDecl().getDescendants();
        for(int i = variables.length-1; i >= 0; i--) {
            if(variables[i].getVar().equals(variable)) {
                variables[i].replace(variable, replacement);
            }
        }
    }
    
    private AST.Process findProcess(String id) throws CompileError {
        ParallelSystem p = (ParallelSystem) tree;
        AST.Process[] processes = p.getProcess().getDescendants();
        for(AST.Process process : processes) {
            if(process.getName().getValue().equals(id)) {
                return process;
            }
        }
        throw new CompileError("This should not happen");
    }
    
    private void setDeclaredVars(ParallelSystem p) {            
        VarDecl[] variables = p.getVarDecl().getDescendants();
        this.declaredVars = new ArrayList<>();
        for(int i = variables.length-1; i >= 0; i--) {
            String name = variables[i].getVar().getValue();
            declaredVars.add(name);
        }            

//        AST.Process[] processes = p.getProcess().getDescendants();
//        for(AST.Process process : processes) {
//            VarDecl[] processVars = process.getVarDecl().getDescendants();
//            for(int i = processVars.length-1; i >= 0; i--) {
//                String name = getUniqueName(process.getName().getValue(), 
//                        processVars[i].getVar().getValue(), declaredVars);
//                declaredVars.add(name);
//                processVars[i].setUniqueID(name);
//            }
//        }     
    }
    
    private String getUniqueName(String processID, String variable) {
        String uniqueName = processID + "_" + variable;
        String _uniqueName = uniqueName;
        int count = 1;
        while(declaredVars.contains(_uniqueName)) {
            _uniqueName = uniqueName + count++;
        }
        declaredVars.add(_uniqueName);
        return _uniqueName;
    }
}
