/*
 * AbstractTypeChecker.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.*;
import CodeGeneration.AST.ASTNode.Type;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.Token;
import Tools.Utils;
import static Tools.Utils.newLine;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public abstract class TypeChecker {
    public static final String stepsVariable = "steps";
    public static final String loopVariable = "loop";
    public static final String maxStepsConstant = "MAXSTEPS";
    public static final String loopVariable1 = "loopi";
    public static final String loopVariable2 = "loopj";
    public static final String loopPropertyVariable = "loopp";
    public static final String clockVariable = "klok";
    public static final String stateVariable = "state";
    public static final String processKeyword = "process";
    
    protected final String[] restrictedVars = {
        stepsVariable, loopVariable, maxStepsConstant, //General
        loopVariable1, loopVariable2, loopPropertyVariable, //Yices
        clockVariable, stateVariable, processKeyword //UPPAAL
    };
    
    private Formula mainFormula;
    //private ASTNode[] globalVars;
    
    /*public AbstractTypeChecker(Formula tree) {
        this(tree, new ASTNode[]{});
    }*/
    
    public TypeChecker(Formula tree) {
        this.mainFormula = tree;
        //this.globalVars = globalVars;
    }
    
    public static TypeChecker getInstance(Formula f) {
        if(f instanceof ParallelSystem) {
            return new TypeCheckerParallelSystem((ParallelSystem)f);
        } else if(f instanceof TransitionSystem) {
            return new TypeCheckerTransitionSystem((TransitionSystem)f);
        } else {
            throw new UnsupportedOperationException("Unrecognized formula in Typechecker.getInstance");
        }
    }
    
    public abstract void performChecks() throws CompileError;    
    protected abstract Type findVarType(ID id) throws CompileError;
    public abstract VarDecl tryFindVar(ID id);
    public abstract VarDecl findVar(ID id) throws CompileError;
    
    protected boolean isRestrictedVariable(Token.Type tokenType, String id) {
        return tokenType != Token.Type.Generated && Utils.contains(restrictedVars, id);
    }
    
    public Type getTypeOf(ASTNode node) throws CompileError {       
        node = AST.filterPars(node);
        
        if(node instanceof AccessorID) {
            return getTypeOf((AccessorID) node);
        } else if(node instanceof Var) {
            return findVarType((Var)node);
        } else if(node instanceof BinOpExp) {
            return getTypeOf((BinOpExp) node);
        } else if(node instanceof UnOpExp) {
            UnOpExp exp = (UnOpExp) node;
            Type opType = getTypeOf(exp.getOp());
            Type expType = getTypeOf(exp.getExp());
            if(opType != expType) {
                throw new CompileError(exp.getOp().getDescription() +
                        " does not have the same type as " + exp.getExp().getDescription() +
                        newLine + opType + " != " + expType);
            }
            return getTypeOf(exp.getExp());
        } else {
            if(node != null) {
                if(node.getType() != Type.Undefined) {
                    return node.getType();
                }
                throw new UnsupportedOperationException("Unsupported class in getTypeOf: " 
                        + node.getClass());           
            }
            throw new UnsupportedOperationException("Undefined type for " 
                    + node.getDescription());
            //return Type.Undefined;
        }
    }
    
    private Type getTypeOf(AccessorID id) throws CompileError {
        ID parent = id;
        ID parent2;
        do {
            parent2 = parent;
            parent = parent2.getNext();
            if(parent != null) {
                Type type = getTypeOf(parent);
                if(type != Type.Proc) {
                    throw new CompileError("Only process variables can have accessable fields" + newLine
                            + parent.getDescription() + " is not a process variable");
                }
            }
        } while(parent != null);
        
        Type t = findVarType(id);
        return t;
    }
        
    private Type getTypeOf(BinOpExp exp) throws CompileError {
        ASTNode lhs = AST.filterPars(exp.lhs());
        ASTNode rhs = AST.filterPars(exp.rhs());
        Type leftType = getTypeOf(lhs);
        Type rightType = getTypeOf(rhs);       
        
        BinOp op = exp.getOp();
        if(op instanceof BinOpArith) {
            if(leftType != Type.Int || rightType != Type.Int) {
                throw new CompileError("Arithmetic expression expected near " + op.getDescription() + " instead of "
                        + ((leftType != Type.Int) ? lhs.getDescription() : "") 
                        + ((rightType != Type.Int) ? (((leftType != Type.Int) ? " and " : "") + rhs.getDescription()) : ""));
            }
            return Type.Int;
        } else if(op instanceof BinOpBool) {
            //Should match one on one with parser checkExp function
            if(op instanceof BinOpBoolCompareArith) {
                if(leftType != Type.Int || rightType != Type.Int) {
                    throw new CompileError("Arithmetic expression expected near " + op.getDescription() + " instead of "
                        + ((leftType != Type.Int) ? lhs.getDescription() : "")
                        + ((rightType != Type.Int) ? (((leftType != Type.Int) ? " and " : "") + rhs.getDescription()) : ""));
                }
            } else if(op instanceof BinOpBoolCompare) {
                if(leftType != Type.Int && leftType != Type.Bool) {
                    throw new CompileError("Integer or Boolean expression expected near " + op.getDescription() + " instead of " + lhs.getDescription());
                }
                if(leftType != rightType) {
                    if(leftType == Type.Int) {
                        throw new CompileError("Arithmetic expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());
                    } else if(leftType == Type.Bool) {
                        throw new CompileError("Boolean expression expected near " + op.getDescription() + " instead of " + rhs.getDescription());                             
                    } else {
                        throw new UnsupportedOperationException("Unhandled type in getTypeOf: " 
                               + leftType + " " + rightType);
                    }
                }                     
            } else if(op instanceof BinOpBoolConnect) {
                if(leftType != Type.Bool || rightType != Type.Bool) {
                    throw new CompileError("Boolean expression expected near " + op.getDescription() + " instead of "
                        + ((leftType != Type.Bool) ? lhs.getDescription() : "")
                        + ((rightType != Type.Bool) ? (((leftType != Type.Bool) ? " and " : "") + rhs.getDescription()) : ""));
                }
            } else {
                throw new UnsupportedOperationException("Unhandled type in getTypeOf: " 
                               + leftType + " " + rightType);
            }
            return Type.Bool;
        } else if(op instanceof BinOpProc) {
            if(leftType != Type.Proc || rightType != Type.Proc) {
                throw new CompileError("Process expression expected near " + op.getDescription() + " instead of "
                        + ((leftType != Type.Proc) ? lhs.getDescription() : "") 
                        + ((rightType != Type.Proc) ? (((leftType != Type.Proc) ? " and " : "") + rhs.getDescription()) : ""));
            }
            return Type.Proc;
        } else {
            //Assignment operator may only assign to plain variables:            
            if(!(lhs instanceof Var)) {
                throw new CompileError("Trying to assign a value to " + lhs.getDescription());
            }
            if(leftType != rightType) {
                throw new CompileError(lhs.getDescription()
                        + " does not have the same type as " + rhs.getDescription()
                        + newLine + leftType + " != " + rightType);
            }
            return Type.Void;
        }
    }
    
    protected void addUnseenVarsFromExpression(ASTNode node, ArrayList<String> vars) throws CompileError {            
        node = AST.filterPars(node);
        
        if(node instanceof Num) {
            
        } else if(node instanceof Bool) {
            
        } else if(node instanceof Var) {
            String id = ((Var)node).getValue();
            if(!vars.contains(id)) {
                vars.add(id);
            }            
        } else if(node instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) node;
            addUnseenVarsFromExpression(exp.lhs(), vars);
            addUnseenVarsFromExpression(exp.rhs(), vars);
        } else {
            if(node != null) {
                throw new UnsupportedOperationException("Unsupported class in getTypeOf: " + node.getClass());            
            }
        }
    }
    
    protected VarDecl tryFindVar(Formula f, ID id) {
        VarDecl dec = f.getVarDecl();
        do {
            if(dec.getVar().equals(id)) {
                return dec;
            }
            dec = dec.getNext();
        } while(dec != null);
        
        return null;
    }
    
    protected VarDecl findVar(Formula f, ID id) throws CompileError {
        VarDecl var = tryFindVar(f, id);
        if(var == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        }
        return var;
    }
    
    /*private Type findVarType(ID id) throws CompileError {       
        if(tree instanceof ParallelSystem) {
            ParallelSystem pTree = (ParallelSystem) tree;
            AST.Process proc = pTree.getProcess();
            do {
                if(id instanceof AccessorID) {
                    ID rootVar = ((AccessorID)id).getRoot();
                    VarDecl varDecl = findVar(tree, rootVar);
                    Value v = varDecl.getDeclaredValue();
                    AST.Process p = tryFindProcess(pTree, (String)v.getValue());
                    
                    return findVar(p, id).getTypeNode().getType();
                    
                    //throw new UnsupportedOperationException("can't check accessor id's yet");
                }
                if(id.equals(proc.getName())) {
                    return Type.Proc;
                }
                proc = proc.getNext();
            } while(proc != null);
        }
        
        if(id instanceof AccessorID) {                
            throw new UnsupportedOperationException("can't check accessor id's yet");
        }        
        VarDecl dec = tryFindVar(tree, id);      
        
        if(dec == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        } else {
            return dec.getTypeNode().getType();
        }
    }
    
    protected VarDecl tryFindVar(Formula f, ID id) {
        VarDecl dec = f.getVarDecl();
        do {
            if(dec.getVar().equals(id)) {
                return dec;
            }
            dec = dec.getNext();
        } while(dec != null);
        
        return null;
    }
    
    protected VarDecl findVar(Formula f, ID id) throws CompileError {
        VarDecl var = tryFindVar(f, id);
        if(var == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        }
        return var;
    }
    
    protected AST.Process tryFindProcess(ParallelSystem s, String id) {
        AST.Process p = s.getProcess();
        AST.Process p2 = p;
        do {
            p = p2;
            
            if(p.getName().getValue().equals(id)) {
                return p;
            }
            
            p2 = p.getNext();
        } while(p2 != null);
        
        return null;
    }*/
    
}
