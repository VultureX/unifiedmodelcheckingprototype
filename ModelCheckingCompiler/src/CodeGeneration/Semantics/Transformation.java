/*
 * Transformation.java
 * Copyright(c) 2014
 */
package CodeGeneration.Semantics;

import CodeGeneration.AST.*;

/**
 *
 * @author Peter Maandag
 */
public abstract class Transformation {
    protected Formula tree;
    protected TypeChecker typeChecker;
    
    public Transformation(Formula tree) {
        this.tree = tree;
        this.typeChecker = TypeChecker.getInstance(tree);
    }
    
    
    
    //Function pointers:
    /*private void transformSystem(Action1E<Stmt, CompileError> transform, String name) throws CompileError {
        if(tree instanceof TransitionSystem) {
            Stmt stmts = ((TransitionSystem)tree).getStmt(); 
            transform.invoke(stmts);
        } else if(tree instanceof ParallelSystem) {
            AST.Process[] processes = ((ParallelSystem)tree).getProcess().getDescendants();
            for(AST.Process p : processes) {
                transform.invoke((Stmt)p.getSystem());
            }            
        } else {
            throw new UnsupportedOperationException("Unrecognized Formula in " + name);
        }
    }*/
    
    /*private void transformSystem(Action1E<Stmt[], CompileError> transform, String name) throws CompileError {
        if(tree instanceof TransitionSystem) {
            Stmt[] stmts = ((TransitionSystem)tree).getStmt().getDescendants(); 
            transform.invoke(stmts);
        } else if(tree instanceof ParallelSystem) {
            AST.Process[] processes = ((ParallelSystem)tree).getProcess().getDescendants();
            Stmt[] stmts = new Stmt[0];
            for(AST.Process proc : processes) {
                stmts = Utils.concatenate(stmts, ((Stmt) proc.getSystem()).getDescendants());
            }
            transform.invoke(stmts);
        } else {
            throw new UnsupportedOperationException("Unrecognized Formula in " + name);
        }
    }*/
    
    /*private VarDecl[] getAllVarDecl() {
        VarDecl[] vars = tree.getVarDecl().getDescendants();
        if(tree instanceof ParallelSystem) {
            ParallelSystem p = (ParallelSystem) tree;
            AST.Process process = p.getProcess();
            do {
                vars = Utils.concatenate(vars, process.getVarDecl().getDescendants());
                process = process.getNext();
            } while(process != null);
        }
        return vars;
    }
    
    private VarInit[] getAllVarInit() {
        VarInit[] inits = new VarInit[0];
        if(tree.getVarInit() != null) {
            inits = tree.getVarInit().getDescendants();
        }
        
        if(tree instanceof ParallelSystem) {
            ParallelSystem p = (ParallelSystem) tree;
            AST.Process process = p.getProcess();
            do {
                VarInit init = process.getVarInit();
                if(init != null) {
                    inits = Utils.concatenate(inits, init.getDescendants());
                }
                process = process.getNext();
            } while(process != null);
        }
        return inits;
    }*/
}
