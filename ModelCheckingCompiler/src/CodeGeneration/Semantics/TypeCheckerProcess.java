/*
 * TypeCheckerProcess.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.ASTNode.Type;
import CodeGeneration.AST.*;
import CodeGeneration.Compiler.CompileError;
import java.util.ArrayList;
import static Tools.Utils.newLine;

/**
 *
 * @author Peter Maandag
 */
public class TypeCheckerProcess extends TypeChecker {
    private Formula mainFormula;
    private AST.Process process;
    private ArrayList<ID> globalVars;
    
    public TypeCheckerProcess(Formula main, AST.Process p, ArrayList<ID> globalVars) {
        super(main);
        this.mainFormula = main;
        this.process = p;
        this.globalVars = globalVars;
    }
    
    @Override
    public void performChecks() throws CompileError {
        checkVarDecl();        
        checkVarInit();
        checkSystem();
    }

    @Override
    protected Type findVarType(ID id) throws CompileError {      
        VarDecl dec = tryFindVar(id);      
        
        if(dec == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        } else {
            return dec.getTypeNode().getType();
        }
    }
    
    public AST.Process getProcess() {
        return process;
    }
    
    private void checkVarDecl() throws CompileError {
        //@Todo: improve the checking of Process names!
        if(isRestrictedVariable(process.getName().getToken().getType(), process.getName().getValue())) {
            throw new CompileError("Cannot use restricted name " + process.getName().getDescription());
        }
        
        VarDecl[] vars = process.getVarDecl().getDescendants();
        ArrayList<ID> seenVars = new ArrayList<>(); //Make a copy
        seenVars.addAll(globalVars);

        for(int i = 0; i < vars.length; i++) {
            VarDecl var = vars[i];
            if(isRestrictedVariable(var.getToken().getType(), var.getVar().getValue())) {
                throw new CompileError("Cannot declare restricted variable " + var.getVar().getDescription());
            }
            if(seenVars.contains(var.getVar())) {
                throw new CompileError(var.getVar().getDescription() + " is declared twice");
            }
            if(getTypeOf(var.getTypeNode()) == Type.Proc) {                
                throw new CompileError("Cannot define " + var.getDescription() + " in transition system");                
            }
            if(var.isConstant()) {
                ASTNode constValue = var.getDeclaredValue();
                ASTNode typeNode = var.getTypeNode();
                Type assType = getTypeOf(constValue);
                Type varType = getTypeOf(typeNode);
                if(assType != varType) {
                    throw new CompileError("Type of " + var.getVar().getDescription()
                        + " does not match with type of " + constValue.getDescription()
                        + newLine + varType + " != " + assType);
                }
            }
            seenVars.add(var.getVar());
        }
    }
    
    private void checkVarInit() throws CompileError {
        ArrayList<String> seenVars = new ArrayList<>();
        ArrayList<String> seenAssignments = new ArrayList<>();
        if(process.getVarInit() != null) {
            VarInit[] inits = process.getVarInit().getDescendants();
            
            for(int i = 0; i < inits.length; i++) {
                VarInit init = inits[i];
                ASTNode expression = init.getExpression();
                Type type = getTypeOf(expression);
                init.setType(type);
                addUnseenVarsFromExpression(expression, seenVars);
                
                if(type == Type.Void) {
                    BinOpExp assignment = (BinOpExp) AST.filterPars(expression);
                    ID id = (Var)AST.filterPars(assignment.lhs()); //@Todo give typeerror for accessorIDs
                    VarDecl var = findVar(id);
                    
                    if(seenAssignments.contains(id.getValue())) {
                        throw new CompileError("Variable " + id.getDescription() + " is assigned a value twice");
                    }
                    if(isRestrictedVariable(assignment.getToken().getType(), id.getValue())) {
                        throw new CompileError("Cannot assign value to restricted variable " + id.getDescription());
                    }                   
                    if(var.isConstant()) {
                        throw new CompileError("Constant variable " + id.getDescription() + " must only be initialized in VAR block");
                    }

                    seenAssignments.add(id.getValue());
                }
            }
        }
    }
    
    private void checkVarAss(VarAss ass) throws CompileError {
        VarAss[] parents = ass.getDescendants();
        
        for(int i = parents.length-1; i >= 0; i--) {
            ass = parents[i];
            Type varType = getTypeOf(ass.getID());
            Type assType = getTypeOf(ass.getAss());
            VarDecl var = findVar(ass.getID());
            if(varType == Type.Proc) {
                throw new CompileError("Cannot assign process variable: " + ass.getID().getDescription());
            }
            if(ass.getID() instanceof AccessorID) {
                throw new CompileError("Cannot assign process field: " + ass.getID().getDescription());
            }
            if(assType != varType) {
                throw new CompileError("Type of " + ass.getID().getDescription() 
                    + " does not match type of " + ass.getAss().getDescription() 
                    + newLine + varType + " != " + assType);
            }
            if(isRestrictedVariable(ass.getToken().getType(), var.getVar().getValue())) {
                throw new CompileError("Cannot assign value to restricted variable " + ass.getID().getDescription());
            }
            if(var.isConstant() || var.isNondeterministic()) {
                throw new CompileError("Cannot assign value to constant or nondet declared variable " + ass.getID().getDescription());
            }
        }
    }
    
    private void checkGuard(ASTNode guard) throws CompileError {
        if(guard != null) {
            Type type = getTypeOf(guard);
            if(type != Type.Bool) {
                throw new CompileError("Guard expression is not a boolean expression");
            }
        }
    }
    
    private void checkStmt(Stmt[] allStmt) throws CompileError {
        for(int i = allStmt.length-1; i >= 0; i--) {
            Stmt s = allStmt[i];
            checkGuard(s.getGuard());
            checkVarAss(s.getEffect());            
        }
    }
    
    private void checkSystem() throws CompileError {
        ASTNode system = process.getSystem();
        if(system instanceof Stmt) {
            Stmt[] stmts = ((Stmt)system).getDescendants(); 
            checkStmt(stmts);
        } else {
            throw new UnsupportedOperationException("Unrecognized Formula in checkSystem()");
        }        
    }
    
    @Override
    public VarDecl tryFindVar(ID id) {
        VarDecl var = tryFindVar(process, id);
        if(var != null) {
            return var;
        }
        var = tryFindVar(mainFormula, id);
        return var;
    }
    
    @Override
    public VarDecl findVar(ID id) throws CompileError {
        VarDecl var = tryFindVar(id);
        if(var == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        }
        return var;
    }

}
