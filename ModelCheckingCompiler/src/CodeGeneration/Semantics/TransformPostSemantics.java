/*
 * TransformPostSemantics.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.ASTNode;
import CodeGeneration.AST.ASTNode.Type;
import CodeGeneration.AST.BinOpExp;
import CodeGeneration.AST.BinOpExpBool;
import CodeGeneration.AST.Exp;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.ID;
import CodeGeneration.AST.ParenExp;
import CodeGeneration.AST.Range;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.UnOpExp;
import CodeGeneration.AST.UnOpExpBool;
import CodeGeneration.AST.Value;
import CodeGeneration.AST.Var;
import CodeGeneration.AST.VarAss;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarDeclRanged;
import CodeGeneration.AST.VarInit;
import CodeGeneration.Compiler.CompileError;
import CodeGeneration.Token;
import Tools.Lambda.Function1;

/**
 *
 * @author Peter Maandag
 */
public class TransformPostSemantics extends Transformation {
    private Initializer init;
    
    public TransformPostSemantics(Formula tree) {
        super(tree);
        init = Initializer.getInstance(tree, false);
    }
    
    /*public static TransformPostSemantics getInstance(Formula tree) {
        if(tree instanceof AST.TransitionSystem) {
            return new TransformPostSemanticsTS((AST.TransitionSystem) tree);
        } else if(tree instanceof AST.ParallelSystem) {
            return new TransformPostSemanticsPS((AST.ParallelSystem) tree);
        } else {
            throw new UnsupportedOperationException("Unsupported formula in getInstance");
        }
    }*/

    public void transformTree() throws CompileError {        
        //Initializer i = Initializer.getInstance(tree, false);
        transformVarDecl(init.getVarDecl());
        transformTransitions(init.getStmt());
        transformVarInit(init.getVarInit());
        transformExpression(tree.getGoal()); //@todo change to initializer
        removeConstVarDecl(init.getVarDecl());
        
        transformGuards();
        
        //transformVarDecl(getAllVarDecl());
        //transformSystem(this::transformTransitions, "transformTransitions");
        //transformVarInit(getAllVarInit());
        //transformExpression(tree.getGoal());
        //removeConstVarDecl(getAllVarDecl());
    }
    
    protected void transformGuards() throws CompileError {
        transformGuards(init.getStmt(), init.getVarDecl());
    }
    
    protected void transformGuards(Stmt[] stmts, VarDecl[] vars) throws CompileError {
        for (Stmt stmt : stmts) {
            //Add to each guard expressions that ensure the corresponding update
            //keeps the variable in range
            Exp guard = stmt.getGuard();
            VarAss[] assignments = stmt.getEffect().getDescendants();
            
            for(VarAss ass : assignments) {
                //Get each variable that is being updated in this update rule
                VarDecl var = getVar(ass.getID(), vars);
                
                if(var instanceof VarDeclRanged) {
                    VarDeclRanged rVar = (VarDeclRanged) var;
                    BinOpExpBool smaller = new BinOpExpBool(Token.getInstance(), ass.getAss().copy(), new AST.LTEOp(), rVar.getRange().getMax().copy());
                    
                    //Only do the smaller than check for the steps variable
                    if(rVar.getVar().getValue().equals(TypeChecker.stepsVariable)) {
                        guard = (guard == null) ? smaller : new BinOpExpBool(Token.getInstance(), guard, new AST.AndOp(), smaller);
                    } else {
                        BinOpExpBool bigger = new BinOpExpBool(Token.getInstance(), ass.getAss().copy(), new AST.GTEOp(), rVar.getRange().getMin().copy());
                        BinOpExpBool and = new BinOpExpBool(Token.getInstance(), smaller, new AST.AndOp(), bigger);
                        guard = (guard == null) ? and : new BinOpExpBool(Token.getInstance(), guard, new AST.AndOp(), and);
                    }                    
                }                
            }
            
            stmt.setGuard(guard);
        }
    }
    
    private boolean isIdentityRuleNeeded(Stmt[] stmts, VarDecl[] vars) {
        //Check if the identity rule is needed:
        if(tree.getMode() == Formula.Mode.Loop || tree.getMode() == Formula.Mode.PropertyLoop) {
            return true;
        }
        
        for (Stmt stmt : stmts) {
            Exp guard = stmt.getGuard();
            VarAss[] assignments = stmt.getEffect().getDescendants();
            boolean hasRangedVar = false;
            for(VarAss ass : assignments) {
                //Get each variable that is being updated in this update rule
                VarDecl var = getVar(ass.getID(), vars);
                if(var instanceof VarDeclRanged) {
                    hasRangedVar = true;
                }
            }
            
            //If a rules exists without a guard and range it is always applicable:
            if(guard == null && !hasRangedVar) {
                return false;
            }
        }
        
        return true;
    }
    
    public void addIdentityRuleYices() {        
        addIdentityRule(var -> var instanceof VarDeclRanged && 
                    !var.getVar().getValue().equals(TypeChecker.stepsVariable));
    }
    
    public void addIdentityRuleNuSMV() {        
        addIdentityRule(var -> var instanceof VarDeclRanged);
    }
    
    private void addIdentityRule(Function1<VarDecl, Boolean> condition) {
        Stmt[] stmts = init.getStmt();
        VarDecl[] vars = init.getVarDecl();
        if(!isIdentityRuleNeeded(stmts, vars)) {
            return;
        }

        Exp idCondition = getIdentityCondition(stmts, vars, condition);
        addEqualities(vars, idCondition);
    }
    
    private Exp getIdentityCondition(Stmt[] stmts, VarDecl[] vars, Function1<VarDecl, Boolean> rangedVarCondition) {       
        //Get the condition
        Exp condition = null;
        for (Stmt stmt : stmts) {
            Exp guard = stmt.getGuard();
            guard = (guard != null) ? new ParenExp(Token.getInstance(), guard) : guard;
            
//            //Add to each guard expressions that ensure the corresponding update
//            //keeps the variable in range
//            VarAss[] assignments = stmt.getEffect().getDescendants();            
//            for(VarAss ass : assignments) {
//                //Get each variable that is being updated in this update rule
//                VarDecl var = getVar(ass.getID(), vars);
//                
//                if(rangedVarCondition.invoke(var) /*var instanceof VarDeclRanged && !var.getVar().getValue().equals(TypeChecker.stepsVariable)*/) {
//                    //@todo: optimize steps range check
//                    VarDeclRanged rVar = (VarDeclRanged) var;
//                    BinOpExpBool smaller = new BinOpExpBool(Token.getInstance(), ass.getAss().copy(), new AST.LTEOp(), rVar.getRange().getMax().copy());
//                    BinOpExpBool bigger = new BinOpExpBool(Token.getInstance(), ass.getAss().copy(), new AST.GTEOp(), rVar.getRange().getMin().copy());
//                    BinOpExpBool and = new BinOpExpBool(Token.getInstance(), smaller, new AST.AndOp(), bigger);
//                    guard = (guard == null) ? and : new BinOpExpBool(Token.getInstance(), guard.copy(), new AST.AndOp(), and);                    
//                }
//            }            
//            guard = (guard != null) ? new ParenExp(Token.getInstance(), guard) : guard;
            
            //disjunct all rules:      
            condition = (condition == null) ? guard : new BinOpExpBool(Token.getInstance(), condition, new AST.OrOp(), guard);
        }
        
        //negation
        if(condition == null) {
            return null;
        }
        condition = new ParenExp(Token.getInstance(), condition);
        condition = new UnOpExpBool(Token.getInstance(), new AST.NotOp(), condition);
        
        return condition;
    }
    
    /*private void addEqualitiesYices(VarDecl[] vars, Exp condition) {
        //Add all variables equal to themselves
        //Todo: this happens anyway because all unseen variables are added in SMT?
        Stmt s = new Stmt(Token.getInstance(), null, condition, null);
        for (VarDecl variable : vars) {
            String var = variable.getVar().getValue();
            s.addAssignmentFront(var, new Var(var));
        }
        tree.addStmt(s);
    }*/
    
    private void addEqualities(VarDecl[] vars, Exp guard) {
        //Add all variables equal to themselves
        Stmt s = new Stmt(Token.getInstance(), null, guard, null);
        for (VarDecl variable : vars) {
            String var = variable.getVar().getValue();
            if(var.equals(TypeChecker.loopVariable)) {
                s.addAssignmentFront(var, new AST.Bool(false));
            } else {
                s.addAssignmentFront(var, new Var(var));
            }            
        }
        tree.addStmt(s);
    }
    
    private VarDecl getVar(ID id, VarDecl[] vars) {
        for(VarDecl var : vars) {
            if(var.getVar().getValue().equals(id.getValue())) {
                return var;
            }
        }
        return null;
    }
    
    private void transformVarAss(VarAss ass) throws CompileError {
        while(ass != null) {
            transformExpression(ass.getAss());
            ass = ass.getNext();
        }
    }
    
    private void transformTransitions(Stmt[] stmts) throws CompileError {
        for (Stmt stmt : stmts) {
            transformExpression(stmt.getGuard());
            transformVarAss(stmt.getEffect());
        }
    }
    
    private void transformVarDecl(VarDecl[] vars) throws CompileError {
        for (VarDecl var : vars) {
            if(var.isConstant()) {
                transformExpression(var.getDeclaredValue());
            }
        }
        
        for (VarDecl var : vars) {
            if (var instanceof VarDeclRanged) {
                VarDeclRanged rangedVar = (VarDeclRanged) var;
                Range r = rangedVar.getRange();
                if(r != null) {
                    transformExpression(r.getMin());
                    transformExpression(r.getMax());
                }                
            }
        }
    }
    
    private void transformVarInit(VarInit[] inits) throws CompileError {
        for (VarInit init : inits) {
            ASTNode expression = init.getExpression();
            Type type = typeChecker.getTypeOf(expression);

            if(type == Type.Void) {

                BinOpExp assignment = (BinOpExp) AST.filterPars(expression);
                transformExpression(assignment.rhs());
            }
        }
    }
    
    private void transformExpression(ASTNode node) throws CompileError {
        node = AST.filterPars(node);
        
        if(node instanceof Var) {
            VarDecl var = typeChecker.findVar((Var)node);
            if(var.isConstant()) {
                node.getParent().replace(node, var.getDeclaredValue());
            }
        } else if(node instanceof Value) {
            
        } else if(node instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) node;
            transformExpression(exp.lhs());
            transformExpression(exp.rhs());
        } else if(node instanceof UnOpExp) {
            UnOpExp exp = (UnOpExp) node;
            transformExpression(exp.getExp());
        } else {
            if(node != null) {
                throw new UnsupportedOperationException("Unsupported class in transformExpression: " 
                        + node.getClass());            
            }
        }
    }
    
    private void removeConstVarDecl(VarDecl[] vars) throws CompileError {              
        for (VarDecl var : vars) {
            if(var.isConstant()) {
                //var.remove(var.getID());
                var.remove();
            }
        }
    }
        
    public void addSystemUnseenVars() throws CompileError {
        //transformSystem(this::addUnseenVarsToTransitions, "addUnseenVarsToTransitions");
        Initializer i = Initializer.getInstance(tree, false);
        addUnseenVarsToTransitions(i.getStmt(), i.getVarDecl());
    }
    
    private void addUnseenVarsToTransitions(Stmt[] stmts, VarDecl[] variables) {
        //Stmt[] parents = tree.getStmt().getDescendants();
        //VarDecl[] variables = getAllVarDecl();
        for(int i = stmts.length-1; i >= 0; i--) {
        //while(stmt != null) {
            addUnseenVars(stmts[i], variables);
            //stmt = stmt.getNext();
        //}
        }
    }
    
    private void addUnseenVars(Stmt s, VarDecl[] variables) {
        VarAss[] seenVariables = s.getEffect().getDescendants();        
        
        for (VarDecl variable : variables) {
            String var = variable.getVar().getValue();
            if (!variable.isNondeterministic() && !isSeenVariable(var, seenVariables) 
                    && variable.getTypeNode().getType() != Type.Proc) {
                s.addAssignmentFront(var, new Var(var));
            }
        }
    }
    
    private boolean isSeenVariable(String element, VarAss[] array) {
        for (VarAss array1 : array) {
            if (array1.getID().getValue().equals(element)) {
                return true;
            }
        }
        return false;
    }
}
