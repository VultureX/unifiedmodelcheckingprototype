/*
 * TransitionSystemInitializer.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.TransitionSystem;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarInit;

/**
 *
 * @author Peter Maandag
 */
public class InitializerTransformTS extends Initializer {
    private TransitionSystem t;
    public InitializerTransformTS(TransitionSystem t) {
        this.t = t;
    }

    @Override
    public VarDecl[] getVarDecl() {
        return t.getVarDecl().getDescendants();
    }

    @Override
    public VarInit[] getVarInit() {
        VarInit init = t.getVarInit();
        if(init != null) {
            VarInit[] inits = init.getDescendants();
            return inits;
        }
        return new VarInit[0];
    }

    @Override
    public Stmt[] getStmt() {
        return t.getStmt().getDescendants();
    }        
}
