/*
 * Initializer.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarInit;

/**
 *
 * @author Peter Maandag
 */
public abstract class Initializer {
    public abstract VarDecl[] getVarDecl();
    public abstract VarInit[] getVarInit();
    public abstract Stmt[] getStmt();
    //public ASTNode getGoal();
    
    public static Initializer getInstance(Formula tree, boolean compiling) {
        if(tree instanceof AST.TransitionSystem) {
            return new InitializerTransformTS((AST.TransitionSystem) tree);
        } else if(tree instanceof AST.ParallelSystem) {
            if(compiling) {
                return new InitializerTransformPS((AST.ParallelSystem) tree);
            } else {
                return new InitializerCompilePS((AST.ParallelSystem) tree);
            }
        } else {
            throw new UnsupportedOperationException("Unsupported formula in getInstance");
        }        
    }
}
