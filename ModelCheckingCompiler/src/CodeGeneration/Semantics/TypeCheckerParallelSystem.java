/*
 * TypeCheckerParallelSystem.java
 * Copyright(c) 2014
 */

package CodeGeneration.Semantics;

import CodeGeneration.AST;
import CodeGeneration.AST.ASTNode.Type;
import CodeGeneration.AST.*;
import CodeGeneration.Compiler.CompileError;
import java.util.ArrayList;
import static Tools.Utils.newLine;

/**
 *
 * @author Peter Maandag
 */
public class TypeCheckerParallelSystem extends TypeChecker {
    private ParallelSystem processModel;
    private TypeCheckerProcess[] processTypeCheckers;
    private ArrayList<ID> globalVars;
    
    public TypeCheckerParallelSystem(ParallelSystem p) {
        super(p);
        this.processModel = p;
        initProcessTypeCheckers();
    }
    
    private void initProcessTypeCheckers() {
        AST.Process[] processes = processModel.getProcess().getDescendants();
        VarDecl[] vardecls = processModel.getVarDecl().getDescendants();
        this.globalVars = new ArrayList<>();
        
        for(AST.Process p : processes) {
            globalVars.add(p.getName());
        }
        for(VarDecl d : vardecls) {
            globalVars.add(d.getVar());
        }
        
        this.processTypeCheckers = new TypeCheckerProcess[processes.length];
        for(int i = 0; i < processes.length; i++) {
            processTypeCheckers[i] = new TypeCheckerProcess(processModel, processes[i], globalVars);
        }       
    }
    
    public ArrayList<ID> getGlobalVars() {
        return globalVars;
    }
    
    @Override
    public void performChecks() throws CompileError {
        checkVarDecl();        
        checkVarInit();
        checkSystem();
        checkGoal();
        checkProcesses();
    }

    @Override
    protected Type findVarType(ID id) throws CompileError {
        if(id instanceof AccessorID) {
            ID rootVar = ((AccessorID)id).getRoot();
            VarDecl varDecl = findVar(rootVar);
            Value v = varDecl.getDeclaredValue();
            TypeCheckerProcess p = tryFindProcessTypeChecker((String)v.getValue());            
            return p.findVarType(id);
        }
     
        VarDecl dec = findVar(id);  
        return dec.getTypeNode().getType();
    }
    
    private void checkVarDecl() throws CompileError {
        VarDecl[] vars = processModel.getVarDecl().getDescendants();
        ArrayList<Var> seenVars = new ArrayList<>();
        ArrayList<String> processIDs = new ArrayList<>();
        getProgramProcesses(processModel.getProgram(), processIDs);

        for(int i = 0; i < vars.length; i++) {
            VarDecl var = vars[i];
            if(isRestrictedVariable(var.getToken().getType(), var.getVar().getValue())) {
                throw new CompileError("Cannot declare restricted variable " + var.getVar().getDescription());
            }
            if(seenVars.contains(var.getVar())) {
                throw new CompileError(var.getVar().getDescription() + " is declared twice");
            }
            if(getTypeOf(var.getTypeNode()) == Type.Proc) {
                if(!(var.getDeclaredValue().getValue() instanceof String)) {
                    throw new CompileError(var.getVar().getDescription() + " be initialized with a process identifier");
                } else {
                    String processID = (String) var.getDeclaredValue().getValue();
                    AST.Process p = tryFindProcess(processID);
                    if(p == null) {
                        throw new CompileError(var.getDeclaredValue().getDescription() + " is not a process");
                    }
                    if(!processIDs.contains(var.getVar().getValue())) {
                        throw new CompileError(var.getVar().getDescription() + " is not part of the program");
                    }
                }
            }
            if(var.isConstant()) {
                ASTNode constValue = var.getDeclaredValue();
                ASTNode typeNode = var.getTypeNode();
                Type assType = getTypeOf(constValue);
                Type varType = getTypeOf(typeNode);
                if(assType != varType) {
                    throw new CompileError("Type of " + var.getVar().getDescription()
                        + " does not match with type of " + constValue.getDescription()
                        + newLine + varType + " != " + assType);
                }
            }
            seenVars.add(var.getVar());
        }
    }
    
    private void checkVarInit() throws CompileError {
        ArrayList<String> seenVars = new ArrayList<>();
        ArrayList<String> seenAssignments = new ArrayList<>();
        if(processModel.getVarInit() != null) {
            VarInit[] inits = processModel.getVarInit().getDescendants();
            
            for(int i = 0; i < inits.length; i++) {
                VarInit init = inits[i];
                ASTNode expression = init.getExpression();
                Type type = getTypeOf(expression);
                init.setType(type);
                addUnseenVarsFromExpression(expression, seenVars);
                
                if(type == Type.Void) {
                    BinOpExp assignment = (BinOpExp) AST.filterPars(expression);
                    ID id = (Var)AST.filterPars(assignment.lhs()); //@Todo give typeerror for accessorIDs
                    VarDecl var = findVar(processModel, id);
                    
                    if(getTypeOf(id) == Type.Proc) {
                        throw new CompileError("Cannot initialize process or process field: " + init.getDescription());
                    }
                    if(seenAssignments.contains(id.getValue())) {
                        throw new CompileError("Variable " + id.getDescription() + " is assigned a value twice");
                    }
                    if(isRestrictedVariable(assignment.getToken().getType(), id.getValue())) {
                        throw new CompileError("Cannot assign value to restricted variable " + id.getDescription());
                    }                   
                    if(var.isConstant()) {
                        throw new CompileError("Constant variable " + id.getDescription() + " must only be initialized in VAR block");
                    }

                    seenAssignments.add(id.getValue());
                }
            }
        }
    }
    
    private void checkSystem() throws CompileError {
       Type type = getTypeOf(processModel.getSystem());
        if(type != Type.Proc) {
            throw new CompileError("Program expression is not a process algebraic expression");
        }
    }
    
    private void getProgramProcesses(ASTNode program, ArrayList<String> ids) throws CompileError {
        if(program instanceof Var) {
            Var processVar = ((Var) program);
            if(ids.contains(processVar.getValue())) {
                throw new CompileError(processVar.getDescription() + " is used more than once");
            }
            ids.add(processVar.getValue());
        } else if(program instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) program;
            getProgramProcesses(exp.lhs(), ids);
            getProgramProcesses(exp.rhs(), ids);
        } else {
            throw new UnsupportedOperationException("unknown class in checkProgram: "
                    + program.getClass());
        }
    }
    
    private void checkGoal() throws CompileError {
        if(processModel.getGoal() != null) {
            Type type = getTypeOf(processModel.getGoal());
            if(type != Type.Bool) {
                throw new CompileError("Goal expression is not a boolean expression");
            }
        }
    }
    
    private void checkProcesses() throws CompileError {
        for(TypeCheckerProcess p : this.processTypeCheckers) {
            p.performChecks();
        }
    }
    
    protected TypeCheckerProcess tryFindProcessTypeChecker(String id) throws CompileError {
        for(TypeCheckerProcess p : this.processTypeCheckers) {
            if(p.getProcess().getName().getValue().equals(id)) {
                return p;
            }
        }
        return null;
    }
    
    protected AST.Process tryFindProcess(String id) throws CompileError {
        for(TypeCheckerProcess p : this.processTypeCheckers) {
            if(p.getProcess().getName().getValue().equals(id)) {
                return p.getProcess();
            }
        }
        return null;
    }
    
    @Override
    public VarDecl tryFindVar(ID id) {
        VarDecl var = tryFindVar(processModel, id);
        if(var != null) {
            return var;
        }
        for(TypeCheckerProcess p : this.processTypeCheckers) {
            var = p.tryFindVar(id);
            if(var != null) {
                return var;
            }
        }

        return var;
    }
    
    @Override
    public VarDecl findVar(ID id) throws CompileError {
        VarDecl var = tryFindVar(id);
        if(var == null) {
            throw new CompileError("Variable " + id.getDescription() 
                    + " is used, but not declared");
        }
        return var;
    }

}
