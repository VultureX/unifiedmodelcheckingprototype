/*
 * SyntaxHighlighter.java
 * Copyright(c) 2014
 */

package CodeGeneration;

import GUI.MainWindow;
import Tools.Utils;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Peter Maandag
 */
public class SyntaxHighlighter extends Thread implements DocumentListener, Runnable
{
    private JTextPane pane;
    private StyledDocument doc;
    private StyledDocument highlightedDoc;
    private ArrayList<Integer> lineOffsets;
    private MutableAttributeSet aset = new SimpleAttributeSet();
    private Thread t;
    
    private volatile boolean changed = false;
    
    public SyntaxHighlighter(JTextPane pane) {
        this.pane = pane;
    }
    
    /**
     * 
     * @param doc
     * @param code
     * @return a a new document, but with highlighted syntax. The original doc
     * will not be changed
     */
    public StyledDocument getHighLighted(StyledDocument doc) {
        this.doc = doc;
        this.highlightedDoc = new DefaultStyledDocument(); //This has size 1
        
        try {
            String code = doc.getText(0, doc.getLength());
            setLineOffsets();
            Tokenizer t;
            t = new Tokenizer(code);
            t.tokenize();
            color(t);
            //Remove last character
            ((DefaultStyledDocument) highlightedDoc).removeElement(
                    this.highlightedDoc.getCharacterElement(this.highlightedDoc.getLength()));
        } catch (BadLocationException ex) {
            Logger.getLogger(SyntaxHighlighter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return highlightedDoc;
    }
    
    private void replaceLine(int start, String line, Color c) {
        aset.addAttribute(StyleConstants.Foreground, c);
        
        try {
            highlightedDoc.remove(start, line.length());
            highlightedDoc.insertString(start, line, aset);
        } catch (BadLocationException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void setLineOffsets() {
        lineOffsets = new ArrayList<>();
        Element e = doc.getDefaultRootElement();
        for(int i = 0; i < e.getElementCount(); i++) {
            //Each child element is a line:
            Element line = e.getElement(i);
            final int lineStart = line.getStartOffset();
            final int lineEnd = line.getEndOffset();
            lineOffsets.add(lineStart);
            try {
                String text = doc.getText(lineStart, lineEnd-lineStart);
                this.highlightedDoc.insertString(lineStart, text, line.getAttributes());
            } catch (BadLocationException ex) {
                Logger.getLogger(SyntaxHighlighter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void color(Tokenizer t) {
       Iterator<Token> it = t.getIterator();
       
       while(it.hasNext()) {
           Token tok = it.next();
           if(tok.getType() != Token.Type.EOF && tok.getLineNumber()-1 < lineOffsets.size()) {
               int lineOffset = lineOffsets.get(tok.getLineNumber()-1);
               replaceLine(lineOffset + tok.getColumn(), tok.value(), getColor(tok.getType()));
           }
       }   
    }
    
    private Color getColor(Token.Type t) {
        switch(t) {
            case Comment:
                return Color.GREEN.darker();
            case MAXSTEPS:
            case VARS:
            case GOAL:
            case LOOP:
            case INIT:
            case TRANS:
                return Color.RED;
            case Number:
                return Color.ORANGE;
            case CONST:
            case INT:
            case BOOL:
            case TRUE:
            case FALSE:
            case NONDET:
                return Color.BLUE;
            case ID:
                return Color.GRAY;
            default:
                return Color.BLACK;
        }
    }
    
    @Override
    public void insertUpdate(DocumentEvent e) {
        synchronized(this) {
            changed = true;            
            notify();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        synchronized(this) {
            changed = true;            
            notify();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }
    
    public void updateDoc() throws InterruptedException {
        synchronized(this) {
            while(!changed) {
                wait();
            }
            changed = false;
        }
        
        StyledDocument newDoc = getHighLighted(pane.getStyledDocument());
        Utils.sleep(100);
        if(!changed) {
            int oldPos = pane.getCaretPosition();
            newDoc.addDocumentListener(this);
            //@todo: Document can be changed here...
            pane.setDocument(newDoc);
            this.doc.removeDocumentListener(this);
            pane.setCaretPosition(oldPos);
        }
        
    }
    
    @Override
    public void run() {        
        while(true) {
            try {
                updateDoc();
            } catch (InterruptedException ex) {
                Logger.getLogger(SyntaxHighlighter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /*private void setLineOffsets() {
        //bad location exception on windows: http://stackoverflow.com/questions/
        //15719770/badlocationexception-when-using-utilities-getrowstart-on-hit-of-enter-key
        //int totalCharacters = textPane.getText().length();
        
        int totalCharacters = doc.getLength();
        try {
           int offset = totalCharacters; 
           while (offset > 0) {
              offset = Utilities.getRowStart(textPane, offset);
              System.out.println(offset);
              if(offset >= 0) {
                  lineOffsets.add(0, offset);
              }
              offset--;
           }
        } catch (BadLocationException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
}
