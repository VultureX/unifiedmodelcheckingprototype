/*
 * CompilerNuSMV.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import CodeGeneration.AST.ASTNode;
import CodeGeneration.AST.AndOp;
import CodeGeneration.AST.AssignmentOp;
import CodeGeneration.AST.BinOpExp;
import CodeGeneration.AST.Bool;
import CodeGeneration.AST.DivOp;
import CodeGeneration.AST.EqualsOp;
import CodeGeneration.AST.Formula;
import CodeGeneration.AST.GTEOp;
import CodeGeneration.AST.GTOp;
import CodeGeneration.AST.ID;
import CodeGeneration.AST.LTEOp;
import CodeGeneration.AST.LTOp;
import CodeGeneration.AST.MinOp;
import CodeGeneration.AST.Num;
import CodeGeneration.AST.OrOp;
import CodeGeneration.AST.ParenExp;
import CodeGeneration.AST.PlusOp;
import CodeGeneration.AST.Range;
import CodeGeneration.AST.Stmt;
import CodeGeneration.AST.TimesOp;
import CodeGeneration.AST.TypeNode;
import CodeGeneration.AST.UnOpExp;
import CodeGeneration.AST.Var;
import CodeGeneration.AST.VarAss;
import CodeGeneration.AST.VarDecl;
import CodeGeneration.AST.VarDeclRanged;
import CodeGeneration.AST.VarInit;
import CodeGeneration.Semantics.Initializer;
import CodeGeneration.Semantics.TransformPostSemantics;
import CodeGeneration.Semantics.TransformPostSemanticsPS;
import CodeGeneration.Semantics.TransformPreSemantics;
import CodeGeneration.Semantics.TypeChecker;
import static Tools.Utils.newLine;
import java.io.IOException;

/**
 *
 * @author Peter Maandag
 */
public class CompilerNuSMV extends Compiler {
    
    private final int VARLIMIT_MIN = 0;
    private final int VARLIMIT_MAX = 1023;
    private final boolean compileLoopCode;
    
    public CompilerNuSMV(Formula syntaxTree) throws CompileError {
        super(syntaxTree);
        
        Formula.Mode mode = syntaxTree.getMode();
        compileLoopCode = mode == Formula.Mode.Loop || 
                mode == Formula.Mode.PropertyLoop || mode == Formula.Mode.Deadlock;
    }
    
    @Override
    protected String[] getExtensions() {
        return new String[]{".NuSMV"};
    }
    
    private void assureRangedVariables(VarDecl[] decls) throws CompileError {
        for(VarDecl var : decls) {
            if(var.getTypeNode().getType() == TypeNode.Type.Int && !(var instanceof VarDeclRanged)
                    && !var.isConstant()) {
                throw new CompileError("All integer variables must be ranged: " + var.getDescription());
            }
        }
    }
    
    @Override
    protected void preCheckTransformTree() throws CompileError {
        //Check for unranged variables:
        assureRangedVariables(Initializer.getInstance(tree, false).getVarDecl());
        
        //Steps variable range can prevent transitions to be made, so the loop check will fail
        if(addSteps && compileLoopCode) {                        
            throw new CompileError("Steps variable cannot be used while checking for loops or deadlock in NuSMV");
        }
        
        TransformPreSemantics transform = new TransformPreSemantics(tree);        
        if(addSteps) {
            transform.addSystemStepsVariable();
        }
        if(tree.getMaxSteps() != null) {
            transform.addMaxStepsVariable();
        }        
        
        if(compileLoopCode) {
            transform.addLoopVariable();
            //transform.addLoopVariableTransitions();
            transform.addSystemLoopVariableTransitions();
        }       
    }
    
    private TransformPostSemantics getInstance(Formula tree) {
        if(tree instanceof AST.TransitionSystem) {
            return new TransformPostSemantics((AST.TransitionSystem) tree);
        } else if(tree instanceof AST.ParallelSystem) {
            return new TransformPostSemanticsPS((AST.ParallelSystem) tree);
        } else {
            throw new UnsupportedOperationException("Unsupported formula in getInstance");
        }
    }
    
    @Override
    protected void postCheckTransformTree() throws CompileError {
        //super.postCheckTransformTree();
        
        TransformPostSemantics transform = getInstance(tree);
        transform.transformTree();
        //if(tree.getMode() != Formula.Mode.Loop) {
            transform.addIdentityRuleNuSMV();
        //}
        //transform.addUnseenVarsToTransitions();
        transform.addSystemUnseenVars();
    }
    
    @Override
    protected void compile(String extension) throws IOException {       
       compile(Initializer.getInstance(tree, true));
    }
    
    private void compile(Initializer formula) throws IOException {
       writeln("MODULE main");
       compileVarDecl(formula.getVarDecl());
       compileVarInit(formula.getVarInit());
       //compileSystem();
       compileTransitions(formula.getStmt());
       compileGoalState();
    }
    
    private void compileGoalState() throws IOException {
       ASTNode goal = tree.getGoal();
        
       write(newLine + newLine + "LTLSPEC ");
       switch(tree.getMode()) {
           case Reachability:
               write("G !(");
               compileExpression(goal);
               writeln(")");
               break;
           case Loop:
               write("F (loop = FALSE)");
               break;
           case PropertyLoop:
               write("(G (loop = TRUE)) -> !G(F( ");
               compileExpression(goal);
               writeln(" ))");
               break;
           case Safety:
               write("G (");
               compileExpression(goal);
               writeln(")");
               break;
           case Deadlock:
               write("G (loop = TRUE)");
               break;
           case Liveness:
               write("G (F(");
               compileExpression(goal);
               writeln("))");
               break;
           default:
               print("Unsupported mode in compileGoalState", ERROR);
       }
    }
    
    private void compileVarInit(VarInit[] inits) throws IOException {
        if(inits.length != 0) {
            writeln("INIT");
            for(int i = inits.length-1; i >= 0; i--) {
                write("(");
                compileExpression(inits[i].getExpression());
                write(")");
                if(i > 0) {
                    write(" & ");
                }
            }
            write(newLine);
        }
    }
    
    private void compileVarDecl(VarDecl[] variables) throws IOException {
        writeln("VAR");
        
        for(int i = variables.length-1; i >= 0; i--) {            
            //ASTNode varMin = VARLIMIT_MIN;
            //ASTNode varMax = VARLIMIT_MAX;
            Range range = null;
            
            String id = variables[i].getVar().getValue();
            TypeNode.Type type = variables[i].getTypeNode().getType();
            if(variables[i] instanceof VarDeclRanged) {
                range = ((VarDeclRanged)variables[i]).getRange();
                /*if(range != null) {
                    varMin = range.getMin();
                    varMax = range.getMax();
                }*/
            } else {
                if(type == TypeNode.Type.Int) {
                    print("Range not defined for variable '" 
                            + id + "'. Reverting to default value "
                            + VARLIMIT_MAX, WARNING);
                }                
            }            
            
            if(type == TypeNode.Type.Bool) {
                writeln(id + " : boolean;");
            } else {
                write(id + " : ");
                if(range != null) {
                    compileExpression(range.getMin());
                    write("..");
                    compileExpression(range.getMax());
                    writeln(";");
                } else {
                    writeln(VARLIMIT_MIN + ".." + VARLIMIT_MAX + ";");
                }
            }
        }  
    }
    
    /*private void compileSystem() throws IOException {
        if(tree instanceof TransitionSystem) {
           Stmt[] stmts = ((TransitionSystem)tree).getStmt().getDescendants(); 
           compileTransitions(stmts);
       } else {
           throw new UnsupportedOperationException("Unrecognized Formula in compile()");
       }
    }*/
    
    private void compileTransitions(Stmt[] stmts) throws IOException {
        writeln("TRANS");
        
        //compileIdentityRule();        
        
        for(int i = stmts.length-1; i >= 0; i--) {            
            compileTransition(stmts[i]);           
            if(i > 0) {
                writeln(" | ");
            }
        }
    }
    
//    private void compileIdentityRule() throws IOException {
//        if(!checkLoops) {
//            VarDecl[] vars = tree.getVarDecl().getDescendants();
//
//            write("(");
//            for(int i = vars.length-1; i >= 0; i--) {
//                VarDecl t = vars[i];
//                addVar(t.getVar(), t.getVar());
//                if(i > 0) {
//                    write(" & ");
//                }
//            }
//            writeln(") |");
//        }
//        
//        /*write("case FALSE : ");
//        VarDecl[] vars = tree.getVarDecl().getDescendants();
//        Action0E<IOException> func = () -> {
//            for(VarDecl var : vars) {
//                addVar(var.getVar(), var.getVar());
//            }
//        };        
//        func.invoke();
//        writeln(";");
//        write("TRUE : ");
//        func.invoke();        
//        writeln("; esac");*/
//    }
    
    //Translate without case statements
    private void compileTransition(Stmt s) throws IOException {        
        write("(");
        if(s.getGuard() != null) {
            compileExpression(s.getGuard());
            write(" & ");            
        } 
        compileVarAss(s.getEffect());
        write(")");
    }
    
    //Translate with case statements
    /*private void compileTransition(Stmt s) throws IOException {
        if(s.getGuard() == null) {
            write("(");
            compileVarAss(s.getEffect());
            //addUnseenVars(s);
            write(")");
        } else {
            write("case ");
            compileExpression(s.getGuard());           
            write(" : ");
            compileVarAss(s.getEffect());
            //addUnseenVars(s);
            write("; " + newLine + "TRUE : ");
            addAllVars();
            write("; esac");
        }        
    }*/
    
    private void addAllVars() throws IOException {                
        VarDecl[] variables = tree.getVarDecl().getDescendants();
        for(int i = 0; i < variables.length; i++) {
            if(!variables[i].isNondeterministic()) {
                String var = variables[i].getVar().getValue();
                Var id = variables[i].getVar();
                
                if(var.equals(TypeChecker.loopVariable)) {
                    addVar(id, new Bool(Token.getInstance(Boolean.toString(false)), false));
                } else {
                    addVar(id, id);
                }

                if(i < variables.length-1 && !variables[i].isNondeterministic()) {
                    write(" & ");
                }
            }
        }
    }
    
    private void compileVarAss(VarAss ass) throws IOException {
        VarAss[] parents = ass.getDescendants();
      
        for(int i = parents.length-1; i >= 0; i--) {
            VarAss t = parents[i];
            addVar(t.getID(), t.getAss());
            if(i > 0) {
                write(" & ");
            }
        }
    }
    
    private void addVar(ID id, ASTNode expression) throws IOException {
        write("next(");
        compileExpression(id);
        write(") = ");
        compileExpression(expression);
    }
    
    private void compileExpression(ASTNode t) throws IOException {            
        if(t instanceof PlusOp) {
            write("+");
        } else if(t instanceof MinOp) {
            write("-");
        } else if(t instanceof DivOp) {
            write("/");
        } else if(t instanceof TimesOp) {
            write("*");
        } else if(t instanceof Var) {
            Var id = (Var) t;
            write(id.getValue());
        } else if(t instanceof Num) {
            Num num = (Num) t;
            write(Integer.toString(num.getValue()));
        } else if(t instanceof Bool) {
            boolean b = ((Bool) t).getValue();
            String bool = b ? "TRUE" : "FALSE";
            write(bool);
        } else if(t instanceof LTEOp) {
            write("<=");
        } else if(t instanceof GTEOp) {
            write(">=");
        } else if(t instanceof LTOp) {
            write("<");
        } else if(t instanceof GTOp) {
            write(">");
        } else if(t instanceof EqualsOp) {
            write("=");
        } else if(t instanceof AndOp) {
            write("&");
        } else if(t instanceof OrOp) {
            write("|");
        } else if(t instanceof AssignmentOp) {
            write("=");
        } else if (t instanceof UnOpExp) {
            write("!");
            compileExpression(((UnOpExp)t).getExp());
        } else if (t instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) t;
            compileExpression(exp.lhs());
            write(" ");
            compileExpression(exp.getOp());
            write(" ");
            compileExpression(exp.rhs());
        } else if (t instanceof ParenExp) {
            write("(");
            ParenExp par = (ParenExp) t;
            compileExpression(par.getExp());
            write(")");
        } else {
            print("Unrecognized operator in compileExpression: " + t.toString(), ERROR);
        }
    }
}
