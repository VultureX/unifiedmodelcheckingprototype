/*
 * Compiler.java
 * Copyright(c) 2014
 */
package CodeGeneration;
import CodeGeneration.AST.Formula;
import CodeGeneration.Semantics.TypeChecker;
import GUI.MainWindow;
import Tools.DirectoryInfo;
import Tools.FileInfo;
import Tools.FileWriterUtility;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public abstract class Compiler {
    protected Formula tree;
    //protected boolean checkLoops;
    protected boolean addSteps;
    
    protected DirectoryInfo compileDir;
    //protected FileInfo[] compiledFiles;
    //protected FileInfo currentFile;
    //protected String compileDir;
    private String[] compiledFileNames;
    protected String currentCompiledFileName;
    private FileWriterUtility fw;
    public static final int INFO = 0;
    public static final int SUCCESS = 1;
    public static final int WARNING = 2;
    public static final int ERROR = 3;
    
    public Compiler(Formula syntaxTree) throws CompileError {
        this.tree = syntaxTree;
        Formula.Mode mode = syntaxTree.getMode();
        //this.checkLoops = mode == Formula.Mode.Loop || mode == Formula.Mode.PropertyLoop; 
        this.addSteps = syntaxTree.getMaxSteps() != null;
    }
    
    private void performPreProcessing() throws CompileError {        
        //Transform tree before semantics checks:
        preCheckTransformTree();        
        
        //Perform semantic checks:
        checkSemantics();
        
        //Transform tree after semantics checks:
        postCheckTransformTree();
    }
    
    protected abstract void preCheckTransformTree() throws CompileError;
    
    protected void checkSemantics() throws CompileError {
        TypeChecker typeChecker = TypeChecker.getInstance(tree);
        typeChecker.performChecks();
    }
    
    protected abstract void postCheckTransformTree() throws CompileError;
    
    public void compile(DirectoryInfo outputDir, String name) throws CompileError {
        performPreProcessing();
        //System.out.println(tree.flattenToken());
        //return;
        
        String[] files = getExtensions();
        this.compiledFileNames = new String[files.length];
        this.compileDir = outputDir;
        
        for(int i = 0; i < files.length; i++) {
            //Find the start of the extension
            int extensionIndex = name.indexOf(".");
            if(extensionIndex <= 0) {
                extensionIndex = name.length();
            }
            
            //Add/replace the extension:
            this.compiledFileNames[i] = name.substring(0, extensionIndex);
            String ext = files[i];
            this.compiledFileNames[i] += ext;            
            
            //Compile the file
            this.currentCompiledFileName = this.compiledFileNames[i];
            compileFile(ext, new FileInfo(compileDir, this.compiledFileNames[i]));
        }
    }
    
    private void compileFile(String extension, FileInfo compileFile) throws CompileError {
        try {
            //fw = new FileWriterUtility(compileDir, compileName);
            //print("Compiling to " + compileDir + compileName + "...", INFO);
            print("Compiling to " + compileFile + "...", INFO);
            fw = new FileWriterUtility(compileFile);            
            compile(extension);
            print("Done!", SUCCESS);
        } catch (IOException ex) {
            Logger.getLogger(Compiler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fw.close();
        }
    }
    
    //@Todo support multiple compileDirs:
    /*public String[] getCompileDirs() {
        String[] dirs = new String[compileFilenames.length];
        for(int i= 0; i < dirs.length; i++) {
            dirs[i] = compileDir;
        }
        return dirs;
    }*/
    
    /*public String[] getCompiledFileNames() {
        return compiledFileNames;
    }*/
    
    public FileInfo[] getCompiledFilesInfo() {
        FileInfo[] files = new FileInfo[compiledFileNames.length];
        for(int i = 0; i < files.length; i++) {
            files[i] = new FileInfo(compileDir, compiledFileNames[i]);
        }
        return files;
    }
    
    public Formula.Mode getMode() {
        return tree.getMode();
    }
    
    protected abstract String[] getExtensions();
    protected abstract void compile(String extension) throws IOException;
    
    protected void write(String s) throws IOException {
        fw.write(s);
    }
    
    protected void writeln(String s) throws IOException {
        fw.write(s + newLine);
    }
    
    protected void print(String s, int code) {
        Color c = Color.RED;
        switch(code) {
            case SUCCESS:
                c = Color.GREEN.darker(); break;
            case WARNING:
                c = Color.ORANGE; break;
            case ERROR:
                c = Color.RED; break;
            case INFO:
                c = Color.BLACK; break;
        }
        MainWindow.getConsole().println(s, c);
    }
    
    public static class CompileError extends Exception {
        public CompileError(String msg) {
            super("Compiler error: " + msg);
            MainWindow.getConsole().println("Compiler error: " + msg, Color.red);
        }
    }
}
