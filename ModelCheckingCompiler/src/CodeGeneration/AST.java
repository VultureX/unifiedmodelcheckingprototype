/*
 * AST.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import Tools.Utils;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

/**
 * All classes in this file are static so they can easily be accessed from outside,
 * without the need for an own file. Forgetting the static keyword in any class
 * can lead to weird compile errors
 * @author Peter Maandag
 */
public class AST {
    /**
     * This is the basic node of the Abstract Syntax Tree.
     * It has a parent node, can have multiple child nodes, which hold values 
     * and each node corresponds to a token that was parsed in the original source file
     * Furthermore an ASTNode can also be used as a double linked list of trees where
     * the descendant points to the next element and the parent points to the previous
     * If you would like to use this list functionality in YourNode then you should
     * extend YourNode with ASTNode\<YourNode\>, so you can call getNext or getDescendants
     * and have the correct type available immediately.
     * @param <T> Should be the type of the subclass
     */
    public static abstract class ASTNode<T extends ASTNode> {
        protected Token token;
        protected ASTNode parent;
        protected ArrayList<ASTNode> children;
        protected T next;
        protected Type type;
        
        public enum Type {
            Undefined, Int, Bool, Proc, Void
        }
        
        public ASTNode(Token token, T next, ASTNode... children) {
            this.type = Type.Undefined;
            this.token = token;
            this.next = next;
            this.children = new ArrayList<>(Arrays.asList(children));
            initNodes();
        }
        
        private void initNodes() {
            children.removeAll(Collections.singleton(null));
            for(ASTNode node : children) {
                node.parent = this;
            }
            
            if(this.next != null) {
                next.parent = this;
            }
        }
        
        //Token functions
        public Token getToken() {
            return token;
        }
        public int getLineNumber() {
            return token.getLineNumber();
        }
        public String getLineNumberString() {
            return "@line " + token.getLineNumber();
        }
        public String getDescription() {
            return "'" + this.getSourceCodeDescription() + "' " + this.getLineNumberString();
        }
        //Description of original code
        public String getSourceCodeDescription() {
            StringBuilder s = new StringBuilder();            
            if(children.isEmpty()) {
                s.append(token.value()); 
                return s.toString();
            }

            for(int i = 0; i < children.size(); i++) {
                ASTNode child = children.get(i);
                s.append(child.getSourceCodeDescription());
                if(i < children.size()-1) {
                     s.append(" ");
                }
                                
                //@Todo: Reverse order:
                ASTNode descendant = child.getNext();
                while(descendant != null) {
                    s.append(descendant.getSourceCodeDescription());
                    descendant = descendant.getNext();                    
                }                
            }
            
            return s.toString();
        }
        public String flattenToken() {
            return flattenToken(0);
        }
        //Description of syntax tree
        private String flattenToken(int indent) {            
            StringBuilder s = new StringBuilder();
            indent(s, indent);
            
            if(children.isEmpty()) {
                s.append(token.value()); 
                if(indent > 0) {
                    s.append("\n");
                }
                return s.toString();
            } else {
                s.append(children.get(0).parent.getClass()).append("\n");
            }
            
            for(ASTNode child : children) {                
                s.append(child.flattenToken(indent+1));               
                                
                ASTNode descendant = child.getNext();
                while(descendant != null) {
                    s.append(descendant.flattenToken(indent+1));
                    descendant = descendant.getNext();
                }
            }
            
            return s.toString();
        }
        private void indent(StringBuilder b, int indent) {
            for(int i = 0; i<indent; i++) {
                b.append("\t");
            }
        }
        
        //Getters and setters
        public Type getType() {
            return type;
        }
        public void setType(Type type) {
            this.type = type;
        }
        public final ASTNode[] getChildren() {
            return children.toArray(new ASTNode[0]);
        }
        public final ASTNode getParent() {
            return parent;
        }
        public T getNext() {
            return next;
        }
        
        //Utilities
        /*public ASTNode[] flatten() {
            if(children.isEmpty()) {
                return new ASTNode[] {this};
            }
            
            ASTNode[] flattened = new ASTNode[0];
            if(!children.isEmpty()) {
                flattened = Tools.concatenate(flattened, children.get(0).parent);
            }

            for(ASTNode child : children) {
                flattened = Tools.concatenate(flattened, child.flatten());
                ASTNode descendant = child.getNext();
                while(descendant != null) {
                    flattened = Tools.concatenate(flattened, descendant.flatten());
                    descendant = descendant.getNext();
                }
            }
            
            return flattened;
        }*/
        
        //@Todo: rename to tailFromHere()
        public T[] getDescendants() {
            //Array must be of the most general type at all times. Derived classes will
            //give arraystore exceptions, because you cannot store a more general
            //type into an array of an extended type
            //System.err.println(this.getClass() + " " + this.getDescription());
            Class arrayType = Utils.getSuperClassThatExtends(ASTNode.class, this.getClass());
            T[] sum = (T[]) Array.newInstance(arrayType, 1);
            sum[0] = (T) this;
            if(next == null) {
                return sum;
            }
            return Utils.concatenate(sum, (T[]) next.getDescendants());
        }
        
        //Transformation functions:
        protected void addChild(ASTNode node) {
            if(node != null) {
                children.add(node);
                node.parent = this;
            }
        }
        
        protected void replaceChild(ASTNode child, ASTNode replacement) {
            if(child == null) {
                addChild(replacement);
                return;
            }
            
            for(int i = 0; i<children.size(); i++) {
                ASTNode node = children.get(i);
                if(node.equals(child)) {
                    children.set(i, replacement);
                    if(replacement != null) {
                        replacement.parent = this;
                    }
                }
            }
            children.removeAll(Collections.singleton(null));
        }
        
        //Replace should always call replaceChild and then replace your local
        //copies of children, currently it does not replace parents or descendants
        public void replace(ASTNode node, ASTNode replacement) {
            System.out.println(this.getClass());
            throw new UnsupportedOperationException("Not supported yet.");
        } 
        
        //Currently does not update the children! Only parents and descendants are updated
        //However this should not be needed since the children should not be able to be
        //reached anymore (unless this tree was a graph)
        public void remove() {
            if(parent == null){
                throw new UnsupportedOperationException("Can't remove the only node from the tree");                
            } 
            if(next != null) {
                next.parent = this.parent;
                Class parentClass = Utils.getSuperClassThatExtends(ASTNode.class, parent.getClass());
                Class nextClass = Utils.getSuperClassThatExtends(ASTNode.class, next.getClass());
                if(nextClass.equals(parentClass)) { //fixes a nasty edge case when the parent is not part of the list
                    parent.next = next;     
                }
            } else {
                parent.next = null;
            }
            
            
            if(parent.children.contains(this)) {
                parent.replace(this, next); //this replaces children and should update the local variables!
            }
        }
        
        public ASTNode<T> copy() {
            throw new UnsupportedOperationException("Copy not supported for " + this.getClass());
        }
        
        //@Todo: fix all equals!!
        /*@Override
        public boolean equals(Object o) {
            System.out.println("Missing equals implementation: " + this.getClass());
            return super.equals(o);
        }*/
    }
    
    public static abstract class Formula<S extends ASTNode, T extends ASTNode> extends ASTNode<T> {
        private Num maxSteps;
        private VarDecl decl;
        private VarInit init;
        //private Stmt stmt;
        protected S system; //Stmt or Process
        private ASTNode goal;
        private Mode mode;
        
        public enum Mode {
            None, Reachability, Loop, PropertyLoop, Deadlock, Safety, Liveness
        }
        
        public Formula(Token token, T next, Num maxSteps, VarDecl decl, VarInit init, 
                S system, ASTNode goal, Mode property, ASTNode[] orderedParams) {
            super(token, next, orderedParams); //maxSteps, decl, init, system, /*stmt,*/ goal);
            this.decl = decl;
            this.init = init;
            this.system = system;
            this.goal = goal;
            this.maxSteps = maxSteps;
            this.mode = property;
            
            /*if(!checkLoops) {
                this.mode = Mode.Reachability;
            } else {
                if(goal == null) {
                    this.mode = Mode.Loop;
                } else {
                    this.mode = Mode.PropertyLoop;
                }
            }*/
        }
        
        public void addConstVarDecl(String id, int value) {
            Var theID = new Var(id);
            Num theValue = new Num(value);
            
            VarDecl oldDecl = decl;
            VarDecl newDecl = new VarDecl(Token.getInstance(), decl, new Keyword(Token.Type.CONST.toString()), null, 
                    new TypeNode(TypeNode.Type.Int), theID, theValue);
            replace(oldDecl, newDecl);
        }
        
        public void addVarDeclAndInitFront(String id, boolean value) {
            Var theID = new Var(id);
            VarDecl oldDecl = decl;
            VarDecl newDecl = new VarDecl(Token.getInstance(), decl, null, null, 
                    new TypeNode(TypeNode.Type.Bool), theID, null);
            replace(oldDecl, newDecl);
            
            Bool theValue = new Bool(value);
            BinOpExp ass = new BinOpExp(Token.getInstance(),theID, new AssignmentOp(), theValue);
            VarInit oldInit = init;
            init = new VarInit(Token.getInstance(), init, ass);
            replaceChild(oldInit, init);
        }
        
        public void addVarDeclAndInitFront(String id, int value, Range r) {
            Var theID = new Var(id);
            VarDecl oldDecl = decl;
            VarDecl newDecl = new VarDeclRanged(Token.getInstance(), decl, null, null, 
                    new TypeNode(TypeNode.Type.Int), theID, null, r);
            replace(oldDecl, newDecl);
            
            Num theValue = new Num(value);
            BinOpExp ass = new BinOpExp(Token.getInstance(),theID, new AssignmentOp(), theValue);
            VarInit oldInit = init;
            init = new VarInit(Token.getInstance(), init, ass);
            replaceChild(oldInit, init);
        }
        
        public abstract void addStmt(Stmt newStmt);
        
        public Mode getMode() {
            return mode;
        }
        
        public ASTNode getGoal() {
            return goal;
        }
        
        public Num getMaxSteps() {
            return maxSteps;
        }
        
        public VarDecl getVarDecl() {
            return decl;
        }
        
        public VarInit getVarInit() {
            return init;
        }
        
        public S getSystem() {
            return system;
        }
        
        /*public Stmt getStmt() {
            return stmt;
        }*/
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(maxSteps)) {
               maxSteps = (Num) replacement;
            } else if(node.equals(decl)) {
                decl = (VarDecl) replacement;
            } else if(node.equals(init)) {
                init = (VarInit) replacement;
            } else if(node.equals(system)) {
                system = (S) replacement;
            } else if(node.equals(goal)) {
                goal = (BinOpExpBool) replacement;
            } else {
                super.replace(node, replacement);
            }
        }
    }
    
    public static class TransitionSystem<T extends ASTNode> extends Formula<Stmt, T> {
         public TransitionSystem(Token token, Num maxSteps, VarDecl decl, 
                 VarInit init, Stmt stmt, ASTNode goal, Mode property) {
             super(token, null, maxSteps, decl, init, stmt, goal, property,
                     new ASTNode[] { maxSteps, decl, init, stmt, goal });
         }
         
         protected TransitionSystem(Token token, T next, Num maxSteps, VarDecl decl, 
                 VarInit init, Stmt stmt, ASTNode goal, Mode property, 
                 ASTNode[] orderedParams) {
             super(token, next, maxSteps, decl, init, stmt, goal, property,
                     orderedParams);
         }
         
        public Stmt getStmt() {
            return (Stmt) getSystem();
        }

        @Override
        public void addStmt(Stmt newStmt) {
            Stmt s = (Stmt) system;
            Stmt oldStmt = s;
            Exp guard = newStmt.getGuard();
            if(guard != null) {
                guard = (Exp) newStmt.getGuard().copy();
            }
            system = new Stmt(Token.getInstance(), oldStmt, 
                    guard, (VarAss) newStmt.getEffect().copy());
            replaceChild(oldStmt, system);
        }
    }
    
    public static class ParallelSystem<T extends ASTNode> extends Formula<ASTNode, T> {
        private Process process;
        
        public ParallelSystem(Token token, Num maxSteps, VarDecl decl, 
                VarInit init, ASTNode stmt, Process processes,
                ASTNode goal, Mode property) {
             super(token, null, maxSteps, decl, init, stmt, goal, property,
                     new ASTNode[] { maxSteps, decl, init, stmt, goal, 
                     processes });
             this.process = processes;
        }
         
        public ASTNode getProgram() {
            return getSystem();
        }
        
        public Process getProcess() {
            return process;
        }
        
        public void replaceProcesses(ArrayList<Process> list) {
            for(int i = 0; i < list.size(); i++) {
                Process p = list.get(i);
                if(i < list.size() -1) {                    
                    p.next = list.get(i+1);
                    p.next.parent = list.get(i);
                }
            }
            this.replace(process, list.get(0));
        }
        
        @Override
        public void addStmt(Stmt newStmt) {
            ((Process) process).addStmt(newStmt);
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(process)) {
               process = (Process) replacement;
            } else {
                super.replace(node, replacement);
            }
        }
    }
    
    public static class Process<S extends ASTNode> extends Formula<S,Process> {
        private Var name;
        
        public Process(Token token, Process next, Var name, VarDecl decl, 
                VarInit init, S stmt) {
             super(token, next, null, decl, init, stmt, null, Mode.None,
                     new ASTNode[] { name, decl, init, stmt});
             this.name = name;
        }
        
        public Var getName() {
            return name;
        }
        
        @Override
        public void addStmt(Stmt newStmt) {            
            Stmt s = (Stmt) system;
            Stmt oldStmt = s;
            Exp guard = newStmt.getGuard();
            if(guard != null) {
                guard = (Exp) newStmt.getGuard().copy();
            }
            system = (S) new Stmt(Token.getInstance(), oldStmt, 
                    guard, (VarAss) newStmt.getEffect().copy());
            replaceChild(oldStmt, system);
        }
        
        //Fixes type errors
        @Override
        public Process[] getDescendants() {
            Formula[] formulas = super.getDescendants();
            Process[] processes = new Process[formulas.length];
            for(int i = 0; i<formulas.length; i++) {
                processes[i] = (Process) formulas[i];
            }
            return processes;
        }
        
        @Override
        public Process getNext() {
            return next;
        }
        
        //@Override
        public ASTNode copy(Process parent, Var newName) {
            VarInit initCopy = (this.getVarInit() == null) ? null : (VarInit) this.getVarInit().copy();
            Process p = new Process(token.copy(), parent, newName,
                    (VarDecl) super.getVarDecl().copy(), initCopy,
                    (S) super.getSystem().copy());
            return p;
        }
    }
    
    public static class VarDecl extends ASTNode<VarDecl> {
        protected Keyword constantKeyword;
        protected Keyword nondetKeyword;
        private TypeNode typeNode;
        private Var id;
        private Value constantValue;
        
        public VarDecl(Token token, VarDecl next, Keyword constant, Keyword nondet,
                TypeNode typeNode, Var id, Value declaredValue) {
            super(token, next, constant, nondet, typeNode, id, declaredValue);
            this.constantKeyword = constant;
            this.nondetKeyword = nondet;
            this.typeNode = typeNode;
            this.id = id;
            this.constantValue = declaredValue;
        }
        
        public Var getVar() {
            return id;
        }
        
        public TypeNode getTypeNode() {
            return typeNode;
        }
        
        public Value getDeclaredValue() {
            return constantValue;
        }
        
        public boolean isConstant() {
            return constantKeyword != null;
        }
        
        public boolean isNondeterministic() {
            return nondetKeyword != null;
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(constantKeyword)) {
               constantKeyword = (Keyword) replacement;
            } else if(node.equals(nondetKeyword)) {
                nondetKeyword = (Keyword) replacement;
            } else if(node.equals(typeNode)) {
                typeNode = (TypeNode) replacement;
            } else if(node.equals(id)) {
                id = (Var) replacement;
            } else if(node.equals(constantValue)) {
                constantValue = (Value) replacement;
            } else {
                super.replace(node, replacement);
            }
        }
        
        @Override
        public ASTNode copy() {
            VarDecl nextDecl = (next == null) ? null : (VarDecl) next.copy();
            Value copyValue = (this.getDeclaredValue() == null) ? null : (Value) this.getDeclaredValue().copy();
            Keyword copyConstant = (constantKeyword == null) ? null : (Keyword) constantKeyword.copy();
            Keyword copyNondet = (nondetKeyword == null) ? null : (Keyword) nondetKeyword.copy();
            return new VarDecl(token.copy(), nextDecl, copyConstant,
                    copyNondet, (TypeNode) this.getTypeNode().copy(), 
                    (Var) this.getVar().copy(), copyValue);
        }
        
        @Override
        public boolean equals(Object other) {
            if(other == null || !(other instanceof VarDecl)) {
                return false;
            }
            
            VarDecl o = (VarDecl) other;
            return o.id.equals(id);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 47 * hash + Objects.hashCode(this.id);
            return hash;
        }
    }
    
    public static class VarDeclRanged extends VarDecl {
        private Range range;
        
        public VarDeclRanged(Token token, VarDecl next, Keyword constant, Keyword nondet,
                TypeNode type, Var id, Value constantValue, Range range) {                
            super(token, next, constant, nondet, type, id, constantValue);
            this.addChild(range);
            
            this.range = range;
        }
        
        public Range getRange() {
            return range;
        }
        
        @Override
        public ASTNode copy() {
            VarDecl nextDecl = (next == null) ? null : (VarDecl) next.copy();
            Value copyValue = (this.getDeclaredValue() == null) ? null : (Value) this.getDeclaredValue().copy();
            Keyword copyConstant = (constantKeyword == null) ? null : (Keyword) constantKeyword.copy();
            Keyword copyNondet = (nondetKeyword == null) ? null : (Keyword) nondetKeyword.copy();
            return new VarDeclRanged(token.copy(), nextDecl, copyConstant,
                    copyNondet, (TypeNode) this.getTypeNode().copy(), 
                    (Var) this.getVar().copy(), copyValue, (Range) this.getRange().copy());
        }
    }
    
    public static class VarInit extends ASTNode<VarInit> {
        private BinOpExp exp; //Does not neccesarily need to be an assignment
        
        public VarInit(Token token, VarInit next, BinOpExp exp) {
            super(token, next, exp);

            this.exp = exp;
        }
        
        public BinOpExp getExpression() {
            return exp;
        }
        
        @Override
        public ASTNode copy() {
            VarInit nextCopy = (next == null) ? null : (VarInit) next.copy();
            return new VarInit(token.copy(), nextCopy, (BinOpExp) exp.copy());
        }
    }
    
    public static class VarAss extends ASTNode<VarAss> {
        private ID id;
        private ASTNode exp;
        
        public VarAss(Token token, VarAss next, ID id, ASTNode exp) {
            super(token, next, id, exp);

            this.id = id;
            this.exp = exp;
        }
        
        public ID getID() {
            return id;
        }
        
        public ASTNode getAss() {
            return exp;
        }
        
        @Override
        public ASTNode copy() {
            VarAss nextCopy = (next == null) ? null : (VarAss) next.copy();
            return new VarAss(token.copy(), nextCopy, (ID) id.copy(), exp.copy());
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(id)) {
                id = (ID) replacement;
            } else if(node.equals(exp)) {
                exp = replacement;
            } //else {
                //id.replace(node, replacement);
                exp.replace(node, replacement);
                if(next != null) {
                    next.replace(node, replacement);
                }
                //super.replace(node, replacement);
            //}
        }
    }
    
    public static class Stmt extends ASTNode<Stmt> {
        private /*BinOpExpBool*/ Exp guard;
        private VarAss ass;
        
        public Stmt(Token token, Stmt next, Exp guard, VarAss ass) {
            super(token, next, guard, ass); 
            
            this.guard = guard;
            this.ass = ass;
        }
        
        public void addAssignmentFront(String var, ASTNode expr) {
            VarAss oldchild = ass;
            ass = new VarAss(Token.getInstance(), ass, new Var(var), expr);
            replaceChild(oldchild, ass);
        }
        
        public void setGuard(Exp newGuard) {
            Exp oldGuard = guard;
            guard = newGuard;
            replaceChild(oldGuard, newGuard);
        }
        
        public Exp getGuard() {
            return guard;
        }
        
        public VarAss getEffect() {
            return ass;
        }
        
        @Override
        public ASTNode copy() {
            Stmt nextCopy = (next == null) ? null : (Stmt) next.copy();
            BinOpExpBool guardCopy = (guard == null) ? null : (BinOpExpBool) guard.copy();
            return new Stmt(token.copy(), nextCopy, guardCopy, 
                    (VarAss) ass.copy());
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(guard)) {
                guard = (BinOpExpBool) replacement;
            } else if(node.equals(ass)) {
                ass = (VarAss) replacement;
            } else {
                if(guard != null) {
                    guard.replace(node, replacement);
                }
                ass.replace(node, replacement);
            }
        }
    }
    
    public static abstract class Exp extends ASTNode {
        protected ASTNode exp;
        
        public Exp(Token token, ASTNode exp, ASTNode... extra) {
            super(token, null, Utils.concatenate(exp, extra));

            this.exp = exp;
            this.type = exp.type;
        }
        
        public ASTNode getExp() {
            return exp;
        }       
    }
    
    public static class ParenExp extends Exp {        
        public ParenExp(Token token, ASTNode exp) {
            super(token, exp);
        }
        
        public ASTNode removeParentheses() {
            ASTNode clean = exp;
            while(clean instanceof ParenExp) {
                ParenExp e = (ParenExp) clean;
                clean = e.getExp();
            }
            return clean;
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(exp)) {
                exp = replacement;
            } else {
                exp.replace(node, replacement);
            }
        }
        
        @Override
        public ASTNode copy() {
            return new ParenExp(token.copy(), exp.copy());
        }
    }
    
    public static class UnOpExp<T extends UnOp> extends Exp {
        protected T op;
        
        public UnOpExp(Token token, T op, ASTNode exp) {
            super(token, exp, op);

            this.op = op;
        }
        
        public T getOp() {
            return op;
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(exp)) {
                exp = replacement;
            } else if(node.equals(op)) {
                op = (T) replacement;
            } else {
                exp.replace(node, replacement);
                //super.replace(node, replacement);
            }
        }        
    }
    
    public static class UnOpExpBool extends UnOpExp<UnOpBool> {
        public UnOpExpBool(Token token, UnOpBool op, ASTNode exp) {
            super(token, op, exp);
            this.type = Type.Bool;
        }
        @Override
        public ASTNode copy() {
            return new UnOpExpBool(token.copy(), (UnOpBool) op.copy(), exp.copy());
        }
    }
    
    public static class BinOpExp<T extends BinOp> extends Exp {
        protected ASTNode /*left,*/ right;
        protected T op;
        
        public BinOpExp(Token token, ASTNode left, T op, ASTNode right) {
            super(token, left, op, right);

            //this.left = left;
            this.right = right;
            this.op = op;
        }
        
        public ASTNode lhs() {
            return exp; //left;
        }
        
        public ASTNode rhs() {
            return right;
        }
        
        public T getOp() {
            return op;
        }
        
        @Override
        public ASTNode copy() {
            return new BinOpExp(token.copy(), exp.copy(), (T) op.copy(), right.copy());
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(exp)) {
                exp = replacement;
            } else if(node.equals(right)) {
                right = replacement;
            } else if(node.equals(op)) {
                op = (T) replacement;
            } //else {
                //if(left instanceof BinOpExp) {
                    exp.replace(node, replacement);
                //}
                //if(right instanceof BinOpExp) {
                    right.replace(node, replacement);
                //}
                //super.replace(node, replacement);
            //}
        }
    }
    
    public static class BinOpExpBool extends BinOpExp<BinOpBool> {
        public BinOpExpBool(Token token, ASTNode left, BinOpBool op, ASTNode right) {
            super(token, left, op, right);
            this.type = Type.Bool;
        }
        
        @Override
        public ASTNode copy() {
            return new BinOpExpBool(token.copy(), exp.copy(), (BinOpBool)op.copy(), right.copy());
        }
    }
    
    public static class BinOpExpArith extends BinOpExp<BinOpArith> {
        public BinOpExpArith(Token token, ASTNode left, BinOpArith op, ASTNode right) {
            super(token, left, op, right);
            this.type = Type.Int;
        }
        
        @Override
        public ASTNode copy() {
            return new BinOpExpArith(token.copy(), exp.copy(), (BinOpArith)op.copy(), right.copy());
        }
    }
    
    public static class BinOpExpProc extends BinOpExp<BinOpProc> {
        public BinOpExpProc(Token token, ASTNode left, BinOpProc op, ASTNode right) {
            super(token, left, op, right);
            this.type = Type.Proc;
        }
        
        @Override
        public ASTNode copy() {
            return new BinOpExpProc(token.copy(), exp.copy(), (BinOpProc)op.copy(), right.copy());
        }
    }
    
    public static abstract class Op extends ASTNode {
        public Op(Token token) {
            super(token, null);
        }
    }
    
    public static abstract class UnOp extends Op {
        public UnOp(Token token) {
            super(token);
        }
    }
    
    public static abstract class UnOpBool extends UnOp {
        public UnOpBool(Token token) {
            super(token);
            this.type = Type.Bool;
        }
    }
    
    public static class NotOp extends UnOpBool {
        public NotOp() {
            this(Token.getInstance(Token.Type.Not.toString()));
        }
        public NotOp(Token token) {
            super(token);
            this.type = Type.Bool;
        }
        @Override
        public ASTNode copy() {
            return new NotOp(token.copy());
        }
    }
    
    public static abstract class BinOp extends Op {
        public BinOp(Token token) {
            super(token);
        }
    }
    
    public static abstract class BinOpArith extends BinOp {
        public BinOpArith(Token token) {
            super(token);
            this.type = Type.Int;
        }
    }
    
    public static abstract class BinOpBool extends BinOp {
        public BinOpBool(Token token) {
            super(token);
            this.type = Type.Bool;
        }
    }
    
     public static abstract class BinOpProc extends BinOp {
        public BinOpProc(Token token) {
            super(token);
            this.type = Type.Proc;
        }
    }
    
    public static abstract class BinOpBoolCompare extends BinOpBool {
        public BinOpBoolCompare(Token token) {
            super(token);
        }
    }
    
    public static abstract class BinOpBoolCompareArith extends BinOpBoolCompare {
        public BinOpBoolCompareArith(Token token) {
            super(token);
        }
    }
    
    public static abstract class BinOpBoolConnect extends BinOpBool {
        public BinOpBoolConnect(Token token) {
            super(token);
        }
    }
    
    public static class AssignmentOp extends BinOp {
        public AssignmentOp(Token token) {
            super(token);
        }
        public AssignmentOp() {
            this(Token.getInstance(Token.Type.Assignment.toString()));
        }
        
        @Override
        public ASTNode copy() {
            return new AssignmentOp(token.copy());
        }
    }
    
    public static class PlusOp extends BinOpArith {
        public PlusOp(Token token) {
            super(token);
        }
        public PlusOp() {
            this(Token.getInstance(Token.Type.Plus.toString()));
        }
        @Override
        public ASTNode copy() {
            return new PlusOp(token.copy());
        }
    }
    
    public static class MinOp extends BinOpArith {
        public MinOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new MinOp(token.copy());
        }
    }
    
    public static class TimesOp extends BinOpArith {
        public TimesOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new TimesOp(token.copy());
        }
    }
    
    public static class DivOp extends BinOpArith {
        public DivOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new DivOp(token.copy());
        }
    }
    
    public static class GTOp extends BinOpBoolCompareArith {
        public GTOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new GTOp(token.copy());
        }
    }
    
    public static class GTEOp extends BinOpBoolCompareArith {
        public GTEOp() {
            this(Token.getInstance(Token.Type.MoreThanEqual.toString()));
        }
        
        public GTEOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new GTEOp(token.copy());
        }
    }
    
    public static class LTOp extends BinOpBoolCompareArith {
        public LTOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new LTOp(token.copy());
        }
    }
    
    public static class LTEOp extends BinOpBoolCompareArith {
        public LTEOp() {
            this(Token.getInstance(Token.Type.LessThanEqual.toString()));
        }
        
        public LTEOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new LTEOp(token.copy());
        }
    }
    
    public static class EqualsOp extends BinOpBoolCompare {
        public EqualsOp(Token token) {
            super(token);
        }
        
        @Override
        public ASTNode copy() {
            return new EqualsOp(token.copy());
        }
    }
    
    public static class AndOp extends BinOpBoolConnect {
        public AndOp() {
            this(Token.getInstance(Token.Type.And.toString()));
        }
        
        public AndOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new AndOp(token.copy());
        }
    }
    
    public static class OrOp extends BinOpBoolConnect {
        public OrOp() {
            this(Token.getInstance(Token.Type.Or.toString()));
        }
        public OrOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new OrOp(token.copy());
        }
    }
    
    public static class ParallelOp extends BinOpProc {
        public ParallelOp(Token token) {
            super(token);
        }
        @Override
        public ASTNode copy() {
            return new ParallelOp(token.copy());
        }
    }
    
    public static class TypeNode extends ASTNode {        
        public TypeNode(Token token, Type type) {
            super(token, null);
            this.type = type;
        }
        
        public TypeNode(Type type) {
            this(Token.getInstance(type.toString()), type);
        }
        
        @Override
        public ASTNode copy() {
            return new TypeNode(token.copy(), type);
        }
        
        @Override
        public String toString() {
            return type + " : Type";
        }
    }
    
    public static class Range extends ASTNode {
        private Value min, max;
        
        public Range(Token token, Value min, Value max) {
            super(token, null, min, max);
            
            this.min = min;
            this.max = max;
        }
        
        public ASTNode getMin() {
            return min;
        }
        
        public ASTNode getMax() {
            return max;
        }
        
        @Override
        public ASTNode copy() {
            return new Range(token.copy(), (Value) min.copy(), (Value) max.copy());
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(min)) {
                min = (Value) replacement;
            } else if(node.equals(max)) {
                max = (Value) replacement;
            } else {
                super.replace(node, replacement);
            }
        }
        
        @Override
        public String toString() {
            return min + ".." + max + " : " + this.getClass().toString();
        }

        @Override
        public String flattenToken() {
            return min.flattenToken() + ".." + max.flattenToken();
        }
    }
    
    //Constant expressions:
    public static abstract class Value<V, N extends ASTNode> extends ASTNode<N> {
        private V value;
        
        public Value(Token token, N next, V value) {
            super(token, next);
            this.value = value;
        }
        
        public V getValue() {
            return value;
        }
        
        @Override
        public void replace(ASTNode node, ASTNode replacement) {
            replaceChild(node, replacement);
            if(node.equals(value)) {
                value = (V) replacement;
            } else {
                //super.replace(node, replacement);
            }
        }
        
        @Override
        public boolean equals(Object other) {
            if(other == null || !(other instanceof Value)) {
                return false;
            }
            
            Value o = (Value) other;
            return o.value.equals(value);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 97 * hash + Objects.hashCode(this.value);
            return hash;
        }
        
        @Override
        public String toString() {
            return value + " : " + this.getClass().toString();
        }
    }
    
    public static class Num extends Value<Integer, Num> {
        public Num(Token token, int num){
            super(token, null, num);
            this.type = Type.Int;
        }
        
        public Num(int num) {
            this(Token.getInstance(Integer.toString(num)), num);
        }
        
        @Override
        public ASTNode copy() {
            return new Num(token.copy(), this.getValue());
        }
    }
    
    public static class Bool extends Value<Boolean, Bool> {
        public Bool(Token token, boolean b){
            super(token, null, b);
            this.type = Type.Bool;
        }
        
        public Bool(boolean b) {
            this(Token.getInstance(Boolean.toString(b)), b);
        }
        
        @Override
        public ASTNode copy() {
            return new Bool(token.copy(), this.getValue());
        }
    }
    
    public static class Keyword extends Value<String, Keyword> {        
        public Keyword(Token token, String id) {
            super(token, null, id);
        }
        
        public Keyword(String id) {
            this(Token.getInstance(id), id);
        }
    }
    
    public static abstract class ID<N extends ID> extends Value<String, N> {
        public ID(Token token, N next, String id) {
            super(token, next, id);
        }
        
        @Override
        public N getNext() {
            return next;
        }
        
        @Override
        public String getValue() {
            return super.getValue();
        }
    }
    
    public static class Var extends ID<Var> {        
        public Var(Token token, String id) {
            super(token, null, id);
        }
        
        public Var(String id) {
            this(Token.getInstance(id), id);
        }
        
        @Override
        public ASTNode copy() {
            return new Var(token.copy(), super.getValue());
        }
    }
    
    public static class AccessorID extends ID<ID> {        
        public AccessorID(Token token, ID next, String id) {
            super(token, next, id);
        }
        
        /**
         * 
         * @return the left most ID. E.g. x.y.z returns x
         */
        public ID getRoot() {
            ID root = this;
            ID root2 = this;
            do {
                root = root2;
                root2 = root.getNext();
            } while(root2 != null);
            return root;
        }
        
        @Override
        public ID getNext() {
            return super.getNext();
        }
        
        @Override
        public boolean equals(Object other) {
            if(other == null || !(other instanceof Value)) {
                return false;
            }
            
            Value o = (Value) other;
            return o.value.equals(this.getValue()) && this.next.equals(o.getNext());
        }

        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }
    }   
    
    //Utilities:
    public static ASTNode filterPars(ASTNode node) {
        if(node instanceof ParenExp) {
            node = ((ParenExp)node).removeParentheses();
        }
        return node;
    }
}
