/*
 * CompilerUppaal.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import CodeGeneration.AST.*;
import CodeGeneration.Semantics.TransformPostSemanticsUPPAAL;
import CodeGeneration.Semantics.TransformPreSemanticsUPPAAL;
import CodeGeneration.Semantics.TypeChecker;
import Tools.Lambda.Action0E;
import Tools.Lambda.Action1E;
import static Tools.Utils.newLine;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Peter Maandag
 */
public class CompilerUppaal extends Compiler {    
    public static final String XML = ".xml";
    public static final String Query = ".q";
    private final boolean compileLoopCode;
    
    private int numLocations = 0;
    private String startLocation;
    //private String rangeCheckLocation;
    private String deadlockLocation;
    
    public CompilerUppaal(Formula syntaxTree) throws CompileError {
        super(syntaxTree);
        
        Formula.Mode mode = syntaxTree.getMode();
        compileLoopCode = mode == Formula.Mode.Loop || 
                mode == Formula.Mode.PropertyLoop;
    }

    @Override
    protected String[] getExtensions() {
        return new String[]{XML, Query};
    }
    
    @Override
    protected void preCheckTransformTree() throws CompileError {
        TransformPreSemanticsUPPAAL transform = new TransformPreSemanticsUPPAAL(tree);
        
        if(addSteps) {
            transform.addSystemStepsVariable();
        }
        if(tree.getMaxSteps() != null) {
            transform.addMaxStepsVariable();
        }        
        
        if(compileLoopCode) {
            transform.addLoopVariable();
        }
        
        if(addSteps && (compileLoopCode || tree.getMode() == Formula.Mode.Deadlock)) {
            //Steps variable range can prevent transitions to be made, so the loop check will fail            
            throw new CompileError("Steps variable may not be used while checking for loops or deadlock in UPPAAL");
        }
    }
    
    @Override
    protected void postCheckTransformTree() throws CompileError {
        //super.postCheckTransformTree();
        
        TransformPostSemanticsUPPAAL transform = new TransformPostSemanticsUPPAAL(tree);
        transform.transformTree();
        
        checkRestrictions();
    }
    
    private void checkRestrictions() throws CompileError {
        checkRestrictions(tree);
        if(tree instanceof ParallelSystem) {
            ParallelSystem pTree = (ParallelSystem) tree;
            AST.Process[] processes = pTree.getProcess().getDescendants();
            for(AST.Process p : processes) {
                checkRestrictions(p);
            }
        }

        /*//Find all init statements that are not assignments
        ArrayList<VarInit> varInits = new ArrayList<>();
        getInitBoolExpressions(varInits);
        
        if(!varInits.isEmpty()) {
           throw new CompileError("Boolean initialization expressions such as " + varInits.get(0).getDescription()
                   + " are currently not supported for UPPAAL");
        }
        
        ArrayList<String> vars = new ArrayList<>();
        ArrayList<VarDecl> initVars = new ArrayList<>();
        varInits.clear();
        
        //Get all the variables that are assigned a value
        getInitVarAssignments(vars, varInits);        
        for(VarInit init : varInits) {
            getVarsInExpression(initVars, init.getExpression());
        }
        if(initVars.size() != tree.getVarDecl().getDescendants().length) {
            throw new CompileError("UPPAAL compilation currently requires a completely specified start state");
        }*/
    }
    
    private int getVarDeclLength(VarDecl[] vars) {
        int count = 0;
        for(int i = 0; i< vars.length; i++) {
            if(vars[i].getTypeNode().getType() != ASTNode.Type.Proc) {
                count++;
            }
        }
        return count;
    }
    
    private void checkRestrictions(Formula f) throws CompileError {
        //Find all init statements that are not assignments
        ArrayList<VarInit> boolInits = new ArrayList<>();
        VarInit[] varInits = (f.getVarInit() == null) ? new VarInit[0] : f.getVarInit().getDescendants();
        getInitBoolExpressions(varInits, boolInits);
        
        if(!boolInits.isEmpty()) {
           throw new CompileError("Boolean initialization expressions such as " + boolInits.get(0).getDescription()
                   + " are currently not supported for UPPAAL");
        }
        
        ArrayList<String> vars = new ArrayList<>();
        ArrayList<VarDecl> varDecls = new ArrayList<>();
        ArrayList<VarInit> assInits = new ArrayList<>();
        
        //Get all the variables that are assigned a value
        getInitVarAssignments(varInits, vars, assInits);        
        for(VarInit init : assInits) {
            getVarsInExpression(f, varDecls, init.getExpression());
        }
        if(varDecls.size() != getVarDeclLength(f.getVarDecl().getDescendants())) {
            throw new CompileError("UPPAAL compilation currently requires a completely specified start state");
        }
    }
    
    //Find all the initialization expressions that are assignments
    private void getInitVarAssignments(VarInit[] inits, ArrayList<String> vars, ArrayList<VarInit> varInits) {
        //VarInit[] inits = tree.getVarInit().getDescendants();
        for(int i = 0; i < inits.length; i++) {
            if(inits[i].getType() == ASTNode.Type.Void) {
                //Void expression must be of kind ID = exp
                vars.add(((Var)inits[i].getExpression().lhs()).getValue());
                varInits.add(inits[i]);
            }
        }
    }
    
    //Find all the initialization expressions that are NOT assignments
    private void getInitBoolExpressions(VarInit[] inits, ArrayList<VarInit> varInits) {
        //VarInit[] inits = tree.getVarInit().getDescendants();
        for(int i = 0; i < inits.length; i++) {
            if(inits[i].getType() != ASTNode.Type.Void) {
                varInits.add(inits[i]);
            }
        }
    }
    
    //Find all the VarDecls that are process variables 
    private void getProcVars(VarDecl[] variables, ArrayList<VarDecl> varDecls) {
        //VarInit[] inits = tree.getVarInit().getDescendants();
        for(int i = 0; i < variables.length; i++) {
            if(variables[i].getTypeNode().getType() == ASTNode.Type.Proc) {
                //vars.add(((Var)variables[i].getVar()).getValue());
                varDecls.add(variables[i]);
            }
        }
    }
    
    //Find all the variables that are used in an ASTNode
    private void getVarsInExpression(Formula f, ArrayList<VarDecl> vars, ASTNode node) {
        if(node instanceof Var) {
            Var id = (Var) node;
            VarDecl decl = findVar(f, id);
            if(!vars.contains(decl)) {
                vars.add(decl);
            }
        } else if(node instanceof Value) {
            
        } else if(node instanceof ParenExp) {
            ParenExp p = (ParenExp) node;
            getVarsInExpression(f, vars, p.getExp());
        } else if(node instanceof BinOpExp) {
            BinOpExp exp = (BinOpExp) node;
            getVarsInExpression(f, vars, exp.lhs());
            getVarsInExpression(f, vars, exp.rhs());
        } else if(node instanceof UnOpExp) {
            UnOpExp exp = (UnOpExp) node;
            getVarsInExpression(f, vars, exp.getExp());
        } else if(node instanceof VarAss) {
            VarAss ass = (VarAss) node;
            getVarsInExpression(f, vars, ass.getID());
            getVarsInExpression(f, vars, ass.getAss());
        } else {
            print("Unrecognized expression in getVarsInExpression: " + node.toString(), ERROR);
        }
    }

    @Override
    protected void compile(String extension) throws IOException {
        switch(extension) {
            case XML:
                compileXML();
                break;
            case Query:
                compileQuery();
                break;
        }
    }
    
    private void compileXML() throws IOException {
        writeln("<?xml version=\"1.0\" encoding=\"utf-8\"?><!DOCTYPE nta PUBLIC "
                + "'-//Uppaal Team//DTD Flat System 1.1//EN' "
                + "'http://www.it.uu.se/research/group/darts/uppaal/flat-1_1.dtd'>");
        writeln("<nta>");
        
        write("<declaration>");
        writeln("// Place global declarations here.");
        if(tree instanceof ParallelSystem) {
            compileXML((ParallelSystem) tree);
        }
        writeln("</declaration>");
        
        /*writeln("<template>");
        writeln("<name>P1</name>");
        compileVarDecl();
        compileVarInit();
        compileSystem();
        writeln("</template>");*/
        if(tree instanceof TransitionSystem) {
            compileProcess(tree, "P1");
        } else if(tree instanceof ParallelSystem) {            
            ParallelSystem pTree = (ParallelSystem) tree;
            
            //Find all process variables
            ArrayList<VarDecl> procVars = new ArrayList<>();
            getProcVars(pTree.getVarDecl().getDescendants(), procVars);
            
            //Compile all the processes
            ArrayList<String> compiledProcesses = new ArrayList<>();
            for(VarDecl d : procVars) {
                //String procName = d.getVar().getValue();
                String procID = (String)d.getDeclaredValue().getValue();                
                
                if(!compiledProcesses.contains(procID)) {
                    compiledProcesses.add(procID);
                    AST.Process process = findProcess(pTree, procID);
                    compileProcess(process, procID);
                }                
            }
        }
        
        compileUPPAALSystem();
        writeln("</nta>");
    }
    
    private AST.Process findProcess(ParallelSystem p, String id) {
        AST.Process[] processes = p.getProcess().getDescendants();
        for(AST.Process process : processes) {
            if(process.getName().getValue().equals(id)) {
                return process;
            }
        }
        throw new UnsupportedOperationException("This should not happen");
    }
    
    private void compileProcess(Formula f, String name) throws IOException {
        writeln("<template>");
        writeln("<name>" + name + "</name>");
        compileVarDecl(f);
        compileVarInit();
        compileSystem(f);
        writeln("</template>");
    }
    
    private void compileXML(ParallelSystem p) throws IOException {
        VarDecl[] variables = p.getVarDecl().getDescendants();
        ArrayList<String> initVars = new ArrayList<>();
        ArrayList<VarInit> varInits = new ArrayList<>();
        getInitVarAssignments(p.getVarInit().getDescendants(), initVars, varInits);
        for(int i = variables.length-1; i >= 0; i--) {
            if(variables[i].getTypeNode().getType() != ASTNode.Type.Proc) {
                Var id = variables[i].getVar();
                ASTNode initExpression = null;
                int index = initVars.indexOf(id.getValue());
                if(index >= 0) {
                    initExpression = varInits.get(index).getExpression();
                }
                compileVarDecl(variables[i], initExpression, compileStandardID);
            }
        }
    }
    
    private String getLocation() {
        return "id" + numLocations++;
    }
    
    private String writeState(String name) throws IOException {
        String id = getLocation();
        writeln("<location id=\"" + id + "\">");
        writeln("<name>" + name + "</name>");
        if(compileLoopCode || tree.getMode() == Formula.Mode.Liveness) {
            writeln("<label kind=\"invariant\">" + TypeChecker.clockVariable + " &lt;= 1</label>");
        }
        writeln("</location>");
        return id;
    }
    
    private void compileVarInit() throws IOException {
        //Make a start and init location:
        startLocation = writeState("Start");
        //rangeCheckLocation = writeState("Range");
        //String initLocation = writeState("Init");       
    }
    
    private void compileSystem(Formula f) throws IOException {
        Stmt[] stmts;
        if(tree instanceof TransitionSystem) {
           stmts = ((TransitionSystem)tree).getStmt().getDescendants();           
        } else if (f instanceof AST.Process) {
            stmts = ((Stmt)((AST.Process)f).getSystem()).getDescendants();
        } else {
           throw new UnsupportedOperationException("Unrecognized Formula in compile()");
        }
        compileTransitions(f, stmts);
    }
    
    private void compileTransitions(Formula f, Stmt[] stmts) throws IOException {        
        //Write all target locations and remember their name:
        ArrayList<String> targets = new ArrayList<>();
        for(int i = 0; i < stmts.length; i++) {
            targets.add(writeState("trans" + i));
        }
        
        //Write deadlock location:
        boolean canDeadlock = canDeadlock(stmts);
        if(canDeadlock) {
            deadlockLocation = writeState("Deadlock");
        }
        
        //Set start location
        writeln("<init ref=\"" + startLocation + "\"/>");
        
        //Write edges for all transitions:
        for(int i = stmts.length-1; i >= 0; i--) {
            compileTransition(i, stmts[i], targets.get(i));           
        }
        
        //Write final edges:
        //compileRangeTransition(f);
        if(canDeadlock) {
            compileDeadlockTransition(stmts);
        }
    }
    
    private void writeEdge(String start, String end) throws IOException {
        writeln("<source ref=\"" + start + "\"/>");
        writeln("<target ref=\"" + end + "\"/>");
    }
    
    /*private void compileRangeTransition(Formula f) throws IOException {
        //Write Edge back to begin state:
        writeln("<transition>");
        writeEdge(rangeCheckLocation, startLocation);
        
        //Compile range checks:
        ArrayList<VarDeclRanged> ranges = new ArrayList<>();
        getRanges(f, ranges);
        write("<label kind=\"guard\">");
        compileAllRanges(ranges);
        writeln(newLine + "</label>");
        
        writeln("</transition>");        
        
        if(!ranges.isEmpty() || compileLoopCode) {
            //Write edge to deadlock state
            writeln("<transition>");
            writeEdge(rangeCheckLocation, deadlockLocation);
        
            //Compile negation of range checks:
            if(!ranges.isEmpty()) {
                write("<label kind=\"guard\">");
                write("!(");
                compileAllRanges(ranges);
                write(")");
                writeln(newLine + "</label>");
            }
            
            //Write loop variable update if checking on loops
            writeLoopVariableUpdate();
            writeln("</transition>");
        }       
    }*/
    
    private boolean canDeadlock(Stmt[] stmts) {
        boolean canDeadlock = true;
        for(Stmt s : stmts) {
            if(s.getGuard() == null) {
                canDeadlock = false;
                break;
            }
        }
        return canDeadlock;
    }
    
    private void compileDeadlockTransition(Stmt[] stmts) throws IOException {       
        writeln("<transition>");

        //Write outgoing arrow:
        writeEdge(startLocation, deadlockLocation);

        //Compile all guard checks:
        write("<label kind=\"guard\">");      
        compileAllGuards(stmts);      
        writeln(newLine + "</label>");

        //Write loop variable update if checking on loops
        writeLoopVariableUpdate();

        writeln("</transition>");
    }
    
    private void writeLoopVariableUpdate() throws IOException {
        if(compileLoopCode) {
            write("<label kind=\"assignment\">");
            write(TypeChecker.loopVariable + "=false");
            writeln(newLine + "</label>");
        }
    }
    
    private void getRanges(Formula f, ArrayList<VarDeclRanged> ranges) {
        VarDecl[] vars = f.getVarDecl().getDescendants();
        
        //Get all variable ranges:
        for(VarDecl var : vars) {
            if(var instanceof VarDeclRanged) {
                VarDeclRanged rangedVar = (VarDeclRanged) var;
                ranges.add(rangedVar);
            }
        }
    }
    
    private void compileAllGuards(Stmt[] stmts) throws IOException {
        boolean foundOne = false;
        for(int i = stmts.length-1; i >= 0; i--) {
            Stmt s = stmts[i];
            if(s.getGuard() != null) {
                if(foundOne) {
                    write(" &amp; ");
                }
                write("!(");
                compileExpression(s.getGuard());
                write(")");
                foundOne = true;
            }           
        }
    }
    
    private void compileAllRanges(ArrayList<VarDeclRanged> ranges) throws IOException {        
        for(int i = 0; i < ranges.size(); i++) {           
            compileRange(ranges.get(i));
            if(i < ranges.size() -1) {
                write(" &amp; ");
            }
        }
    }
    
    private void compileRange(VarDeclRanged r) throws IOException {
        compileExpression(r.getVar());
        write(" &gt;= ");
        compileExpression(r.getRange().getMin());
        write(" &amp; ");
        compileExpression(r.getVar());
        //Fix for maximum number of steps being offset by 1
        if(r.getVar().getValue().equals(TypeChecker.stepsVariable)) {
            write(" &lt; ");     
        } else {
            write(" &lt;= ");
        }                
        compileExpression(r.getRange().getMax());
    }
    
    private void compileTransition(int index, Stmt s, String targetID) throws IOException {
        writeln("<transition>");
        
        //Write outgoing arrow:
        writeEdge(startLocation, targetID);
        if(s.getGuard() != null) {
            write("<label kind=\"guard\">");
            compileExpression(s.getGuard());
            writeln(newLine + "</label>");
        } 
        compileVarAss(index);
        writeln("</transition>");
        
        //Write arrow going back to start
        writeln("<transition>");
        //writeEdge(targetID, rangeCheckLocation);
        writeEdge(targetID, startLocation);
        writeln("</transition>");
    }
    
    private void compileVarAss(Formula f, VarAss ass, String name) throws IOException {        
        VarAss[] parents = ass.getDescendants();
        writeln("void " + name + "()");
        writeln("{");
        
        //Get all used variables:
        ArrayList<VarDecl> vars = new ArrayList<>();
        for(int j = parents.length-1; j >= 0; j--) {
            VarAss t = parents[j];
            getVarsInExpression(f, vars, t);
        }
        //Filter generated varDecls:
        for(int i = 0; i<vars.size(); i++) {
            VarDecl v = vars.get(i);
            if(v.getToken().getType() == Token.Type.Generated) {
                vars.remove(i);
                i--;
            }
        }
        //Make local copies:
        for(int i = 0; i<vars.size(); i++) {
            VarDecl v = vars.get(i);
            write("\t");
            compileExpression(v.getTypeNode());
            write(" ");
            compileExpression(v.getVar(), compileCopyID);
            write(" = ");
            compileExpression(v.getVar());
            writeln(";");
        }
        
        //Write the variable updates
        for(int i = parents.length-1; i >= 0; i--) {
            VarAss t = parents[i];          
            
            write("\t" + t.getID().getValue() + " = ");
            if(t.getToken().getType() == Token.Type.Generated) {
                compileExpression(t.getAss());
            } else {
                compileExpression(t.getAss(), compileCopyID);
            }
            
            writeln(";");
        }
        
        writeln("}" + newLine);
    }
    
    private void compileVarAss(int index) throws IOException {
        write("<label kind=\"assignment\">");
        write("_trans" + index + "()");        
        writeln(newLine + "</label>");
    }
    
    private void compileUPPAALSystem() throws IOException {
        writeln("<system>// Place template instantiations here.");
        
        ArrayList<VarDecl> procVars = new ArrayList<>();
        if(tree instanceof TransitionSystem) {
            writeln("Process = P1();");
        } else if(tree instanceof ParallelSystem) {            
            ParallelSystem pTree = (ParallelSystem) tree;
            
            //Find all process variables            
            getProcVars(pTree.getVarDecl().getDescendants(), procVars);
            
            //Declare all the processes
            for(VarDecl d : procVars) {
                write(d.getVar().getValue());
                write(" = ");
                write((String)d.getDeclaredValue().getValue());
                writeln("();");
            }            
        }

        writeln("// List one or more processes to be composed into a system.");
        if(tree instanceof TransitionSystem) {
            writeln("system Process;</system>");
        } else if(tree instanceof ParallelSystem) {            
            ParallelSystem pTree = (ParallelSystem) tree;
            
            write("system ");
            for(int i = 0; i<procVars.size(); i++) {
                VarDecl var = procVars.get(i);
                write(var.getVar().getValue());
                if(i < procVars.size()-1) {
                    write(", ");
                }
            }
            writeln(";</system>");
        }
    }
    
    //Initialize all assigned variables
    private void compileVarDecl(Formula f) throws IOException {
        write("<declaration>");
        
        VarDecl[] variables = f.getVarDecl().getDescendants();
        ArrayList<String> initVars = new ArrayList<>();
        ArrayList<VarInit> varInits = new ArrayList<>();
        getInitVarAssignments(f.getVarInit().getDescendants(), initVars, varInits);
        for(int i = variables.length-1; i >= 0; i--) {
            Var id = variables[i].getVar();
            ASTNode initExpression = null;
            int index = initVars.indexOf(id.getValue());
            if(index >= 0) {
                initExpression = varInits.get(index).getExpression();
            }
            compileVarDecl(variables[i], initExpression, compileStandardID);
            //Make a copy of all old values of variables to ensure proper updating
            /*if(variables[i].getToken().getType() != Token.Type.Generated) {
                compileVarDecl(type, id, expression, compileCopyID);
            }*/
        }
        
        if(compileLoopCode || tree.getMode() == Formula.Mode.Liveness) {
            writeln("clock " + TypeChecker.clockVariable + ";");
        }
        
        //Add all transition updates as functions:
        Stmt[] stmts;
        if(f instanceof TransitionSystem) {
            stmts = ((TransitionSystem)f).getStmt().getDescendants();           
        } else if (f instanceof AST.Process) {
            stmts = ((Stmt)((AST.Process)f).getSystem()).getDescendants();
        } else {
            throw new UnsupportedOperationException("Unrecognized Formula in compileVarDecl()");
        }
        for(int i = stmts.length-1; i >= 0; i--) {
            compileVarAss(f, stmts[i].getEffect(), "_trans" + i);
        }
        
        writeln("</declaration>");
    }
    
    private void compileVarDecl(VarDecl decl, ASTNode initExpression, 
            Action1E<Var, IOException> idWriter) throws IOException {
        compileExpression(decl.getTypeNode());
        write(" ");
        
        if(decl instanceof VarDeclRanged) {
            VarDeclRanged r = (VarDeclRanged) decl;
            write("[");
            compileExpression(r.getRange().getMin(), idWriter);
            write(", ");
            compileExpression(r.getRange().getMax(), idWriter);
            write("] ");
        }
        
        if(initExpression != null) {
            compileExpression(initExpression, idWriter);
        } else {
            compileExpression(decl.getVar(), idWriter);
        }            

        writeln(";");
    }
    
    private VarDecl findVar(Formula f, Var id) {
        VarDecl dec = f.getVarDecl();
        do {
            if(dec.getVar().equals(id)) {
                return dec;
            }
            dec = dec.getNext();
        } while(dec != null);
        
        if(f instanceof AST.Process && dec == null) {
            dec = tree.getVarDecl();
            do {
                if(dec.getVar().equals(id)) {
                    return dec;
                }
                dec = dec.getNext();
            } while(dec != null);
        }
        
        print("Couldn't find var " + id.getValue() + " in findVar CompilerUppaal!", ERROR);
        return null;
    }
    
    private void compileExpression(ASTNode t) throws IOException {
        compileExpression(t, compileStandardID, this::compileBinOpXML);
    }
    
    private void compileExpression(ASTNode t, Action1E<Var, IOException> idWriter) throws IOException {
        compileExpression(t, idWriter, this::compileBinOpXML);
    }
    
    private void compileExpression(ASTNode t, Action1E<Var, IOException> idWriter,
            Action1E<BinOp, IOException> binOpWriter) throws IOException {
        if(t instanceof Var) {
            Var id = (Var) t;
            idWriter.invoke(id);
        } else if(t instanceof Num) {
            Num num = (Num) t;
            write(Integer.toString(num.getValue()));
        } else if(t instanceof Bool) {
            boolean b = ((Bool) t).getValue();
            String bool = b ? "true" : "false";
            write(bool);
        } else if(t instanceof AccessorID) {
            AccessorID id = (AccessorID) t;
            write(id.getRoot().getValue());
            write(".");
            //@Todo Single support only:
            write(id.getValue());
        } else if (t instanceof TypeNode) {
            TypeNode typeNode = (TypeNode) t;
            switch(typeNode.getType()) {
                case Int:
                    write("int"); break;
                case Bool:
                    write("bool"); break;
                default:
                    print("Unrecognized type in compileExpression: " 
                            + typeNode.getType().toString(), ERROR);
            }
        } else if(t instanceof ParenExp) {
            ParenExp p = (ParenExp) t;
            write("(");
            compileExpression(p.getExp(), idWriter, binOpWriter);
            write(")");
        } else if(t instanceof BinOpExp) {
            compileBinOpExp((BinOpExp)t, idWriter, binOpWriter);
        } else if(t instanceof UnOpExp) {
            write("!");
            compileExpression(((UnOpExp)t).getExp(), idWriter, binOpWriter);
        } else {
            print("Unrecognized expression in compileExpression: " + t.toString(), ERROR);
        }
    }
    
    private void compileBinOpExp(BinOpExp e, Action1E<Var, IOException> idWriter,
            Action1E<BinOp, IOException> binOpWriter) throws IOException {        
        BinOp op = e.getOp();
        ASTNode lhs = e.lhs();
        ASTNode rhs = e.rhs();
        
        compileExpression(lhs, idWriter, binOpWriter);
        write(" ");
        compileBinOp(op, binOpWriter);
        write(" ");
        compileExpression(rhs, idWriter, binOpWriter);
    }
    
    private void compileBinOpQ(BinOp op) throws IOException {
        if(op instanceof LTEOp) {
            write("<="); 
        } else if(op instanceof GTEOp) {
            write(">=");
        } else if(op instanceof LTOp) {
            write("<");
        } else if(op instanceof GTOp) {
            write(">");
        } else if(op instanceof AndOp) {
            write("and");
        } else if(op instanceof OrOp) {
            write("or");
        } else {
            print("Unrecognized operator in compileBinOpQ: " + op.toString(), ERROR);
        }
    }
    
    private void compileBinOpXML(BinOp t) throws IOException {
        if(t instanceof LTEOp) {
            write("&lt;=");
        } else if(t instanceof GTEOp) {
            write("&gt;=");
        } else if(t instanceof LTOp) {
            write("&lt;");
        } else if(t instanceof GTOp) {
            write("&gt;");
        } else if(t instanceof AndOp) {
            write("&amp;");
        } else if(t instanceof OrOp) {
            write("|");
        } else {
            print("Unrecognized operator in compileBinOpXML: " + t.toString(), ERROR);
        }
    }
    
    private void compileBinOp(BinOp op, Action1E<BinOp, IOException> binOpCompiler) throws IOException {
        if(op instanceof PlusOp) {
            write("+");
        } else if(op instanceof MinOp) {
            write("-");
        } else if(op instanceof DivOp) {
            write("/");
        } else if(op instanceof TimesOp) {
            write("*");
        } else if(op instanceof EqualsOp) {
            write("==");
        } else if(op instanceof AssignmentOp) {
            write("=");
        } else {
            binOpCompiler.invoke(op);
        }
    }
    
    private void compileQuery() throws IOException {
        writeln("/*" + newLine + newLine + "*/");
        
        switch(tree.getMode()) {
           case Reachability:
               write("E<> (");        
               compileExpression(tree.getGoal(), compileQueryID, this::compileBinOpQ);        
               writeln(")");
               break;
           case Loop:
               compileGoal(() -> writeln("E[] Process." + TypeChecker.loopVariable),
                       (procVars) -> {
                           write("E[] (");
                            for(int i = 0; i< procVars.size(); i++) {
                                write(procVars.get(i).getVar().getValue() + ".loop");
                                if(i < procVars.size() -1) {
                                    write(" or ");
                                }
                            }
                            writeln(")");
                       }
                       );              
               break;
//           case PropertyLoop:
//               writeln("E[] Process." + TypeChecker.loopVariable);
//               writeln("/*" + newLine + newLine + "*/");
//               write("Process." + TypeChecker.loopVariable + " --> (");
//               compileExpression(tree.getGoal(), compileQueryID, this::compileBinOpQ);
//               write(") or Process.Deadlock");
//               break;
           case Safety:
               write("A[] (");
               compileExpression(tree.getGoal(), compileQueryID, this::compileBinOpQ);
               writeln(")");
               break;
           case Deadlock:
               compileGoal(() -> writeln("E<> Process.Deadlock"),
                       (procVars) -> {
                           write("E<> (");
                            for(int i = 0; i< procVars.size(); i++) {
                                write(procVars.get(i).getVar().getValue() + ".Deadlock");
                                if(i < procVars.size() -1) {
                                    write(" and ");
                                }
                            }
                            writeln(")");
                       }
                       );              
               break;
           case Liveness:
               write("A<> (");
               compileExpression(tree.getGoal(), compileQueryID, this::compileBinOpQ);        
               writeln(")");
               break;
           default:
               print("Unsupported mode in compileGoalState", ERROR);
       }
    }
    
    private void compileGoal(Action0E<IOException> ts, Action1E<ArrayList<VarDecl>, IOException> ps
            ) throws IOException {
        if(tree instanceof TransitionSystem) {
             ts.invoke();
        } else if (tree instanceof ParallelSystem) {
            //Find all process variables
             ArrayList<VarDecl> procVars = new ArrayList<>();
             getProcVars(tree.getVarDecl().getDescendants(), procVars);
             
             ps.invoke(procVars);
        } else {
            throw new UnsupportedOperationException("Unrecognized Formula in compileGoal()");
        }
    }
    
    //Function pointers for different translations of ID and BinOp
    Action1E<Var, IOException> compileStandardID 
            = x -> write(x.getValue());
    Action1E<Var, IOException> compileQueryID 
            = x -> {
                if(tree instanceof TransitionSystem) {
                    write("Process." + x.getValue());
                } else if(tree instanceof ParallelSystem) {
                    write(x.getValue());
                } else {
                    throw new UnsupportedOperationException("Unrecognized Formula in compile()");
                }
            };
    Action1E<Var, IOException> compileCopyID 
            = x -> write(x.getValue() + "_old"); //@Todo: use a unique name!!
}


/*private void compileVarAss(VarAss ass) throws IOException {        
    VarAss[] parents = ass.getDescendants();
    write("<label kind=\"assignment\">");        

    //Make copies of all used variables:
    ArrayList<VarDecl> vars = new ArrayList<>();
    for(int j = parents.length-1; j >= 0; j--) {
        VarAss t = parents[j];
        getVarsInExpression(vars, t);
    }
    //Filter generated varDecls:
    for(int i = 0; i<vars.size(); i++) {
        VarDecl v = vars.get(i);
        if(v.getToken().getType() == Token.Type.Generated) {
            vars.remove(i);
            i--;
        }
    }
    for(int i = 0; i<vars.size(); i++) {
        VarDecl v = vars.get(i);
        //@Todo: use the unique name!!            
        write(v.getID().getValue() + "_old = " + v.getID().getValue());
        if(i < vars.size()-1 || parents.length > 0) {
            writeln(",");
        }
    }

    //Write the variable updates
    for(int i = parents.length-1; i >= 0; i--) {
        VarAss t = parents[i];          

        write(t.getID().getValue() + " = ");
        if(t.getToken().getType() == Token.Type.Generated) {
            compileExpression(t.getAss());
        } else {
            compileExpression(t.getAss(), compileCopyID);
        }

        if(i > 0) {
            write(", ");
        }
    }
    writeln(newLine + "</label>");
}*/
