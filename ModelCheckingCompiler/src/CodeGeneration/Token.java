/*
 * Token.java
 * Copyright(c) 2014
 */
package CodeGeneration;

/**
 *
 * @author Peter Maandag
 */
public class Token {
    
    public enum Type {
        Number, ID, Plus, Minus, Div, Times, ParenOpen, ParenClose, FieldAccessor,
        LessThan, MoreThan, LessThanEqual, MoreThanEqual, Equals, And, Or, Not, 
        Parallel, Assignment, AssignmentSeperator, Statement, Guard, Transition,
        Colon, RangeSeperator, MAXSTEPS, VARS, INIT, TRANS, PROGRAM, PROCESS, 
        GOAL, LOOP, DEADLOCK, SAFE, LIVENESS, TRUE, FALSE, INT, BOOL, PROC, CONST, NONDET, 
        Comment, LineSeparator, EOF, Generated
    }
    
    private Type type;
    private String value;
    private int lineNumber, column;
    
    public Token(Type type, String value, int lineNumber, int column) {
        this.type = type;
        this.value = value;
        this.lineNumber = lineNumber;
        this.column = column;
    }
    
    public Token copy() {
        return new Token(type, value, lineNumber, column);
    }
    
    public int getLineNumber() {
        return lineNumber;
    }
    
    public int getColumn() {
        return column;
    }
    
    public Type getType() {
        return type;
    }
    
    public String value() {
        return value;
    }
    
    public static Token getInstance() {
        return new Token(Type.Generated, "**GENERATED TOKEN**", -1, -1);
    }
    
    public static Token getInstance(String desc) {
        return new Token(Type.Generated, "**GENERATED " + desc + "**", -1, -1);
    }
    
    @Override
    public String toString() {
        return value + " " + type;
    }
    
}
