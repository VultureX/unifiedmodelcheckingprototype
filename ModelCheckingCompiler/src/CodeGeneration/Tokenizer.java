/*
 * Tokenizer.java
 * Copyright(c) 2014
 */
package CodeGeneration;

import Tools.FileInfo;
import Tools.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Peter Maandag
 */
public class Tokenizer {
    //Restricted atomic strings:
    public static final String PAREN_OPEN = "(";
    public static final String PAREN_CLOSE = ")";
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String TIMES = "*";
    public static final String DIV = "/";
    public static final String LESS_THAN = "<";
    public static final String MORE_THAN = ">";
    public static final String LESS_THAN_EQUAL = "<=";
    public static final String MORE_THAN_EQUAL = ">=";
    public static final String EQUALS = "==";
    public static final String AND = "&";
    public static final String OR = "|";
    public static final String NOT = "!";
    public static final String PARALLEL = "||";
    public static final String FIELD_ACCESSOR = ".";
    public static final String ASSIGNMENT = "=";
    public static final String ASSIGNMENT_SEPERATOR = ",";
    public static final String TRANSITION = "->";
    public static final String STATEMENT = ";";
    public static final String COMMENT = "//";
    public static final String GUARD = "?";
    public static final String COLON = ":"; //used as range declaration and block end
    public static final String RANGE_SEPERATOR = "..";
    public static final String MAXSTEPS = "MAXSTEPS";
    public static final String VARS = "VARS";
    public static final String INIT = "INIT";
    public static final String TRANS = "TRANS";
    public static final String PROGRAM = "PROGRAM";
    public static final String PROCESS = "PROCESS";
    public static final String GOAL = "REACH";
    public static final String LOOP = "INF";
    public static final String DEADLOCK = "DEADLOCK";
    public static final String SAFE = "SAFE";
    public static final String LIVENESS = "LIVENESS";
    public static final char LINE_SEPARATOR = '\n';
    
    //Keywords:
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String INT = "int";
    public static final String BOOL = "bool";
    public static final String PROC = "proc";
    public static final String CONST = "const";
    public static final String NONDET = "nondet";    
    
    private StringBuilder currentString = new StringBuilder();    
    private ArrayList<Token> tokens = new ArrayList<>();
    private int lineNumber = 1;
    private int column = 0;
    private int columnStart = 0;
    private String source;
    private char c;
    
    public Tokenizer(String code) {
        this.source = code;
    }
    
    public Tokenizer(FileInfo file) {
        this.source = FileReader.readFile(file);
    }
    
    public Iterator<Token> getIterator() {        
        return tokens.iterator();
    }
    
    private void consumeToken() {
        if(currentString.length() != 0) {
            Token t = consume(currentString.toString());
            if(t.getType() != Token.Type.LineSeparator) {
                tokens.add(t);
            }

            this.currentString = new StringBuilder();
        }
        if(c == LINE_SEPARATOR) {
            lineNumber++;
            column = 0;
            columnStart = 0;
        } else {
            columnStart = column;
        }
    }
    
    private int skipToNextLine(int i) {
        while(i < source.length()-1 && c != LINE_SEPARATOR) {           
            c = source.charAt(i);
            currentString.append(c);
            i++;
        }
        
        Token t = new Token(Token.Type.Comment, currentString.toString(), lineNumber, columnStart);
        tokens.add(t);
        
        column = 0;
        this.currentString = new StringBuilder();
        
        if(i < source.length()-1) {
            return i-2;
        }
        return i;
    }
    
    private void sanitizeLineSeperators() {
        StringBuilder b = new StringBuilder();
        
        String[] lines = source.split("\r\n|\r|\n");
        for (String line : lines) {
            b.append(line).append(LINE_SEPARATOR);       
        }
        //b.deleteCharAt(b.length()-1); //Don't add extra last Line Separator!
        source = b.toString();
    }
    
    public void tokenize() {
        sanitizeLineSeperators();
        
        this.tokens = new ArrayList<>();
        this.currentString = new StringBuilder();
        this.lineNumber = 1;
        this.column = 0;
        this.columnStart = 0;
        for(int i = 0; i < source.length(); i++) {
            c = source.charAt(i);
            column++;
            
            if(Character.isWhitespace(c)) {
                //Always consumeToken after whitespace
                consumeToken();
            } else {                
                //If it's an empty token don't bother extra pattern matching
                if(currentString.length() != 0) {                    
                    //Try matching atomic string of length 2
                    Token t = tryAtomic(currentString.toString() + Character.toString(c));
                    if(t != null) {
                        if(t.getType() == Token.Type.Comment) {
                            i = skipToNextLine(i);                            
                        } else {
                            currentString.append(c);
                            consumeToken();
                        }                       
                        continue;
                    } else { 
                        //Try matching atomic string of length 1
                        t = tryAtomic(currentString.toString());
                        if(t != null) {
                            column--;
                            consumeToken();
                            column++;
                        } else {
                            //Try parsing a number
                            t = tryNumber(currentString.toString());
                            if(t != null && !Character.isDigit(c)) {
                                column--;
                                consumeToken();
                                column++;
                            } else {
                                //Also consume the current token if a new one comes after
                                t = tryAtomic(Character.toString(c));
                                if(t != null) {
                                    column--;
                                    consumeToken();
                                    column++;
                                }
                            }
                        }
                    }                    
                }
                currentString.append(c);
            }
        }       
        
        tokens.add(new Token(Token.Type.EOF, "EOF", lineNumber, columnStart));
    }
    
    private Token tryKeyword(String s) {
        Token.Type type;
        
        switch(s) {
            case TRUE:
                type = Token.Type.TRUE; break;
            case FALSE:
                type = Token.Type.FALSE; break;
            case INT:
                type = Token.Type.INT; break;
            case BOOL:
                type = Token.Type.BOOL; break;
            case PROC:
                type = Token.Type.PROC; break;
            case CONST:
                type = Token.Type.CONST; break;
            case NONDET:
                type = Token.Type.NONDET; break;
            case MAXSTEPS:
                type = Token.Type.MAXSTEPS; break;
            case VARS:
                type = Token.Type.VARS; break;
            case INIT:
                type = Token.Type.INIT; break;
            case TRANS:
                type = Token.Type.TRANS; break;
            case PROGRAM:
                type = Token.Type.PROGRAM; break;
            case PROCESS:
                type = Token.Type.PROCESS; break;
            case GOAL:
                type = Token.Type.GOAL; break;
            case LOOP:
                type = Token.Type.LOOP; break;
            case DEADLOCK:
                type = Token.Type.DEADLOCK; break;
            case SAFE:
                type = Token.Type.SAFE; break;
            case LIVENESS:
                type = Token.Type.LIVENESS; break;
            default:
                return null;
        }
        
        return new Token(type, s, lineNumber, columnStart);
    }
    
    private Token tryAtomic(String s) {
        Token.Type type; 
        
        switch(s) {
            case PAREN_OPEN:
                type = Token.Type.ParenOpen; break;
            case PAREN_CLOSE:
                type = Token.Type.ParenClose; break; 
            case PLUS:
                type = Token.Type.Plus; break; 
            case MINUS:
                type = Token.Type.Minus; break; 
            case TIMES:
                type = Token.Type.Times; break; 
            case DIV:
                type = Token.Type.Div; break; 
            case LESS_THAN:
                type = Token.Type.LessThan; break; 
            case MORE_THAN:
                type = Token.Type.MoreThan; break; 
            case LESS_THAN_EQUAL:
                type = Token.Type.LessThanEqual; break; 
            case MORE_THAN_EQUAL:
                type = Token.Type.MoreThanEqual; break; 
            case EQUALS:
                type = Token.Type.Equals; break;
            case ASSIGNMENT:
                type = Token.Type.Assignment; break;
            case ASSIGNMENT_SEPERATOR:
                type = Token.Type.AssignmentSeperator; break;
            case TRANSITION:
                type = Token.Type.Transition; break;
            case STATEMENT:
                type = Token.Type.Statement; break;
            case GUARD:
                type = Token.Type.Guard; break;
            case AND:
                type = Token.Type.And; break;
            case OR:
                type = Token.Type.Or; break;
            case NOT: 
                type = Token.Type.Not; break;
            case PARALLEL:
                type = Token.Type.Parallel; break;
            case FIELD_ACCESSOR:
                type = Token.Type.FieldAccessor; break;
            case COLON:
                type = Token.Type.Colon; break;
            case RANGE_SEPERATOR:
                type = Token.Type.RangeSeperator; break;
            case COMMENT:
                type = Token.Type.Comment; break;
            default:
                return null;
        }
        return new Token(type, s, lineNumber, columnStart); 
    }
    
    private Token consume(String s) {
        Token t = tryAtomic(s);      
        
        if(t == null) {
            t = tryNumber(s);
            if(t == null) {
                t = tryKeyword(s);
                if(t == null) {
                    t = new Token(Token.Type.ID, s, lineNumber, columnStart);
                }
            }
        }
        
        return t;
    }
    
    private Token tryNumber(String s) {
        try {
            Integer.parseInt(s);
            return new Token(Token.Type.Number, s, lineNumber, columnStart);
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public String toString() {
        Iterator i = tokens.iterator();
        StringBuilder b = new StringBuilder();
        while(i.hasNext()) {
            b.append(i.next().toString()).append("\n"); 
        }
        return b.toString();
    }
    
}
