/*
 * FlashingTabbedPane.java
 * Copyright(c) 2014
 */

package GUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTabbedPane;
import javax.swing.Timer;

/**
 *
 * @author Peter Maandag
 */
public class FlashingTabbedPane extends JTabbedPane {

    private int _tabIndex;
    private Color _background;
    private Color _foreground;
    private Color _savedBackground;
    private Color _savedForeground;
    private Timer timer = new Timer(1000, new ActionListener() {

        private boolean on = false;

        @Override
        public void actionPerformed(ActionEvent e) {
            flash(on);
            on = !on;
        }
    });
    
    //@Todo: fix background colors are ignored by Nimbus look and feel
    //@Todo: flash on multiple tabs
    public void flash(int tabIndex /*, Color foreground, Color background*/) {
        _tabIndex = tabIndex;
        _savedForeground = getForeground();
        _savedBackground = getBackground();
        _foreground = Color.RED;
        //_background = background;
        timer.start();
    }

    private void flash(boolean on) {
        if (on) {
            if (_foreground != null) {
                setForegroundAt(_tabIndex, _foreground);
            }
            if (_background != null) {
                setBackgroundAt(_tabIndex, _background);
            }
        } else {
            if (_savedForeground != null) {
                setForegroundAt(_tabIndex, _savedForeground);
            }
            if (_savedBackground != null) {
                setBackgroundAt(_tabIndex, _savedBackground);
            }
        }
        repaint();
    }
    
    public void stopFlashing() {
        timer.stop();
        setForegroundAt(_tabIndex, _savedForeground);
        setBackgroundAt(_tabIndex, _savedBackground);
    }
}
