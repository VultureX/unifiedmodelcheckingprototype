/*
 * HTMLWordWrap.java
 * Copyright(c) 2014
 */

package GUI;

import static Tools.Utils.newLine;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.SizeRequirements;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.GlyphView;
import javax.swing.text.ParagraphView;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.InlineView;

/**
 *
 * @author Peter Maandag
 */
public class HTMLWordWrap extends HTMLEditorKit {

    @Override 
    public ViewFactory getViewFactory(){
        return new HTMLFactory(){ 
            @Override
            public View create(Element e){ 
               View v = super.create(e); 
               if(v instanceof InlineView){
                   return new InlineView(e){ 
                       @Override
                       public int getBreakWeight(int axis, float pos, float len) { 
                           //return View.BadBreakWeight;
                           if (axis == View.X_AXIS) {
                               checkPainter();
                               int p0 = getStartOffset();
                               int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                               if (p1 == p0) {
                                   // can't even fit a single character
                                   return View.BadBreakWeight;
                               }
                               try {
                                   //if the view contains line break char return forced break
                                   if (getDocument().getText(p0, p1 - p0).indexOf(newLine) >= 0) {
                                       return View.ForcedBreakWeight;
                                   }
                               }
                               catch (BadLocationException ex) {
                                   //should never happen
                               }  

                           }
                           return super.getBreakWeight(axis, pos, len);
                       } 
                       @Override
                       public View breakView(int axis, int p0, float pos, float len) { 
                           if (axis == View.X_AXIS) {
                               checkPainter();
                               int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                               try {
                                   //if the view contains line break char break the view
                                   int index = getDocument().getText(p0, p1 - p0).indexOf(newLine);
                                   if (index >= 0) {
                                       GlyphView v = (GlyphView) createFragment(p0, p0 + index + 1);
                                       return v;
                                   }
                               }
                               catch (BadLocationException ex) {
                                   //should never happen
                               }

                           }
                           return super.breakView(axis, p0, pos, len);
                     } 
                   }; 
               } 
               else if (v instanceof ParagraphView) {
                   return new ParagraphView(e) { 
                        @Override
                        protected SizeRequirements calculateMinorAxisRequirements(int axis, SizeRequirements r) { 
                           if (r == null) { 
                                 r = new SizeRequirements(); 
                           } 
                           float pref = layoutPool.getPreferredSpan(axis); 
                           float min = layoutPool.getMinimumSpan(axis); 
                           // Don't include insets, Box.getXXXSpan will include them. 
                             r.minimum = (int)min; 
                             r.preferred = Math.max(r.minimum, (int) pref); 
                             r.maximum = Integer.MAX_VALUE; 
                             r.alignment = 0.5f; 
                           return r; 
                         }

                     }; 
                 }
               return v; 
             } 
         }; 
     }
    
    class NumberedParagraphView extends ParagraphView {
        private final short NUMBERS_WIDTH = 10;

        public NumberedParagraphView(Element e) {
            super(e);
            short top = 0;
            short left = 0;
            short bottom = 0;
            short right = 0;
            this.setInsets(top, left, bottom, right);
        }

        @Override
        protected void setInsets(short top, short left, short bottom,
                                 short right) {
            super.setInsets(top,(short)(left+NUMBERS_WIDTH),
                                 bottom,right);
        }

        @Override
        public void paintChild(Graphics g, Rectangle r, int n) {
            super.paintChild(g, r, n);
            int previousLineCount = getPreviousLineCount();
            int numberX = r.x - getLeftInset();
            int numberY = r.y + r.height;

            if(previousLineCount + n + 1 > 1) {
                g.drawString(Integer.toString(previousLineCount + n + 1),
                                          numberX, numberY);
            }
        }

        public int getPreviousLineCount() {
            int lineCount = 0;
            View parent = this.getParent();
            int count = parent.getViewCount();
            for (int i = 0; i < count; i++) {
                if (parent.getView(i) == this) {
                    break;
                }
                else {
                    lineCount += parent.getView(i).getViewCount();
                }
            }
            return lineCount;
        }
    }

}
