/*
 * ControllerWindow.java
 * Copyright(c) 2014
 */
package GUI;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public class ControllerWindow extends ControllerClose<JFrame> {    
    private WindowController w;
    
    public ControllerWindow(MainLogic model, MainWindow gui) {
        super(model, gui);
        this.w = new WindowController();
    }   

    @Override
    public void register(JFrame... components) {        
        for(JFrame frame : components) {
            frame.addWindowListener(w);
        }
    }

    @Override
    public void unregister(JFrame... components) {
        for(JFrame frame : components) {
            frame.removeWindowListener(w);
        }
    }
    
    //@Todo: add shutdown hook to virtual machine that closes running benchmarks!
    private class WindowController extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent we) {
            ControllerWindow.this.close();
        }
    }    
}
