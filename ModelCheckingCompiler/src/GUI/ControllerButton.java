/*
 * ControllerButton.java
 * Copyright(c) 2014
 */

package GUI;

import Benchmarks.CMDAlgorithm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public class ControllerButton extends Controller<JButton> implements ActionListener {
    
    public ControllerButton(MainLogic model, MainWindow gui) {
        super(model, gui);
    }

    @Override
    public void register(JButton... components) {
        for(JButton button : components) {
            button.addActionListener(this);
        }
    }

    @Override
    public void unregister(JButton... components) {
        for(JButton button : components) {
            button.removeActionListener(this);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
            case MainWindow.BUTTON_START:
                //gui.enableButton(MainWindow.BUTTON_START, false);
                //gui.enableButton(MainWindow.BUTTON_STOP, true);
                //model.runBenchmark();
                model.compileAndRun(gui.getEditorText(), gui.getEditorRelativeDir(), gui.getEditorCompilename(), 0);
                break;
            case MainWindow.BUTTON_STOP:
                model.killBenchmark(CMDAlgorithm.USER_ABORT);
                //gui.enableButton(MainWindow.BUTTON_START, true);
                //gui.enableButton(MainWindow.BUTTON_STOP, false);
                break;
            case MainWindow.BUTTON_COMPILE:
                model.tryCompile(gui.getEditorText(), gui.getEditorRelativeDir(), gui.getEditorCompilename());
                break;
            default:
                System.err.println("Unimplemented Button: " + e.getActionCommand());
        }
    }
}
