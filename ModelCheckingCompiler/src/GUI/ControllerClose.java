/*
 * ControllerClose.java
 * Copyright(c) 2014
 */

package GUI;

import Benchmarks.CMDAlgorithm;
import java.awt.Component;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public abstract class ControllerClose<T extends Component> extends Controller<T> {
    public ControllerClose(MainLogic model, MainWindow gui) {
        super(model, gui);
    }
    
    //@Todo: add shutdown hook to virtual machine that closes running benchmarks!
    public void close() {
        model.killBenchmark(CMDAlgorithm.USER_ABORT);
        gui.dispose();        
        System.exit(0);
    }
}
