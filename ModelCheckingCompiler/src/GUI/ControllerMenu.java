/*
 * ControllerMenu.java
 * Copyright(c) 2014
 */

package GUI;

import CodeGeneration.CompilerNuSMV;
import CodeGeneration.CompilerSMT;
import CodeGeneration.CompilerUppaal;
import Tools.DirectoryInfo;
import Tools.FileInfo;
import Tools.FileReader;
import Tools.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileFilter;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public class ControllerMenu extends ControllerClose<JMenuItem> implements ActionListener, ItemListener {    
    public ControllerMenu(MainLogic model, MainWindow gui) {
        super(model, gui);
    }

    @Override
    public void register(JMenuItem... components) {
        for(JMenuItem menu : components) {
            if(menu instanceof JCheckBoxMenuItem) {
                menu.addItemListener(this);
            } else {
                menu.addActionListener(this);
            }            
        }
    }

    @Override
    public void unregister(JMenuItem... components) {
        for(JMenuItem menu : components) {
            if(menu instanceof JCheckBoxMenuItem) {
                menu.removeItemListener(this);
            } else {
                menu.removeActionListener(this);
            }            
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
            case MainWindow.MENUITEM_OPEN:
                final JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new ScriptFilter());
                fc.setCurrentDirectory(new File(Utils.executionPath).getParentFile());
                int returnVal = fc.showOpenDialog(gui);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    String name = fc.getSelectedFile().getName(); //@todo, strip extension?
                    DirectoryInfo relativeDir = new DirectoryInfo(fc.getSelectedFile().getParent());
                    FileInfo file = new FileInfo(relativeDir, name);
                    String source = FileReader.readFile(file);
                    
                    gui.setEditorText(name, source, relativeDir, name, true);
                }
                break;
            case MainWindow.MENUITEM_CLOSE:
                this.close();
                break;
            default:
                System.err.println("Unimplemented menuItem: " + e.getActionCommand());
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        JCheckBoxMenuItem checkbox = (JCheckBoxMenuItem) e.getSource();
        boolean selected = e.getStateChange() == ItemEvent.SELECTED;
        
        switch(checkbox.getActionCommand()) {
            case MainWindow.MENUITEM_YICES:
                model.setEnabled(CompilerSMT.class, selected);
                break;
            case MainWindow.MENUITEM_UPPAAL:
                model.setEnabled(CompilerUppaal.class, selected);
                break;
            case MainWindow.MENUITEM_NUSMV:
                model.setEnabled(CompilerNuSMV.class, selected);
                break;
            case MainWindow.MENUITEM_BUILDALL:
                if(selected) {
                    model.enable(CompilerSMT.class, CompilerUppaal.class, CompilerNuSMV.class);
                    gui.checkMenuItem(MainWindow.MENUITEM_YICES, true);
                    gui.checkMenuItem(MainWindow.MENUITEM_UPPAAL, true);
                    gui.checkMenuItem(MainWindow.MENUITEM_NUSMV, true);
                }
                break;
            default:
                System.err.println("Unimplemented checkboxItem: " + checkbox.getActionCommand());
        }
    }
    
    private class ScriptFilter extends FileFilter {
        
        public final static String script = "script";
        
        //Accept all directories and all script files.
        @Override
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }

            String extension = getExtension(f);
            if (extension != null) {
                return extension.equals(script);
            }

            return false;
        }

        //The description of this filter
        @Override
        public String getDescription() {
            return "Scripts";
        }
        
        private String getExtension(File f) {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 &&  i < s.length() - 1) {
                ext = s.substring(i+1).toLowerCase();
            }
            return ext;
        }

}

}
