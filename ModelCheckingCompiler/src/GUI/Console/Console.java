/*
 * Console.java
 * Copyright(c) 2014
 */

package GUI.Console;

import java.awt.Color;

/**
 *
 * @author Peter Maandag
 */
public interface Console {    
    void print(boolean b); 
    void print(char c); 
    void print(char[] s);
    void print(double d); 
    void print(float f);
    void print(int i); 
    void print(long l); 
    void print(Object obj);
    void print(String s);

    void println(boolean b); 
    void println(char c); 
    void println(char[] s);
    void println(double d); 
    void println(float f);
    void println(int i); 
    void println(long l); 
    void println(Object obj);
    void println(String s);
    
    void println(String s, Color c);
}
