/*
 * ConsoleRedirect.java
 * Copyright(c) 2014
 */

package GUI.Console;

import java.awt.Color;
import java.util.Arrays;

/**
 *
 * @author Peter Maandag
 */
public class ConsoleRedirect implements Console {
    private final Console[] redirects;
    
    public ConsoleRedirect(Console... redirects) {
        this.redirects = redirects;
    }
    
    @Override
    public void print(boolean b) {
        for(Console con : redirects) {
            con.print(b);
        }
    } 
 
    @Override
    public void print(char c) {
        for(Console con : redirects) {
            con.print(c);
        }
    }
 
    @Override
    public void print(char[] s) {
        for(Console con : redirects) {
            con.print(Arrays.toString(s));
        }
    }

    @Override
    public void print(double d) {
        for(Console con : redirects) {
            con.print(d);
        }
    }
 
    @Override
    public void print(float f) {
        for(Console con : redirects) {
            con.print(f);
        }
    }

    @Override
    public void print(int i) {
        for(Console con : redirects) {
            con.print(i);
        }
    }
 
    @Override
    public void print(long l) {
        for(Console con : redirects) {
            con.print(l);
        }
    }
 
    @Override
    public void print(Object obj) {
        for(Console con : redirects) {
            con.print(obj);
        }
    }

    @Override
    public void print(String s) {
        for(Console con : redirects) {
            con.print(s);
        }
    }

    @Override
    public void println(boolean b) {
        for(Console con : redirects) {
            con.println(b);
        }
    } 
 
    @Override
    public void println(char c) {
        for(Console con : redirects) {
            con.println(c);
        }
    }
 
    @Override
    public void println(char[] s) {
        for(Console con : redirects) {
            con.println(Arrays.toString(s));
        }
    }

    @Override
    public void println(double d) {
        for(Console con : redirects) {
            con.println(d);
        }
    }
 
    @Override
    public void println(float f) {
        for(Console con : redirects) {
            con.println(f);
        }
    }

    @Override
    public void println(int i) {
        for(Console con : redirects) {
            con.println(i);
        }
    }
 
    @Override
    public void println(long l) {
        for(Console con : redirects) {
            con.println(l);
        }
    }
 
    @Override
    public void println(Object obj) {
        for(Console con : redirects) {
            con.println(obj);
        }
    }

    @Override
    public void println(String s) {
        for(Console con : redirects) {
            con.println(s);
        }
    }
    
    @Override
    public void println(String s, Color c) {
        for(Console con : redirects) {
            con.println(s);
        }
    }
}
