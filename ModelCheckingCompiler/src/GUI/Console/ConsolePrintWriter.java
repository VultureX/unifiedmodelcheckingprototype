/*
 * ConsolePrintWriter.java
 * Copyright(c) 2014
 */

package GUI.Console;

import java.awt.Color;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * @author Peter Maandag
 */
public class ConsolePrintWriter extends PrintWriter implements Console {
    
    public ConsolePrintWriter() {
        super(new StringWriter(), true);
    }

    @Override
    public void println(String s, Color c) {
        super.println(s);
    }
}
