/*
 * ConsolePrintStream.java
 * Copyright(c) 2014
 */

package GUI.Console;

import java.awt.Color;
import java.io.PipedOutputStream;
import java.io.PrintStream;

/**
 * 
 * @author Peter Maandag
 */

public class ConsolePrintStream extends PrintStream implements Console
{       
    public ConsolePrintStream() {
        super(new PipedOutputStream(), true);
    }
    
    public ConsolePrintStream(PrintStream p) {
        super(p, true);
    }
    
    public void redirectStdOut() {
        System.setOut( this );
    }
    
    public void redirectStdErr() {
        System.setErr( this );
    }

    @Override
    public void println(String s, Color c) {
        super.println(s);
    }
}
