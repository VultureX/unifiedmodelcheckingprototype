/*
 * ConsoleString.java
 * Copyright(c) 2014
 */

package GUI.Console;

import Tools.Lambda.Action1;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.util.Arrays;

/**
 *
 * @author Peter Maandag
 */
public abstract class ConsoleString implements Console {
    private final Action1<String> write;
    
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public ConsoleString() {
        this.write = getWriter();
    }
    
    protected abstract Action1<String> getWriter();
    
    @Override
    public void print(String s) {
        write.invoke(s);
    }

    @Override
    public void print(boolean b) {
        write.invoke(Boolean.toString(b));
    }

    @Override
    public void print(char c) {
        write.invoke(Character.toString(c));
    }

    @Override
    public void print(char[] s) {
        write.invoke(Arrays.toString(s));
    }

    @Override
    public void print(double d) {
        write.invoke(Double.toString(d));
    }

    @Override
    public void print(float f) {
        write.invoke(Float.toString(f));
    }

    @Override
    public void print(int i) {
        write.invoke(Integer.toString(i));
    }

    @Override
    public void print(long l) {
        write.invoke(Long.toString(l));
    }

    @Override
    public void print(Object obj) {
        write.invoke(obj.toString());
    }

    @Override
    public void println(boolean b) {
        write.invoke(Boolean.toString(b) + newLine);
    }

    @Override
    public void println(char c) {
        write.invoke(Character.toString(c) + newLine);
    }

    @Override
    public void println(char[] s) {
        write.invoke(Arrays.toString(s) + newLine);
    }

    @Override
    public void println(double d) {
        write.invoke(Double.toString(d) + newLine);
    }

    @Override
    public void println(float f) {
        write.invoke(Float.toString(f) + newLine);
    }

    @Override
    public void println(int i) {
        write.invoke(Integer.toString(i) + newLine);
    }

    @Override
    public void println(long l) {
        write.invoke(Long.toString(l) + newLine);
    }

    @Override
    public void println(Object obj) {
        write.invoke(obj.toString() + newLine);
    }
    
    @Override
    public void println(String s, Color c) {
        write.invoke(s + newLine);
    }

    @Override
    public void println(String s) {
        write.invoke(s + newLine);
    }
}
