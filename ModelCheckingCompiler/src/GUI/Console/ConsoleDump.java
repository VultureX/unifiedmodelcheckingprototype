/*
 * ConsoleDump.java
 * Copyright(c) 2014
 */

package GUI.Console;

import Benchmarks.ExecutionNotifier;
import Tools.FileInfo;
import Tools.FileWriterUtility;
import Tools.Lambda.Action1;
import Tools.Timer;
import static Tools.Utils.newLine;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter Maandag
 */
public class ConsoleDump extends ConsoleString implements ExecutionNotifier {
    
    private FileWriterUtility fw;
    
    public ConsoleDump(FileInfo file) {
        try {
            this.fw = new FileWriterUtility(file, true);
        } catch (IOException ex) {
            Logger.getLogger(ConsoleDump.class.getName()).log(Level.SEVERE, null, ex);
            if(fw != null) {
                fw.close();
            }
        }
    }
    
    private void close() {
        if(fw != null) {
            fw.close();
        }
    }
    
    private void write(String s) {
        if(fw != null && !fw.isClosed()) {
            try {
                fw.write(s);
            } catch (IOException ex) {
                Logger.getLogger(ConsoleDump.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }    
        
    @Override
    protected Action1<String> getWriter() {
        return this::write;
    }

    @Override
    public void onStart(String name) {
        
    }

    @Override
    public void onProgress(String name, double elapsedTime) {
        
    }

    @Override
    public void onShutdown(String name, String result, int exitCode, double elapsedTime) {
        String info = name + " completed with exit code " + exitCode + newLine
                + "Execution time: " + Timer.getFormattedTime(elapsedTime) + " seconds" + newLine;
        write(info + newLine);
        close();
    }

    @Override
    public void onAborted(String name, String result, int exitCode, double elapsedTime) {
        String info = name + " was aborted with exit code " + exitCode + newLine
                + "Execution time: " + Timer.getFormattedTime(elapsedTime) + " seconds" + newLine;
        write(info + newLine);
        close();
    }
}
