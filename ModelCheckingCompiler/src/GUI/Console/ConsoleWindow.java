/*
 * ConsoleWindow.java
 * Copyright(c) 2014
 */

package GUI.Console;

import GUI.MainWindow;
import Tools.Lambda.Action1;
import static Tools.Utils.newLine;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Peter Maandag
 */
public class ConsoleWindow extends ConsoleString {
    private AttributeSet aset;
    private StyledDocument doc;
    
    public ConsoleWindow(JTextPane pane, Color c) {        
        this.doc = pane.getStyledDocument();
        this.aset = setColor(c);
    }
    
    private AttributeSet setColor(Color c) {
        StyleContext sc = StyleContext.getDefaultStyleContext();        
        AttributeSet aSet = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
        aSet = sc.addAttribute(aSet, StyleConstants.FontFamily, "Lucida Console");
        //this.aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return aSet;
    }
    
    private void write(String line) {
        try {
            doc.insertString(doc.getLength(), line, aset);
        } catch (BadLocationException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    @Override
    protected Action1<String> getWriter() {
        return this::write;
    }
    
    @Override
    public void println(String line, Color c) {
        AttributeSet aSet = setColor(c);
        try {
            doc.insertString(doc.getLength(), line + newLine, aSet);
        } catch (BadLocationException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
