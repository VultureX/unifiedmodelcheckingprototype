/*
 * MainWindow.java
 * Copyright(c) 2014
 */
package GUI;

import Benchmarks.NuSMV;
import Benchmarks.Uppaal;
import Benchmarks.Yices;
import CodeGeneration.SyntaxHighlighter;
import GUI.Console.Console;
import GUI.Console.ConsolePrintStream;
import GUI.Console.ConsoleWindow;
import Tools.DirectoryInfo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultCaret;
import javax.swing.text.StyledDocument;
import modelcheckingcompiler.Main;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public class MainWindow extends JFrame {
    
    public static final int DEFAULT_WIDTH = 800;
    public static final int DEFAULT_HEIGHT = 600;
    public static final Font EDITOR_FONT = new Font("Lucida Console", Font.PLAIN, 14);
    
    public static final String MENU_FILE = "File";
    public static final String MENUITEM_OPEN = "Open";
    public static final String MENUITEM_SAVE = "Save";
    public static final String MENUITEM_SAVEAS = "Save as";
    public static final String MENUITEM_CLOSE = "Quit";
    public static final String MENU_BUILD = "Build";
    public static final String MENUITEM_WRITEFILE = "Write to file";
    public static final String MENUITEM_YICES = Yices.name;
    public static final String MENUITEM_NUSMV = NuSMV.name;
    public static final String MENUITEM_UPPAAL = Uppaal.name;
    public static final String MENUITEM_BUILDALL = "All";
    public static final String BUTTON_COMPILE = "Compile";
    public static final String BUTTON_START = "Run";
    public static final String BUTTON_STOP = "Stop";
    
    private MainLogic model;
    
    private JMenuItem[] menuItems;
    private JPanel mainPanel;
    private FlashingTabbedPane consolePane;
    private FlashingTabbedPane editorPane;
    private JButton[] buttons;
    private static Console console = new ConsolePrintStream(System.out);
    
    public MainWindow(MainLogic model) {
        this(model, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
    
    public MainWindow(MainLogic model, int width, int height) {
        this.model = model;
        
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.Y_AXIS));
        this.mainPanel.setPreferredSize(new Dimension(width, height));
        
        initMainPanel();
        initMenu();
        
        this.setTitle(Main.APP_NAME + " " + Main.VERSION);
        this.setMinimumSize(new Dimension(200,200));
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.validate();
        this.pack();
        this.setLocationRelativeTo(null);
    }
    
    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        JMenuItem menuItem;
        ArrayList<JMenuItem> items = new ArrayList<>();
        
        //File Menu:
        menu = new JMenu(MENU_FILE);
        menuBar.add(menu);        
        menuItem = new JMenuItem(MENUITEM_OPEN);
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JMenuItem(MENUITEM_SAVE);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
            KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JMenuItem(MENUITEM_SAVEAS);
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JMenuItem(MENUITEM_CLOSE);
        menu.add(menuItem); items.add(menuItem);
        
        //Build menu:
        menu = new JMenu(MENU_BUILD);
        menuBar.add(menu);        
        menuItem = new JCheckBoxMenuItem(MENUITEM_WRITEFILE);
        menu.add(menuItem); items.add(menuItem);
        menu.addSeparator();
        menuItem = new JCheckBoxMenuItem(MENUITEM_YICES);
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JCheckBoxMenuItem(MENUITEM_NUSMV);
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JCheckBoxMenuItem(MENUITEM_UPPAAL);
        menu.add(menuItem); items.add(menuItem);
        menuItem = new JCheckBoxMenuItem(MENUITEM_BUILDALL);
        menu.add(menuItem); items.add(menuItem);
        
        this.setJMenuBar(menuBar);        
        this.menuItems = items.toArray(new JMenuItem[]{});
    }
    
    private void initButtons(JPanel buttonPanel) {
        JButton stopButton = new JButton(BUTTON_STOP);
        //stopButton.setEnabled(false);
        this.buttons = new JButton[] {
            new JButton(BUTTON_COMPILE),
            new JButton(BUTTON_START),
            stopButton
        };
        for(JButton b : buttons) {
            buttonPanel.add(b);
        }
    }
    
    private void initMainPanel() {
        //Buttons:
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        buttonPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 10));
        initButtons(buttonPanel);
        mainPanel.add(buttonPanel);
        
        //Script editor:
        this.editorPane = new FlashingTabbedPane();
        editorPane.setMinimumSize(new Dimension(Integer.MAX_VALUE, 145));
        editorPane.setPreferredSize(new Dimension(Integer.MAX_VALUE, 400));
        //@todo: invent better names:
        addNewEditorTab(editorPane, "New", new DirectoryInfo("../"), "compiledNew", true);
        
        //Output console:
        this.consolePane = new FlashingTabbedPane();
        consolePane.setMinimumSize(new Dimension(Integer.MAX_VALUE,145));
        
        //Split pane that contains console and editor:
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                           editorPane, consolePane);
        splitPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel.add(splitPane);
    }
    
    public ConsoleWindow addNewTab(String name) {
        return new ConsoleWindow(addNewTab(name, true),Color.BLACK);
    }
    
    private EditorPane addNewEditorTab(FlashingTabbedPane tabbedPane, String name, 
            DirectoryInfo relativeDir, String compileName, boolean closeable) {
        EditorPane pane = new EditorPane(name, relativeDir, compileName);
        pane.setEditorKit(new TabSizeEditorKit()); //Better line wrapping
        pane.setFont(EDITOR_FONT);
        
        JScrollPane editorScrollPane = new JScrollPane(pane);
        editorScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setHorizontalScrollBarPolicy(
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        tabbedPane.addTab(name, editorScrollPane);
        
        //Add close button
        if(closeable) {
            tabbedPane.setTabComponentAt(tabbedPane.getTabCount()-1, new ButtonTabComponent(tabbedPane));
        }
        
        return pane;
    }
    
    private class EditorPane extends JTextPane {
        private String name, compileName;
        private DirectoryInfo relativeDir;
        public EditorPane(String name, DirectoryInfo relativeDir, String compileName) {
            this.name = name;
            this.relativeDir = relativeDir;
            this.compileName = compileName;
        }
        
        /*@Override
        public boolean getScrollableTracksViewportWidth()
        {
            //disable linewrap:
            return false;
        }*/

    }

    private JTextPane addNewTab(String name, boolean closeable) {
        JTextPane textPane = new JTextPane();
        //auto scroll down
        DefaultCaret caret = (DefaultCaret)textPane.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        textPane.setEditable(false);
        //textPane.setContentType("text/html");
        textPane.setEditorKit(new WordWrapEditorKit()); //Better line wrapping
        
        JScrollPane consoleScrollPane = new JScrollPane(textPane);
        //consoleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        consoleScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        consolePane.addTab(name, consoleScrollPane);
        
        //Add close button
        if(closeable) {
            consolePane.setTabComponentAt(consolePane.getTabCount()-1, new ButtonTabComponent(consolePane));
        }
        
        return textPane;
    }
    
    public String getEditorCompilename() {
        return ((EditorPane)((JScrollPane) editorPane.getSelectedComponent()).getViewport().getView()).compileName;
    }
    
    public DirectoryInfo getEditorRelativeDir() {
        return ((EditorPane)((JScrollPane) editorPane.getSelectedComponent()).getViewport().getView()).relativeDir;
    }
    
    public String getEditorFilename() {
        int index = editorPane.getSelectedIndex();
        if(index != -1) {
            return editorPane.getTitleAt(editorPane.getSelectedIndex());
        }
        return null;
    }
    
    public String getEditorText() {
        String text = ((JTextPane)((JScrollPane) editorPane.getSelectedComponent()).getViewport().getView()).getText();
        System.out.println(text);
        return text;
        
        //Document d = ((JTextPane) editorPane.getSelectedComponent()).getDocument();
        //return d.getText(0, d.getLength());
        //return editorPane.getText();
    }
    
    public void setEditorText(String name, String text, DirectoryInfo relativeDir, String compileName, boolean newTab) {
        /*StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.RED);
        
        StyledDocument d = new DefaultStyledDocument();
        try {
            d.insertString(0, text, aset);
        } catch (BadLocationException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        editorPane.setDocument(d);*/
        
        JScrollPane scrollPane = (JScrollPane) editorPane.getSelectedComponent();
        JTextPane pane;        
        if(scrollPane == null || newTab) {
            pane = addNewEditorTab(editorPane, name, relativeDir, compileName, true);
            editorPane.setSelectedIndex(editorPane.getTabCount()-1); //Must revalidate, other wise document is not painted properly!
            editorPane.validate();
            editorPane.repaint();
        } else {
            pane = (JTextPane) scrollPane.getViewport().getView();
            int index = editorPane.indexOfComponent(scrollPane);
            editorPane.setTitleAt(index, name);
        }                  
        
        pane.setText(text);
        
        
        SyntaxHighlighter s = new SyntaxHighlighter(pane);       
        StyledDocument doc = s.getHighLighted(pane.getStyledDocument());
        doc.addDocumentListener(s);
        pane.setDocument(doc);        
             
        s.setDaemon(true);
        s.start();        
    }
    
    public static Console getConsole() {
        return console;
    }
    
    public void createConsole() {
        JTextPane consoleTextPane = addNewTab("Console", false);
        console = new ConsoleWindow(consoleTextPane, Color.BLACK);
        //ConsoleStream.redirectOut((ConsoleWindow)console);
        //console = addNewTab("Errors", false);
        //ConsoleStream.redirectErr(new ConsoleWindow(consoleTextPane, Color.RED));
    }
    
    public void setControllers() {        
        ControllerWindow cw = new ControllerWindow(model, this);
        cw.register(this); //java-bug: Using register function on Controller is broken for JFrame
        Controller c = new ControllerMenu(model, this);
        c.register(menuItems);
        c = new ControllerButton(model, this);
        c.register(buttons);        
    }
    
    public void enableButton(String name, boolean enabled) {
        for(JButton button : buttons) {
            if(button.getText().equals(name)) {
                button.setEnabled(enabled);
            }
        }
    }
    
    public void checkMenuItem(String name, boolean check) {
        for(JMenuItem menu : menuItems) {
            if(menu instanceof JCheckBoxMenuItem && menu.getText().equals(name)) {
                JCheckBoxMenuItem item = (JCheckBoxMenuItem) menu;
                item.setState(true);
            }         
        }
    }
}