/*
 * Controller.java
 * Copyright(c) 2014
 */

package GUI;

import java.awt.Component;
import modelcheckingcompiler.MainLogic;

/**
 *
 * @author Peter Maandag
 */
public abstract class Controller<T extends Component> {
    protected MainLogic model;
    protected MainWindow gui;
    
    public Controller(MainLogic model, MainWindow gui) {
        this.model = model;
        this.gui = gui;
    }
    
    public abstract void register(T... components);
    public abstract void unregister(T... components);
}
