/*
 * TabSizeEditorKit.java
 * Copyright(c) 2014
 */

package GUI;

import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.text.*;

/**
 *
 * @author Peter Maandag
 */
public class TabSizeEditorKit extends StyledEditorKit {
    
    public static final int TAB_SIZE = 30;
    
    @Override
    public ViewFactory getViewFactory() {
        return new MyViewFactory();
    }
    
    static class MyViewFactory implements ViewFactory {

        @Override
        public View create(Element elem) {
            String kind = elem.getName();
            if (kind != null) {
                switch (kind) {
                    case AbstractDocument.ContentElementName:
                        return new LabelView(elem);
                    case AbstractDocument.ParagraphElementName:
                        return new CustomTabParagraphView(elem);
                    case AbstractDocument.SectionElementName:
                        return new BoxView(elem, View.Y_AXIS);
                    case StyleConstants.ComponentElementName:
                        return new ComponentView(elem);
                    case StyleConstants.IconElementName:
                        return new IconView(elem);
                }
            }
 
            return new LabelView(elem);
        }
    }
    
    static class CustomTabParagraphView extends ParagraphView {
        private final short NUMBERS_WIDTH = 30;
        
        public CustomTabParagraphView(Element elem) {
            super(elem);
            //short top = 0;
            //short left = 0;
            //short bottom = 0;
            //short right = 0;
            //this.setInsets(top, left, bottom, right);
        }
		
        @Override
        public float nextTabStop(float x, int tabOffset) {
            float tabBase = getTabBase();
            TabSet tabs = getTabSet();
            if(tabs == null) {                
                x -= tabBase;
                return (float)(tabBase + (((int)x / TAB_SIZE + 1) * TAB_SIZE));
            }

            return super.nextTabStop(x + tabBase, tabOffset);
        }
        
        /*@Override
        protected final void setInsets(short top, short left, short bottom,
                                 short right) {
            super.setInsets(top,(short)(left + NUMBERS_WIDTH),
                                 bottom,right);
        }*/
        
        @Override
        protected short getLeftInset() {
            return NUMBERS_WIDTH;
        }

        @Override
        public void paintChild(Graphics g, Rectangle r, int n) {
            super.paintChild(g, r, n);            
            int previousLineCount = getPreviousLineCount();
            int numberX = r.x - getLeftInset();
            int numberY = r.y + r.height - 3;
            
            if(n == 0) {
                g.drawString(Integer.toString(previousLineCount + 1),
                                      numberX, numberY);
            }
        }

        private int getPreviousLineCount() {
            int lineCount = 0;
            View parent = this.getParent();
            int count = parent.getViewCount();
            for (int i = 0; i < count; i++) {                
                if (parent.getView(i) == this) {
                    break;
                }
                lineCount++; //+= parent.getView(i).getViewCount();
            }
            return lineCount;
        }
 
    }


}
