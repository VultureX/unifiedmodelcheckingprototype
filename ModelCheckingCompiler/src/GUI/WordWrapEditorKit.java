/*
 * WordWrapEditorKit.java
 * Copyright(c) 2014
 */

package GUI;

import javax.swing.text.*;
import javax.swing.*;
import static Tools.Utils.newLine;

/**
 *
 * @author Peter Maandag
 */
public class WordWrapEditorKit extends StyledEditorKit {
    
    @Override
    public ViewFactory getViewFactory() {
        return new MyViewFactory();
    }
    
    static class MyViewFactory implements ViewFactory {

        @Override
        public View create(Element elem) {
            String kind = elem.getName();
            if (kind != null) {
                switch (kind) {
                    case AbstractDocument.ContentElementName:
                        return new LineBreakLabelView(elem);
                        //return new LabelView(elem);
                    case AbstractDocument.ParagraphElementName:
                        return new NumberedParagraphView(elem);
                    case AbstractDocument.SectionElementName:
                        return new BoxView(elem, View.Y_AXIS);
                    case StyleConstants.ComponentElementName:
                        return new ComponentView(elem);
                    case StyleConstants.IconElementName:
                        return new IconView(elem);
                }
            }
            
            //return new LabelView(elem);
            return new LineBreakLabelView(elem);
        }
    }
    
    static class LineBreakLabelView extends LabelView {
        public LineBreakLabelView(Element elem) {
            super(elem);
        }
        
        @Override
        public int getBreakWeight(int axis, float pos, float len) { 
            //return View.BadBreakWeight;
            if (axis == View.X_AXIS) {
                checkPainter();
                int p0 = getStartOffset();
                int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                if (p1 == p0) {
                    // can't even fit a single character
                    return View.BadBreakWeight;
                }
                try {
                    //if the view contains line break char return forced break
                    if (getDocument().getText(p0, p1 - p0).indexOf(newLine) >= 0) {
                        //return View.ForcedBreakWeight;
                        return View.ExcellentBreakWeight;
                    }
                }
                catch (BadLocationException ex) {
                    //should never happen
                }  

            }
            return super.getBreakWeight(axis, pos, len);
        }
        
        @Override
        public View breakView(int axis, int p0, float pos, float len) { 
            if (axis == View.X_AXIS) {
                checkPainter();
                int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                try {
                    //if the view contains line break char break the view
                    int index = getDocument().getText(p0, p1 - p0).indexOf(newLine);
                    if (index >= 0) {
                        GlyphView v = (GlyphView) createFragment(p0, p0 + index + 1);
                        return v;
                    }
                }
                catch (BadLocationException ex) {
                    //should never happen
                }

            }
            return super.breakView(axis, p0, pos, len);
        }
    }
    
    static class NumberedParagraphView extends ParagraphView {
        private final short NUMBERS_WIDTH = 10;

        public NumberedParagraphView(Element e) {
            super(e);
//            short top = 0;
//            short left = 0;
//            short bottom = 0;
//            short right = 0;
//            this.setInsets(top, left, bottom, right);
        }
        
        @Override
        protected SizeRequirements calculateMinorAxisRequirements(int axis, SizeRequirements r) { 
            if (r == null) { 
                  r = new SizeRequirements(); 
            } 
            float pref = layoutPool.getPreferredSpan(axis); 
            float min = layoutPool.getMinimumSpan(axis); 
            // Don't include insets, Box.getXXXSpan will include them. 
              r.minimum = (int)min; 
              r.preferred = Math.max(r.minimum, (int) pref); 
              r.maximum = Integer.MAX_VALUE; 
              r.alignment = 0.5f; 
            return r; 
        }

//        @Override
//        protected void setInsets(short top, short left, short bottom,
//                                 short right) {
//            super.setInsets(top,(short)(left+NUMBERS_WIDTH),
//                                 bottom,right);
//        }
//
//        @Override
//        public void paintChild(Graphics g, Rectangle r, int n) {
//            super.paintChild(g, r, n);
//            int previousLineCount = getPreviousLineCount();
//            int numberX = r.x - getLeftInset();
//            int numberY = r.y + r.height - 3;
//
//            g.drawString(Integer.toString(previousLineCount + n + 1),
//                                      numberX, numberY);
//        }
//
//        private int getPreviousLineCount() {
//            int lineCount = 0;            
//            View parent = this.getParent();
//            int count = parent.getViewCount();            
//            for (int i = 0; i < count; i++) {               
//                if (parent.getView(i) == this) {
//                    break;
//                }
//                else {
//                    lineCount += parent.getView(i).getViewCount();                    
//                }
//            }
//            return lineCount;
//        }
    }
}
